module.exports = function (err, req, res, next) {
    res.status(err.code);
    res.json({
        'ResponseCode': '0',
        'ResponseMessage': 'Error',
        'Response': [],
        'errors': err.message,
    });

}