const {verify} = require('jsonwebtoken');
const {BlackList} = require('../models');

const validateToken =(req, res, next) =>{
    const accessToken = req.header('accessToken');
    if (!accessToken) return res.json({
        'ResponseCode': '0',
        'ResponseMessage': 'Please Login to continue',
        'Response': {},
        'errors': 'User not logged In()',
    })
    BlackList.findOne({where: { accessToken: accessToken }})
        .then(dat=>{
            if(dat){
                return res.json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Please Login to continue',
                    'Response': {},
                    'errors': 'User not logged In',
            }) 
        }
        else{
            try{
        
        const validToken = verify(accessToken, 'Important Text');
        req.user = validToken;
        //console.log(validToken)
        if(validToken) return next();
    }
    catch(err){
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'Invalid AccessToken',
            'Response': {},
            'errors': 'Access Denied',
        })
    }
        }
    })
    
};

module.exports= {validateToken};