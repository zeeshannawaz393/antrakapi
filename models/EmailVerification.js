module.exports = (sequelize, DataTypes) =>{
    const EmailVerification = sequelize.define('EmailVerification', {
        requested_at: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        OTP: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
    });
    // AEmailVerification.associate = (models)=>{
    //     AEmailVerification.hasMany(models.Quizzes,{
    //         onDelete: 'cascade', 
    //     });
    //     models.Quizzes.belongsTo(AEmailVerification);
    // };
    return EmailVerification;
};
