module.exports = (sequelize, DataTypes) =>{
    const Package = sequelize.define('Package', {
        weight: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        length: {
            type: DataTypes.FLOAT,
            allowNull: true,
        }, 
        width: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        height: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        insurance: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }, 
        estWorth: {
            type: DataTypes.FLOAT,
            allowNull: true,
        }, 
        total: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        unit: {
            type: DataTypes.STRING(10),
            allowNull: true,
        }, 
    });
    // // Each user can have one email verification code
    // Package.associate = (models)=>{
    //     Package.hasOne(models.EmailVerification,{
    //         onDelete: 'cascade', 
    //     });
    //     models.EmailVerification.belongsTo(Package);
    // };
    // // Each User can have many password reset requests
    // Package.associate = (models)=>{
    //     Package.hasMany(models.ForgetPassword,{
    //         onDelete: 'cascade', 
    //     });
    //     models.ForgetPassword.belongsTo(Package);
    // };
    return Package;
};
