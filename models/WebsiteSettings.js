module.exports = (sequelize, DataTypes) =>{
    const WebsiteSettings = sequelize.define('WebsiteSettings', {
        title: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        value: {
            type: DataTypes.TEXT('long'),
            allowNull: true,
        }, 
         
    });
    return WebsiteSettings;
};