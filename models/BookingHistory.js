module.exports = (sequelize, DataTypes) =>{
    const BookingHistory = sequelize.define('BookingHistory', {
        time: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
    });
    return BookingHistory;
};
