module.exports = (sequelize, DataTypes) =>{
    const AppSettings = sequelize.define('AppSettings', {
        title: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        value: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        }, 
        
         
    });
    return AppSettings;
};
