module.exports = (sequelize, DataTypes) =>{
    const Tips = sequelize.define('Tips', {
        amount: {
            type: DataTypes.STRING(),
            allowNull: true,
        }
    });
    
    return Tips;
};