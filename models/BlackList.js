module.exports = (sequelize, DataTypes) =>{
    const BlackList = sequelize.define('BlackList', {
        accessToken: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
    });
    return BlackList;
};