module.exports = (sequelize, DataTypes) =>{
    const Role = sequelize.define('Role', {
        name: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        }, 
    });
    Role.associate = (models)=>{
        Role.hasOne(models.Users);
        models.Users.belongsTo(Role);
        
        Role.hasOne(models.Permissions);
        models.Permissions.belongsTo(Role);
        
    };
    return Role;
};
