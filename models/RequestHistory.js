module.exports = (sequelize, DataTypes) =>{
    const RequestHistory = sequelize.define('RequestHistory', {
        amount: {
            type: DataTypes.INTEGER(),
            allowNull: true,
        },
        pending: {
            type: DataTypes.INTEGER(),
            allowNull: true,
        },
        time: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        
    });
    return RequestHistory;
};

