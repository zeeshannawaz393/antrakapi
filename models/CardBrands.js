module.exports = (sequelize, DataTypes) =>{
    const CardBrand = sequelize.define('CardBrand', {
        title: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        image: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
    });
    return CardBrand;
};