module.exports = (sequelize, DataTypes) =>{
    const VehicleImages = sequelize.define('VehicleImages', {
        title: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        image: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        uploadTime: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
    });
    // VehicleImages.associate = (models)=>{
    //     VehicleImages.hasOne(models.DriverDetails);
    //     models.DriverDetails.belongsTo(VehicleImages);
    // };
    return VehicleImages;
};
