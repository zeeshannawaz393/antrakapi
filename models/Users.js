module.exports = (sequelize, DataTypes) =>{
    const Users = sequelize.define('Users', {
        firstName: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        lastName: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        email: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        phoneNum: {
            type: DataTypes.STRING(72),
            allowNull: true,
        },
        password: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        email_verified_at: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        deviceToken: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        rememberToken: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        stripeCustomerId: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        deviceToken: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        profilePicture: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
    });
    // Each user can have one email verification code
    Users.associate = (models)=>{
        Users.hasOne(models.EmailVerification,{
            onDelete: 'cascade', 
        });
        models.EmailVerification.belongsTo(Users);
        // Each User can have many password reset requests
        Users.hasMany(models.ForgetPassword,{
            onDelete: 'cascade', 
        });
        models.ForgetPassword.belongsTo(Users);
        // Each User can have many addresses
        Users.hasMany(models.Address,{
            onDelete: 'cascade', 
        });
        models.Address.belongsTo(Users);
        // Each end User can have many bookings
        Users.hasMany(models.Booking,{
            onDelete: 'cascade', foreignKey: 'UserId' 
        });
        models.Booking.belongsTo(Users,{as: 'User',foreignKey: 'UserId' });
        // Each driver can have many bookings
        Users.hasMany(models.Booking,{
            onDelete: 'cascade', foreignKey: 'DriverId' 
        });
        models.Booking.belongsTo(Users,{as: 'Driver',foreignKey: 'DriverId' });
        
        // Each User can have one Driver Details
        Users.hasMany(models.DriverDetails,{
            onDelete: 'cascade', 
        });
        models.DriverDetails.belongsTo(Users);
        // Each User can have one Vehicle Details
        Users.hasMany(models.VehicleDetails,{
            onDelete: 'cascade', 
        });
        models.VehicleDetails.belongsTo(Users);
         // Each User can have many cancel history
         Users.hasMany(models.CancelHistory,{
            onDelete: 'cascade', 
        });
        models.CancelHistory.belongsTo(Users);
        // Each User can have many Payment History
        Users.hasMany(models.PaymentHistory,{
            onDelete: 'cascade',
        });
        models.PaymentHistory.belongsTo(Users);
        // Each user can have many  Wallet entries
        Users.hasMany(models.Wallet,{
            onDelete: 'cascade', 
        });
        models.Wallet.belongsTo(Users);
        // Each user can make many Payment Requests
        Users.hasMany(models.RequestHistory,{
            onDelete: 'cascade', 
        });
        models.RequestHistory.belongsTo(Users);
        // Each User can have many Banks
        Users.hasMany(models.Banks,{
            onDelete: 'cascade', 
        });
        models.Banks.belongsTo(Users);
        
        // Each User can have many Ratings
        Users.hasMany(models.Ratings);
        models.Ratings.belongsTo(Users);
        // Each User can have many Tips
        Users.hasMany(models.Tips);
        models.Tips.belongsTo(Users);
    };
    return Users;
};
