module.exports = (sequelize, DataTypes) =>{
    const DriverDetails = sequelize.define('DriverDetails', {
        licIssueDate: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        licExpiryDate: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        licNum: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        issuingState: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        licFrontPhoto: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        licBackPhoto: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        SNN: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        accountTitle: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        iBAN: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        bank: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        country: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
    });
    // Each user can have one email verification code
    // DriverDetails.associate = (models)=>{
    //     DriverDetails.hasOne(models.EmailVerification,{
    //         onDelete: 'cascade', 
    //     });
    //     models.EmailVerification.belongsTo(DriverDetails);
    // }
    return DriverDetails;
};
