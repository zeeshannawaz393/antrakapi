module.exports = (sequelize, DataTypes) =>{
    const Address = sequelize.define('Address', {
        title: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        building: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        aptNum: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        city: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        state: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        zip: {
            type: DataTypes.STRING(72),
            allowNull: true,
        },
        phoneNum: {
            type: DataTypes.STRING(72),
            defaultValue: false,
        }, 
        status: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }, 
        lng: { 
            type: DataTypes.FLOAT,
            allowNull: true,
        }, 
        lat: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        exactAddress: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
    });
    // // Each Address can have many bookings
    Address.associate = (models)=>{
        Address.hasMany(models.Booking,{ foreignKey: 'pickupId' });
        Address.hasMany(models.Booking,{ foreignKey: 'deliveryId' });
        models.Booking.belongsTo(Address, {as: 'pickupAddress',foreignKey: 'pickupId' });
        models.Booking.belongsTo(Address, {as: 'deliveryAddress',foreignKey: 'deliveryId' });
    };
    // // Each User can have many password reset requests
    // Address.associate = (models)=>{
    //     Address.hasMany(models.ForgetPassword,{
    //         onDelete: 'cascade', 
    //     });
    //     models.ForgetPassword.belongsTo(Address);
    // };
    return Address;
};
