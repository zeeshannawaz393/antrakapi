module.exports = (sequelize, DataTypes) =>{
    const BookingStatus = sequelize.define('BookingStatus', {
        name: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        }, 
    });
    BookingStatus.associate = (models)=>{
        BookingStatus.hasOne(models.Booking);
        models.Booking.belongsTo(BookingStatus);
        BookingStatus.hasMany(models.BookingHistory);
        models.BookingHistory.belongsTo(BookingStatus);
    };
    return BookingStatus;
};
