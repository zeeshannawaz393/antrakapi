module.exports = (sequelize, DataTypes) =>{
    const Restricted = sequelize.define('Restricted', {
        name: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        image: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status:{
            type: DataTypes.BOOLEAN,
            allowNull: true,
        }, 
      
    });
  
   
    return Restricted;
};
