module.exports = (sequelize, DataTypes) =>{
    const Category = sequelize.define('Category', {
        name: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        Image: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
       adult_18plus: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
    });
    Category.associate = (models)=>{
        Category.hasMany(models.Package);
        models.Package.belongsTo(Category);
    };
    return Category;
};
