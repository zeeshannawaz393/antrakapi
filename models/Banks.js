module.exports = (sequelize, DataTypes) =>{
    const Banks = sequelize.define('Banks', {
        accTitle: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        iBAN: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        bankName: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        country: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        
         
    });
    return Banks;
};