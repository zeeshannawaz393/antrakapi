module.exports = (sequelize, DataTypes) =>{
    const VehiclesType = sequelize.define('VehiclesType', {
        name: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        image: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        baseRateVec: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
         baseRateMile: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        
        //volume capacity
          VolumeCap: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        
        //weight capacity
         WeightCap: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        
        
    });
    VehiclesType.associate = (models)=>{
        VehiclesType.hasOne(models.DriverDetails);
        models.DriverDetails.belongsTo(VehiclesType);
        
        VehiclesType.hasMany(models.VehicleDetails);
        models.VehicleDetails.belongsTo(VehiclesType);
        
        VehiclesType.hasMany(models.Booking);
        models.Booking.belongsTo(VehiclesType);
    };
    return VehiclesType;
};
