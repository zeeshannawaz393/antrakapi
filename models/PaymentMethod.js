module.exports = (sequelize, DataTypes) =>{
    const PaymentMethod = sequelize.define('PaymentMethod', {
        title: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        clientKey: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        secretKey: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        
         
    });
    return PaymentMethod;
};