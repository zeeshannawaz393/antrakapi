module.exports = (sequelize, DataTypes) =>{
    const CancelHistory = sequelize.define('CancelHistory', {
        reason: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        note: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        amount: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        cancelledAt: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
    });
    return CancelHistory;
};
