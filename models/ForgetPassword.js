module.exports = (sequelize, DataTypes) =>{
    const ForgetPassword = sequelize.define('ForgetPassword', {
        requested_at: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        OTP: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        expiry_at: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
    });
    // AEmailVerification.associate = (models)=>{
    //     AEmailVerification.hasMany(models.Quizzes,{
    //         onDelete: 'cascade', 
    //     });
    //     models.Quizzes.belongsTo(AEmailVerification);
    // };
    return ForgetPassword;
};
