module.exports = (sequelize, DataTypes) =>{
    const PaymentHistory = sequelize.define('PaymentHistory', {
        amount: {
            type: DataTypes.INTEGER(),
            allowNull: true,
        },
        transaction_id: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        time: {
            type: DataTypes.STRING(),
            allowNull: true,
        }
    });
    return PaymentHistory;
};