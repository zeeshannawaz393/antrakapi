module.exports = (sequelize, DataTypes) =>{
    const Wallet = sequelize.define('Wallet', {
        amount: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
    });
   
    return Wallet;
};