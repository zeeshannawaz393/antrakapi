module.exports = (sequelize, DataTypes) =>{
    const Charge = sequelize.define('Charge', {
        title: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        value: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        amount: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    });
    return Charge;
};



