module.exports = (sequelize, DataTypes) =>{
    const VehicleDetails = sequelize.define('VehicleDetails', {
        make: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        model: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        year: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        licPlate: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        color: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },  
    });
    VehicleDetails.associate = (models)=>{
        VehicleDetails.hasMany(models.VehicleImages);
        models.VehicleImages.belongsTo(VehicleDetails);
    };
    return VehicleDetails;
};
