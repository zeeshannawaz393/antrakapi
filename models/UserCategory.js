module.exports = (sequelize, DataTypes) =>{
    const UserCategory = sequelize.define('UserCategory', {
        name: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
    });
    UserCategory.associate = (models)=>{
        UserCategory.hasOne(models.Users);
        models.Users.belongsTo(UserCategory);
    };
    return UserCategory;
};
