module.exports = (sequelize, DataTypes) =>{
    const Coupons = sequelize.define('Coupons', {
        name: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        discount: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        from: {
            type: DataTypes.DATE(),
            allowNull: true,
        },
        to: {
            type: DataTypes.DATE(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        type: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        conditionAmount: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        
    });
    Coupons.associate = (models)=>{
        Coupons.hasOne(models.Booking);
        models.Booking.belongsTo(Coupons);
    };
    return Coupons;
};