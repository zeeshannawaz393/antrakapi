module.exports = (sequelize, DataTypes) =>{
    const BookIDPictures = sequelize.define('BookIDPictures', {
        senderFront: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        senderBack: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        receiverFront: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        receiverback: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        
    });
    
    return BookIDPictures;
};