module.exports = (sequelize, DataTypes) =>{
    const Booking = sequelize.define('Booking', {
        rName: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        rEmail: {
            type: DataTypes.STRING(),
            allowNull: true,
        }, 
        rPhoneNum: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        rAlPhoneNum: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        pickupDate: {
            type: DataTypes.STRING(),
            defaultValue: false,
        }, 
        note: {
            type: DataTypes.STRING(),
            defaultValue: false,
        }, 
        total: {
            type: DataTypes.FLOAT,
            allowNull: true,
        }, 
        grandTotal: {
            type: DataTypes.FLOAT,
            allowNull: true,
        },
        condition: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        totalDistance: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        securityKey: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        estDriverEarning: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        load_unload: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }, 
        paymentConfirmed: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        paymentRecieved: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        pmId: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        piId: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        chargeId: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        paymentMethod: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    });
    // Each user can have one email verification code
    Booking.associate = (models)=>{
        Booking.hasMany(models.Package,{
            onDelete: 'cascade', 
        });
    models.Package.belongsTo(Booking);
    // Each User can have many cancel history
    Booking.hasMany(models.CancelHistory,{
            onDelete: 'cascade', 
    });
    models.CancelHistory.belongsTo(Booking);
    
    Booking.hasMany(models.BookingHistory);
    models.BookingHistory.belongsTo(Booking);
        
    // Each Booking has one rating 
    Booking.hasOne(models.Ratings);
    models.Ratings.belongsTo(Booking);
        
    // Each Booking has one rating 
    Booking.hasOne(models.Tips);
    models.Tips.belongsTo(Booking);
        
    // Each Booking has BookIDPictures 
    Booking.hasOne(models.BookIDPictures);
    models.BookIDPictures.belongsTo(Booking);
    // Each booking has one wallet
    Booking.hasOne(models.Wallet);
    models.Wallet.belongsTo(Booking);
    };
    // // Each User can have many password reset requests
    // Booking.associate = (models)=>{
    //     Booking.hasMany(models.ForgetPassword,{
    //         onDelete: 'cascade', 
    //     });
    //     models.ForgetPassword.belongsTo(Booking);
    // };
    return Booking;
};


