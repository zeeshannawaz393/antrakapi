module.exports = (sequelize, DataTypes) =>{
    const Permissions = sequelize.define('Permissions', {
        DashBoard: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Users: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Admin: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }, 
        Employees: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Roles: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Mails: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Drivers: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Coupons: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Charges: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Bookings: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Categories: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Banners: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        RestrictedItems: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Vehicles: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        RequestHistory: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        PendingRequests: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        PaymentHistory: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        Earnings: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        FrontEndSettings: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        
    });
    return Permissions;
};
