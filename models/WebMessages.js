module.exports = (sequelize, DataTypes) =>{
    const WebMessages = sequelize.define('WebMessages', {
        firstName: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        lastName: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        email: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        
        message: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        
    });
    return WebMessages;
};
