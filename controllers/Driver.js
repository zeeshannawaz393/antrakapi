module.exports = function RegisterDriver(handler){
    return async (req, res, next)=>{
        try{
           await handler(req, res, next);
        }
        catch(ex){
            return (res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error',
                'Response': [],
                'errors': "User exists",
            }));
        }
    }
};