const express = require('express');
const router = express();
const {Role} = require('../models');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');

/*
            1. Add Role
    ___________________________________________________
*/
router.post('/add',  asyncMiddleware(async (req, res) => {
    const {name, status } = req.body;
    const exist = await Role.findOne({where: {name: name}})
    if(exist) throw new CustomException('Already exist', 200);
    Role.create({
        name,
        status
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Role Added',
            'Response': [dat],
            'errors': '',
        })
    })
}));

/*
            2. Get all Roles
    ___________________________________________________
*/
router.get('/getall',  asyncMiddleware(async (req, res) => {
    const allRoles = await Role.findAll()
    let tmpArr = [];
    allRoles.map((role, index)=>{
        let tmpObj = {
            id : role.id,
            name: role.name,
            status: role.status,
        }
        tmpArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'All Roles',
        'Response': tmpArr,
        'errors': '',
    })
}));

/*
            3. Update status of role using Role ID
    ___________________________________________________
*/
router.put('/updatestatus/:cId', asyncMiddleware(async (req, res) => {
    const catId = req.params.cId;
    const { status } = req.body;
    Role.update({
        status: status
    }, { where: { id: catId } })
    .then(rt=>{    
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Role Updated',
            'Response': [],
            'errors': '',
        });
    })
}));

module.exports = router;