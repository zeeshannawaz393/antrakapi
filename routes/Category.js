const express = require('express');
const router = express();
const {Category, CardBrand, AppSettings, Charge} = require('../models');

// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');

const multer = require('multer')
const path = require('path')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null,`Images/Categories`)
    },
    filename: (req, file, cb) => {
        cb(null,path.basename(file.originalname, path.extname(file.originalname)) + '-'  + Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({
    storage: storage,
    limits: { fileSize: '1000000' },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/
        const mimeType = fileTypes.test(file.mimetype)  
        const extname = fileTypes.test(path.extname(file.originalname))

        if(mimeType && extname) {
            return cb(null, true)
        }
        cb('Give proper files formate to upload')
    }
}).single('image')

/*
            1. Add Category of Package Type
    ________________________________________________________
*/
router.post('/add', upload, asyncMiddleware(async (req, res) => {
    const {name, adult_18plus} = req.body;
    const status=true;
    //console.log(req.protocol, req.get('host') ,req.path);
    const exist = await Category.findOne({where: {name: name, status: 1}});
    //return res.json(exist)
    if(exist){
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Category already exist',
            'Response': [],
            'errors': '',
        })
    }
    Category.create({
       name,
       status,
       Image:`${req.file.path}`,
       adult_18plus,
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Category Added',
            'Response': ['New category has been added'],
            'errors': '',
        })
    })
}));

/*
            2. Get all categories along with some texts for Customer App
    ____________________________________________________________________
*/
router.get('/getall', asyncMiddleware(async (req, res) => {
    const insuranceText = await AppSettings.findOne({where: { title: 'Insurance Text'}});
    const load_unloadCharges = await Charge.findOne({where: { title: 'load_unload'}});
    const InsuranceCharges = await Charge.findOne({where: { title: 'Insurance'}});
    let load_unload_chargesText = `The charges for load/unload is ${load_unloadCharges.amount} US$ per ${load_unloadCharges.value} pounds(lbs)`;
    let insurance_chargesText = `The charges for insurance is ${InsuranceCharges.amount}${InsuranceCharges.value} of worth of package`
    Category.findAll({where: {status: 1}})
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'All categories',
            'Response': [{
                insuranceText: insuranceText.value,
                load_unload_chargesText,
                insurance_chargesText,
                categories: dat
            }],
            'errors': '',
        })
    })
}));

// 3. Update Category Status
/*
            3. Update Status of Category
    _______________________________________________________________
*/ 
router.put('/updatestatus/:cId', asyncMiddleware(async (req, res) => {
    const catId = req.params.cId;
    const {status} = req.body; 
    Category.update({status: status}, {where: {id: catId}})
        .then((dat)=>{
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Status Updated ',
                'Response': [{msg: `Status updated`}],
                'errors': '',
            });
        })
        .catch((err)=>{
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Status Update Failed',
                'Response': [],
                'errors': 'Failed to update the Status',
            });
        });

}));

/*
            4. Get all categroies for Admin Panel
    ________________________________________________________________
*/
router.get('/getallcat', asyncMiddleware(async (req, res) => {
    //const insuranceText = await AppSettings.findOne({where: { title: 'Insurance Text'}})
    Category.findAll({where: {status: 1}})
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'All categories',
            'Response': {
                categories: dat
            },
            'errors': '',
        })
    })
    .catch(err =>{
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Error getting categories',
            'Response': [],
            'errors': '',
        })
    })
}));

/*
             --------------------------------------
            |    ADD CARD BRANDS FOR DISPLAY       |
             --------------------------------------

*/
// I guess I might need a new route for this ... 
router.post('/addcardbrand', upload, asyncMiddleware(async (req, res) => {
    const {title} = req.body;
    CardBrand.create({
       title,
       image:`${req.file.path}`,
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Card Brand Added',
            'Response': ['Card Brand has been added'],
            'errors': '',
        })
    })
}));

module.exports = router;