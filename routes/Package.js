const express = require('express');
const router = express();
const {Package, Category, Booking, Coupons, Charge} = require('../models');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');


/*
            1. Add Package
    ____________________________________________________________
    a. Check if the package is single or multiple
    b. If single, add it to the table and return Success msg
    c. If not, Create multiple entries and return success msg
*/
router.post('/add', asyncMiddleware(async (req, res) => {
    const {package} = req.body;
    if(package.length === 1){
        Package.create(package[0])
            .then((dat)=>{
                // For limiting the values upto 2 
                let tot = Math.round((dat.total * 100)) / 100;
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Package Added',
                    'Response': [{
                        id : [dat.id],
                        total : [`${tot}`]
                    }],
                    'errors': '', 
                })
            })
    }
    else {
        Package.bulkCreate(package)
        .then((dat)=>{
            let outArr =[];
            let priceArr =[];
            dat.map((d,i)=>{
                outArr[i]= d.id;
                // For limiting the values upto 2 
                let tot = Math.round((d.total * 100)) / 100;
                priceArr[i] = `${tot}`;
            })
            console.log(outArr)
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Mutliple Packages Added',
                'Response': [{
                    id: outArr,
                    total: priceArr,
                }],
                'errors': '',
            })    
        })
    }
}));

/*
            2. Get All Packages
    ______________________________________________________
*/
router.get('/getall', asyncMiddleware(async (req, res) => {
    const packages = await Package.findAll( {include: [{model: Category}]});
    let outArr =[];
    if(!packages){ return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No Package Found',
            'Response':[],
            'errors': '',
        });
    }
    packages.map((pack, index)=>{
        let tmpObj ={
            id : pack.id,
            weight : pack.weight,
            dimensions : `${pack.length} *${pack.width} * ${pack.height}`,
            insured : pack.insurance,
            estWorth : pack.estWorth,
            total: pack.total,
            category: pack.Category.name
        };
        outArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'All Packages',
        'Response': outArr,
        'errors': '',
    });
})); 

/*
            3. Update Packages
    ____________________________________________________
    a. same as 1
*/
router.put('/update/:bookid', asyncMiddleware(async (req, res) => {
    const BookingId = req.params.bookid;
    const done = Package.destroy({where: {BookingId: BookingId}})
    const {package} = req.body;
    if(package.length === 1){
        Package.create(package[0])
            .then((dat)=>{
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Package Added',
                    'Response': [{
                        id: [dat.id],
                        total: [`${dat.total}`]
                    }],
                    'errors': '', 
                })
            })
    }
    else {
        Package.bulkCreate(package)
        .then((dat)=>{
            let outArr =[];
            let priceArr =[];
            dat.map((d,i)=>{
                outArr[i]=d.id;
                priceArr[i] = `${d.total}`;
            })
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Mutliple Packages Added',
                'Response': [{
                    id: outArr,
                    total: priceArr,
                }],
                'errors': '',
            })    
        })
    }
}));

/*
            4. Delete a package
    TO DO: GET THE ADMIN COMMISSION AND use it to cal est Earning
    _____________________________________________________
    a. Delete the Package from the table
    b. Get all the remaining packages and store the ids and prices in arrays
    c. Calculate the new total and new earning of driver
    d. Check if Coupon is applied or not
    e. If not, Update the booking and Return --> Success Msg
    f. Find the coupon type
    g. If it is flat, subtract the discount from both and update the booking and return --> Success Msg
    h. If it is amount dependent, check if still applies or not
    i. If applies, subtract the discount from both and update the booking and return --> Success Msg
    j. if not, update the booking and return --> Success Msg
*/
router.post('/delete', asyncMiddleware(async (req, res) => {
    const {packageId, bookingId, loadAmount} = req.body;
    // getting pckg to delete
    const pckg = await Package.findOne({where: {id: packageId}});
    const Insurance = await Charge.findOne({where: {title: 'Insurance'}});
    const adminCommission = await Charge.findOne({where: {title: 'Admin Commission'}});
    const load_unload_rate = await Charge.findOne({where: {title: 'load_unload'}});
    let adminCommissionPercent = 1 - (parseFloat(adminCommission.amount)/100);
    //Do calculations to determine the amount of package
    let pckgAmount = pckg.total;
    let pckgEstWorth = pckg.estWorth;
    let totalWeight = pckg.weight;
    let load_Amount=0;
    // Deleting the Package
    Package.destroy({where: {id: packageId}})
    .then(()=>{
        // getting package IDs and their price to return to the front end 
        Package.findAll({where: { BookingId: bookingId }})
            .then((pkg)=>{
                let tmpArr =[];
                let priceArr = [];
                pkg.map((pckg,index)=>{
                    tmpArr[index] = pckg.id;
                    priceArr[index] = `${pckg.total}`;
                })
                // Now manipulating the Booking (updating price and est Driver earning)
                Booking.findOne({where: {id: bookingId}})
                    .then((dat)=>{
                        // Caculate load amount for the removed package
                        if (totalWeight <= parseFloat(load_unload_rate.value)) {
                            load_Amount = load_Amount + parseFloat(load_unload_rate.amount)
                            //console.log('volume less', weight_Amount);
                        }
                        else {
                            let extraload= totalWeight - parseFloat(load_unload_rate.value);
                            let extraUnits = extraload / parseFloat(load_unload_rate.value);
                            //console.log(Math.ceil(extraUnits));
                            load_Amount = load_Amount + parseFloat(load_unload_rate.amount) + (extraUnits * parseFloat(load_unload_rate.amount))
                        }
                        load_Amount= Math.round((load_Amount * 100)) / 100;
                        // Calculate new total 
                        let newTotal = parseFloat(dat.total) - pckgAmount - load_Amount;
                        newTotal= Math.round((newTotal * 100)) / 100;
                        let newEstDriverEarning =  parseFloat(dat.estDriverEarning) - (( pckgAmount * adminCommissionPercent))  - (load_Amount *adminCommissionPercent);
                        newEstDriverEarning= Math.round((newEstDriverEarning * 100)) / 100;
                        // Subtracting current from previous
                        offLoadCharge = loadAmount - load_Amount;
                        console.log(newTotal,newEstDriverEarning,offLoadCharge)
                        //return res.json({newTotal, newEstDriverEarning, load_Amount})
                        // Check if coupon Applied
                        if(dat.CouponId){
                            Coupons.findOne({where: {id: dat.CouponId}})
                                .then(coup=>{
                                    // Checking the type of coupon
                                    if(coup.type === 'Flat'){
                                        let newGrandTotal = newTotal - parseFloat(coup.discount);
                                        let cEstEarning = newEstDriverEarning - parseFloat(coup.discount);
                                        Booking.update({total: newTotal, grandTotal: newGrandTotal, estDriverEarning: cEstEarning },{ where: {id: bookingId}})
                                            .then(()=>{
                                                 res.status(200).json({
                                                    'ResponseCode': '1',
                                                    'ResponseMessage': 'Package Deleted and Flat coupon updated',
                                                    'Response': [{
                                                        BookingId: `${bookingId}`,
                                                        total: `${newGrandTotal}`,
                                                        packageIds: [{
                                                            id: tmpArr,
                                                            total: priceArr,
                                                        }],
                                                        offLoadCharge: `${offLoadCharge}`, 
                                                        }],
                                                    'errors': '',
                                                })
                                            })
                                            .catch(err=>{
                                                res.status(200).json({
                                                    'ResponseCode': '0',
                                                    'ResponseMessage': 'Error in updating booking to DB',
                                                    'Response': [],
                                                    'errors': err,
                                                })
                                            })
                                    }
                                    // Conditional Coupon 
                                    else {
                                        if(newTotal > parseFloat(coup.conditionAmount)){
                                            // Applied
                                            let newGrandTotal = newTotal - parseFloat(coup.discount);
                                            let cEstEarning = newEstDriverEarning - parseFloat(coup.discount);
                                            Booking.update({total: newTotal, grandTotal: newGrandTotal, estDriverEarning: cEstEarning },{ where: {id: bookingId}})
                                            .then(()=>{
                                                 res.status(200).json({
                                                    'ResponseCode': '1',
                                                    'ResponseMessage': 'Package Deleted and CA coupon updated',
                                                    'Response': [{
                                                        BookingId: `${bookingId}`,
                                                        total: `${newGrandTotal}` ,
                                                        packageIds: [{
                                                            id: tmpArr,
                                                            total: priceArr,
                                                        }],
                                                        loadAmount: `${offLoadCharge}`,
                                                        }],
                                                    'errors': '',
                                                })
                                            })
                                            .catch(err=>{
                                                res.status(200).json({
                                                    'ResponseCode': '0',
                                                    'ResponseMessage': 'Error in updating booking to DB',
                                                    'Response': [],
                                                    'errors': err,
                                                })
                                            })
                                            
                                        }
                                        else{
                                            // Could not Apply Coupon 
                                            Booking.update({total: newTotal, CouponId: null},{ where: {id: bookingId}})
                                            .then(()=>{
                                                 res.status(200).json({
                                                    'ResponseCode': '1',
                                                    'ResponseMessage': 'Coupon not applicablle',
                                                    'Response': [{
                                                        BookingId: `${bookingId}`,
                                                        total: `${newTotal}` ,
                                                        packageIds: [{
                                                            id: tmpArr,
                                                            total: priceArr,
                                                        }],
                                                        loadAmount: `${offLoadCharge}`,
                                                        }],
                                                    'errors': '',
                                                })
                                            })
                                            .catch(err=>{
                                                res.status(200).json({
                                                    'ResponseCode': '0',
                                                    'ResponseMessage': 'Error in updating booking to DB',
                                                    'Response': [],
                                                    'errors': err,
                                                })
                                            })
                                        }
                                    }
                                })
                                .catch(err=>{
                                    res.status(200).json({
                                        'ResponseCode': '0',
                                        'ResponseMessage': 'No coupon found against this ID',
                                        'Response': [],
                                        'errors': err,
                                        })
                                })
                        }else{
                            Booking.update({total: newTotal, grandTotal: newTotal, estDriverEarning : newEstDriverEarning},{ where: {id: bookingId}})
                                .then(()=>{
                                     res.status(200).json({
                                        'ResponseCode': '1',
                                        'ResponseMessage': 'Package Deleted',
                                        'Response': [{
                                            BookingId: `${bookingId}`,
                                            total: `${newTotal}` ,
                                            packageIds: [{
                                                            id: tmpArr,
                                                            total: priceArr,
                                                        }],
                                            }],
                                            loadAmount: `${offLoadCharge}`,
                                            'errors': '',
                                    })
                                })
                                .catch(err=>{
                                    res.status(200).json({
                                        'ResponseCode': '0',
                                        'ResponseMessage': 'Error in updating booking to DB',
                                        'Response': [],
                                        'errors': err,
                                    })
                                })
                        }
                    })
                    // .catch(err=>{
                    //     res.status(200).json({
                    //         'ResponseCode': '0',
                    //         'ResponseMessage': 'No Booking found against this ID',
                    //         'Response': [],
                    //         'errors': err,
                    //     })
                    // })
            })
            .catch(err=>{
                res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Deletion Failed',
                    'Response': [],
                    'errors': err,
                })
            })
    })
}));

module.exports = router;