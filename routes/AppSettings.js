const express = require('express');
const router = express();
const { AppSettings, WebsiteSettings, WebMessages } = require('../models');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');

// Importing multer to add Images
const multer = require('multer')
const path = require('path')
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, `Images/AppSettings`)
    },
    filename: (req, file, cb) => {
        cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
    }
})
const upload = multer({
    storage: storage,
    limits: { fileSize: '1000000' },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/
        const mimeType = fileTypes.test(file.mimetype)
        const extname = fileTypes.test(path.extname(file.originalname))

        if (mimeType && extname) {
            return cb(null, true)
        }
        cb('Give proper files formate to upload')
    }
}).single('value')

/* 
            1. Updating the current information in the Appsettings
    _________________________________________________________________________
    a. Get the the id and msg from the user and simply update it
*/
router.put('/addDM', asyncMiddleware(async (req, res) => {
    const { id, value } = req.body;
    AppSettings.update({
        value,
    }, { where: { id: id } })
        .then((dat) => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Information Updated',
                'Response': [],
                'errors': '',
            })
        })
}));

/* 
            2. Adding the image in the Appsettings
    _________________________________________________________________________
    a. Get the the title and image from the user and simply create a new 
    entry in the table
*/
router.post('/addHomeImage', upload, asyncMiddleware(async (req, res) => {
    const { title } = req.body;
    AppSettings.create({
        title,
        value: req.file.path,
    })
        .then((dat) => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Home Page Image Added',
                'Response': ['Home Page Image Added'],
                'errors': '',
            })
        })
}));


/*
            3. Get All App settings
    _________________________________________________________________
    a. Get all the settings from the table & fitlter then on the 
        basis of its title. Similiar, entries are placed in the arrays
*/
router.get('/getall', asyncMiddleware(async (req, res) => {
    const appSettings = await AppSettings.findAll();
    //return res.json(appSettings);
    let displayMessage = {}, email = {}, contact = {}, insurance = {}, insuranceLong = {}, loadUnload = {}, customerCancelReasons = [], driverCancelReasons = [];
    appSettings.map((val, index) => {
        if (val.title === 'DM for CApp') {
            displayMessage.id = val.id;
            displayMessage.text = val.value;
        }
        if (val.title === 'email') {
            email.id = val.id;
            email.text = val.value;
        }
        if (val.title === 'contact') {
            contact.id = val.id;
            contact.text = val.value;
        }
        if (val.title === 'Insurance Text') {
            insurance.id = val.id;
            insurance.text = val.value;
        }
        if (val.title === 'insuranceLongText') {
            insuranceLong.id = val.id;
            insuranceLong.text = val.value;
        }
        if (val.title === 'loadUnloadText') {
            loadUnload.id = val.id;
            loadUnload.text = val.value;
        }
        if (val.title === 'Cancel Reason') {
            let tmpObj = {
                id: val.id,
                text: val.value,
            };
            customerCancelReasons.push(tmpObj)
        }
        if (val.title === 'Cancel Reason Driver') {
            let tmpObj = {
                id: val.id,
                text: val.value,
            };
            driverCancelReasons.push(tmpObj)
        }
    });
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'App settings',
        'Response': {
            msgCustApp: displayMessage,
            email: email,
            contact: contact,
            insuranceText: insurance,
            insuranceLongText: insuranceLong,
            loadUnloadText: loadUnload, 
            customerCancelReasons,
            driverCancelReasons,
        },
        'errors': '',
    });
}))

/* 
            4. Updating the current information in the Appsettings
    _________________________________________________________________________
    a. Get the the id and msg from the user and simply update it
*/
router.put('/update/app', asyncMiddleware(async (req, res) => {
    const { id, newValue } = req.body;
    AppSettings.update({ value: newValue }, { where: { id: id } })
        .then(() => {
            return res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'App settings Updated',
                'Response': [],
                'errors': '',
            });
        })
}))
/*
        ---------------------------------------
        |        WEBSITE SETTINGS              |
        ---------------------------------------
*/

/*
            5. Add Settings
    _______________________________________________
    a. Get title and value(msg) from the request body
    and add a new entry in the table 
*/
router.post('/add/web', asyncMiddleware(async (req, res) => {
    const { title, value } = req.body;
    WebsiteSettings.create({ title, value })
        .then(() => {
            res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Websetting Added',
                'Response': [],
                'errors': '',
            });
        })

}));

/*
          6. Get settings
  __________________________________________________

*/
router.get('/get/web', asyncMiddleware(async (req, res) => {
    const webset = await WebsiteSettings.findAll();
    res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'All Website settings',
        'Response': webset,
        'errors': '',
    })

}));

/*
            7. Update Settings
    __________________________________________________
*/
router.put('/update/web', asyncMiddleware(async (req, res) => {
    const { id, value } = req.body;
    WebsiteSettings.update({ value: value }, { where: { id: id } })
        .then(() => {
            res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Updated',
                'Response': [],
                'errors': '',
            })
        });

}));

/*
            8. Get Banners 
    _______________________________________________________
    a. Return a list of all the banners
*/
router.get('/banners', asyncMiddleware(async (req, res) => {
    const banner = await AppSettings.findAll({ where: { title: 'Banner', status: 1 } });
    // res.send(about);
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Bannner Images',
        'Response': banner,
        'errors': '',
    });



}));

/*
            9. Add Banner
    _______________________________________________________ 
*/
router.post('/addbanner', upload, asyncMiddleware(async (req, res) => {
    AppSettings.create({
       title: 'Banner',
       status: true,
       value:`${req.file.path}`,
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Banner Added',
            'Response': 'Banner Added!',
            'errors': '',
        })
    })
}));

/*
            10. Delete Banner
    _________________________________________________
    a. Not actually deleting the bannner but changing its status to 0
*/
router.put('/delete_banner/:id' ,asyncMiddleware(async (req, res) => {
    const exist = await AppSettings.findAll({where: {title: 'Banner'}})
    //return res.json(exist.length);
    if(exist.length === 1){
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Atleast one banner is required. You cant delete it',
            'Response': '',
            'errors': '',
        })
    }
    AppSettings.destroy({where: { id: req.params.id }})
    .then(()=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Banner Deleted',
            'Response': 'Deleted Banner',
            'errors': '',
        })
    });
    
   
}));
/*
            11. Send message from Website
    __________________________________________________________
*/
router.post('/sendmessage' ,asyncMiddleware(async (req, res) => {
    const {firstName, lastName, email, message} = req.body;
    WebMessages.create({firstName, lastName, email,status: true ,message})
    .then(dat=>{
        return res.json({
            'ResponseCode': '1',
            'ResponseMessage': 'Sent successfully',
            'Response': '',
            'errors': '',
        }); 
    })
    .catch(err=>{
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'Error in sending your request',
            'Response': '',
            'errors': '',
        }); 
    })
}));
/*
            12. Get messages sent through Website
    __________________________________________________________
*/
router.get('/getallmessages' ,asyncMiddleware(async (req, res) => {
    const messages = await WebMessages.findAll({where: {status: true}});
    //return res.json(messages);
    return res.json({
            'ResponseCode': '1',
            'ResponseMessage': 'All pending Messgaes',
            'Response': messages,
            'errors': '',
        });
}));

/*
            13. Change status if it is responded
    __________________________________________________________
*/
router.put('/responded/:id' ,asyncMiddleware(async (req, res) => {
    WebMessages.update({status: false}, {where: {id: req.params.id}})
    .then(done=>{
        return res.json({
            'ResponseCode': '1',
            'ResponseMessage': 'Responded successfully',
            'Response': '',
            'errors': '',
        });
    }).
    catch(err=>{
        return res.json({
            'ResponseCode': '1',
            'ResponseMessage': 'Error in response',
            'Response': '',
            'errors': '',
        });
    })
}));

/*
            12. Get messages sent through Website
    __________________________________________________________
*/
router.get('/getrepliedmessages' ,asyncMiddleware(async (req, res) => {
    const messages = await WebMessages.findAll({where: {status: false}});
    //return res.json(messages);
    return res.json({
            'ResponseCode': '1',
            'ResponseMessage': 'All pending Messgaes',
            'Response': messages,
            'errors': '',
        });
}));

module.exports = router;