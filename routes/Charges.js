const express = require('express');
const router = express();
const {Charge} = require('../models');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');

/*
            1. Add Charges
    ________________________________________________________
*/
router.post('/add', asyncMiddleware(async (req, res) => {
    const {title, value, amount} = req.body;
    Charge.create({
        title,
        value,
        amount,
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Charge Added',
            'Response': ['New type of Charge Added'],
            'errors': '',
        })
    })
}));

/*
            2. Get All charges
    ________________________________________________________
*/
router.get('/get',  asyncMiddleware(async (req, res) => {
    const charges = await Charge.findAll();
    if(charges)
    {
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'All Charges Data',
            'Response': charges,
            'errors': '',
        })
    }
    else
    {
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'No Record Found',
            'Response': 'No Record Found',
            'errors': '',
        })
    }
}));

/*
            3. Update Charges
    ________________________________________________________
    a. 
*/
router.put('/update', asyncMiddleware(async (req, res) => {
    const { id, amount, value } = req.body;
    Charge.update({ amount: amount, value: value }, { where: { id: id } })
        .then(result => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Charge Updated',
                'Response': '',
                'errors': '',
            });
        })
        .catch(err=>{
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Error Updating',
                'Response': '',
                'errors': 'Error Updating',
            })
        })
}));
router.put('/update_amount', asyncMiddleware(async (req, res) => {
    const { id, amount } = req.body;
    Charge.update({ amount: amount }, { where: { id: id } })
        .then(result => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Charge Updated',
                'Response': '',
                'errors': '',
            });
        })
        .catch(err=>{
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Error Updating',
                'Response': '',
                'errors': 'Error Updating',
            })
        })
}));
router.put('/update_value', asyncMiddleware(async (req, res) => {
    const { id, value } = req.body;
    Charge.update({ value: value }, { where: { id: id } })
        .then(result => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Charge Updated',
                'Response': '',
                'errors': '',
            });
        })
        .catch(err=>{
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Error Updating',
                'Response': '',
                'errors': 'Error Updating',
            })
        })
}));
module.exports = router;