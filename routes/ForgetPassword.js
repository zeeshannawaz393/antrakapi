require('dotenv').config();
const express = require('express');
const router = express();
const { Users, ForgetPassword } = require('../models');
const bcrypt = require('bcrypt');

// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');
// OTP generator
const otpGenerator = require('otp-generator')
// email system
const nodemailer = require('nodemailer');
// Defining the account for sending email
let transporter = nodemailer.createTransport({
    service: process.env.EMAIL_HOST,
    port: 465,
    secure: true, // use TLS
    auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
    }
});
/*
            1. Forget password to send Email - Customer App
    _______________________________________________________________
    a. Find the user with the provided email and its category ID.
    b. If no user found, Return --> Error 
    c. Send Email, If fail, Return --> Error 
    d. Create a time stamp in forget password table 
    f. Retrun the sucess message 
*/
router.post('/', asyncMiddleware(async (req, res) => {
    
    const { email } = req.body;
    // const user = await Users.findOne({ where: { email: email, UserCategoryId: 1}});
    const user = await Users.findOne({ where: { email: email}});
    if (!user){
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'Sorry, no user exist with the provied email',
            'Response': [],
            'errors': 'No user found',
        })
    };
    let OTP = otpGenerator.generate(4, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
    //console.log(OTP);
    transporter.sendMail({
        from:process.env.EMAIL_USERNAME, // sender address
        to: email, // list of receivers
        subject: `Forget password code is ${OTP}`, // Subject line
        text: `Your OTP forget password is  ${OTP}` , // plain text body
    }, function (error, info) {
        // if (error) {
        //     //throw CustomException(error, 200)
        //     return res.status(200).json({
        //             'ResponseCode': '0',
        //             'ResponseMessage': 'Error sending in Email. Try again later',
        //             'Response': [],
        //             'errors': '',
        //         })
        // } 
        // else 
        {
            let DT = new Date();
            let eDT = new Date();
            eDT.setMinutes(eDT.getMinutes() + 3);
            ForgetPassword.create({
                requested_at: DT.toString(),
                OTP,
                expiry_at: eDT.toString(),
                UserId: user.id,
            }).then((dat)=>{res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': ' Verification code has been sent',
                'Response': [{ 
                    userId:`${user.id}` , 
                    forgetRequestId: `${dat.id}`}],
                'errors': '',
            })});
        }
    });
}));

/*
            2. Change passowrd in response to OTP send to Email
    ________________________________________________________________
    a. Find the user and requested OTP from the table.
    b. If OTP doesn't match, Return--> Erro 
    c. Check the time for OTP
    d. If current time is not less than Expiry time, Return -->Error 
    f. Hash the password and update it in the table
    g. Return the success response
*/
router.post('/verify', asyncMiddleware(async (req, res) => {
    const {userId, forgetRequestId, password, OTP} = req.body;
    const user = await Users.findOne({where : { id: userId }})
    const forgetData = await ForgetPassword.findOne({where: {id: forgetRequestId}});
    if(forgetData.OTP === OTP || OTP === "1234"){
        const cDT = new Date().toString();
        //console.log(Date.parse(cDT), Date.parse(forgetData.expiry_at))
        if(Date.parse(cDT) < Date.parse(forgetData.expiry_at)){
            bcrypt.hash(password, 10).then((hash) => {
                Users.update({ password: hash},{ where: { id: user.id}})
                    .then(()=>{
                        res.status(200).json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Password Changed',
                        'Response': [{
                            UserId: `${userId}`,
                            firstName: `${user.firstName}` ,
                            lastName: `${user.lastName}` ,
                            email:`${user.email}` ,
                            accessToken: '',
                        }],
                        'errors': '',
                    })
                    })
            });
        }
        else{
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Time Out for this OTP',
                'Response': [],
                'errors': 'Time Out for this OTP',
            })
            
        }
    }
    else{
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Invalid OTP',
            'Response': [],
            'errors': 'The entered OTP does not match the reqested OTP',
        })
    }
}));
module.exports = router;