const express = require('express');
const router = express();
const {BookingStatus} = require('../models');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');

/*
            1. Add Booking Status
    ______________________________________________________ 
*/
router.post('/add',  asyncMiddleware(async (req, res) => {
    const {name, status } = req.body;
    const exist = await BookingStatus.findOne({where: {name: name}})
    if(exist) throw new CustomException('Already exist', 200);
    BookingStatus.create({
        name,
        status
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Booking Status Added',
            'Response': [dat],
            'errors': '',
        })
    })
}));
module.exports = router;