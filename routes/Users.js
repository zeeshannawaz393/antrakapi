require('dotenv').config();
const express = require('express');
const router = express();
const { Users, DriverDetails,PaymentMethod, VehiclesType,WebMessages, EmailVerification, UserCategory, Address, WebsiteSettings, Booking, CardBrand, AppSettings, Package, Category, BookingStatus, CancelHistory, BlackList, Role, Permissions } = require('../models');
const bcrypt = require('bcrypt');
const { sign } = require('jsonwebtoken');
const { validateToken } = require('../middlewares/AuthorizationMW');
const { Op } = require("sequelize");
const { verify } = require('jsonwebtoken');
const stripe = require('stripe')(process.env.STRIPE_KEY);
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');
const axios = require('axios');
var striptags = require('striptags');
// OTP generator
const otpGenerator = require('otp-generator')
// email system
const nodemailer = require('nodemailer');
// Defining the account for sending email
const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: 465,
    secure: true, // use TLS
    auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
    },
});

let FCM = require('fcm-node');
let fcm = new FCM(process.env.FIREBASE_SERVER_KEY);


// FOR CREATING PDF 
// Importing modules
const PDFDocument = require('pdfkit');
const fs = require('fs')

/*
            1. Register Customer
    __________________________________________________________________________
    a. Check if user exist or not with same email or phoneNum
    b. If exists, Return --> Error
    c. Check if signin is through Google or not
    d. If not, send an email, create stripe customer and create user and Return --> Success msg
    e. Create stripe customer and create user and Return --> Success msg 
*/
router.post('/register', asyncMiddleware(async (req, res) => {
    // getting data from request
    let { firstName, lastName, email, countryCode, phoneNum, password, gkey, deviceToken } = req.body;
    // concat phone number with country code
    phoneNum = countryCode.concat(phoneNum);
    // getting the Category of User
    const uCat = await UserCategory.findOne({ where: { name: 'User' } })
    // Check if the user exists or not with same input data
    const user = await Users.findOne({ where: { [Op.or]: [{ email: email }, { phonenum: phoneNum }] } });
    if (user) {
        if (user.email === email && user.UserCategoryId === 2) { throw new CustomException('This email has already been taken', 200) }
        else if (user.phoneNum === phoneNum && user.UserCategoryId === 2) { throw CustomException('This phone number has already been taken', 200) }
    }

    // Checking if the signIn is thorugh Google or not
    if (gkey === '0') {
        let OTP = otpGenerator.generate(4, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
        // Sending verification email alongwith OTP
        transporter.sendMail({
            from: process.env.EMAIL_USERNAME, // sender address
            to: email, // list of receivers
            subject: `Your OTP for Antrak is ${OTP}`, // Subject line
            text: `Your OTP for Antrak is ${OTP}`, // plain text body
        }, function (error, info) {
            // if (error) {
            //     return res.status(200).json({
            //         'ResponseCode': '0',
            //         'ResponseMessage': 'Error in sending email',
            //         'Response': [],
            //         'errors': 'Error',
            //     })
            // }
            // else
            {
                stripe.customers.create({ email: email })
                    .then(customer => {
                        bcrypt.hash(password, 10).then((hash) => {
                            Users.create({
                                firstName,
                                lastName,
                                email,
                                phoneNum,
                                password: hash,
                                UserCategoryId: uCat.id,
                                deviceToken,
                                stripeCustomerId: customer.id,
                                status: true,
                            })
                                .then((dat) => {
                                    let DT = new Date().toString();
                                    EmailVerification.create({
                                        requested_at: DT,
                                        OTP,
                                        UserId: dat.id,
                                    })
                                        .then(succ => {
                                            res.json({
                                                'ResponseCode': '1',
                                                'ResponseMessage': 'Email for verification has been sent',
                                                'Response': [{
                                                    UserId: `${dat.id}`,
                                                    firstName: `${dat.firstName}`,
                                                    lastName: `${dat.lastName}`,
                                                    email: `${dat.email}`,
                                                    accessToken: '',
                                                }],
                                                'errors': '',
                                            })
                                        })
                                        .catch(err => {
                                            res.status(200).json({
                                                'ResponseCode': '0',
                                                'ResponseMessage': 'Something went wrong. Please try again later',
                                                'Response': [],
                                                'errors': `Error`,
                                            });
                                        })
                                })
                                .catch(err => {
                                    res.status(200).json({
                                        'ResponseCode': '0',
                                        'ResponseMessage': 'Something went wrong. Please try again later',
                                        'Response': [],
                                        'errors': `Error`,
                                    });
                                })
                        })
                            .catch(err => {
                                res.status(200).json({
                                    'ResponseCode': '0',
                                    'ResponseMessage': 'Something went wrong. Please try again later',
                                    'Response': [],
                                    'errors': `Error`,
                                });
                            })
                    })
                    .catch(err => {
                        //console.log(err)
                        return res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': err.raw.message,
                            'Response': [],
                            'errors': `Srtipe Error`,
                        });
                    })
            }
        }
        );
    }
    else {
        stripe.customers.create({
            email: email,
        }).then(customer => {
            bcrypt.hash(password, 10).then((hash) => {
                Users.create({
                    firstName,
                    lastName,
                    email,
                    phoneNum,
                    password: hash,
                    UserCategoryId: uCat.id,
                    email_verified_at: 'Google Sign In',
                    deviceToken,
                    stripeCustomerId: customer.id,
                    status: 1,
                }).then((dat) => {
                    const accessToken = sign({ username: dat.email, id: dat.id }, 'Important Text')
                    res.status(200).json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Logged In successfull',
                        'Response': [{
                            UserId: `${dat.id}`,
                            firstName: `${dat.firstName}`,
                            lastName: `${dat.lastName}`,
                            email: `${dat.email}`,
                            accessToken: `${accessToken}`,
                        }],
                        'errors': '',
                    })
                })
            })
        });
    }
}));


/*
            2. Login Customer
    ________________________________________________________
    a. Check if user exits or not
    b. If not, Return --> Error
    c. Compare the password, If it doesnt match, Return --> Error
    d. Check If email verified, If not, Return --> Error
    e. Update the device Token, Check if accessToken is blacklisted or not
    f. If Yes, Destroy it and Login 
    e. Just login
*/
router.post('/login', asyncMiddleware(async (req, res) => {
    const { email, password, deviceToken } = req.body;
    const user = await Users.findOne({ where: { email: email, userCategoryId: 2, deleted: null } });
    if (!user) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Sorry. We cant find an account with this email',
            'Response': [{
                "UserId": '',
                "firstName": '',
                "lastName": '',
                "email": '',
                "accessToken": '',
            }],
            'errors': 'Trying to sign up?',
        })
    }
    if (user.UserCategoryId === 3) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Sorry. This email is already taken',
            'Response': [{
                "UserId": '',
                "firstName": '',
                "lastName": '',
                "email": '',
                "accessToken": '',
            }],
            'errors': 'Trying to sign up?',
        })
    }
    bcrypt.compare(password, user.password)
        .then((match) => {
            if (!match) {
                return res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': `Sorry, the password for ${email} is incorrect`,
                    'Response': [{
                        "UserId": '',
                        "firstName": '',
                        "lastName": '',
                        "email": '',
                        "accessToken": '',
                    }],
                    'errors': 'Incorrect Password',
                })
            }
            if (!user.email_verified_at) {
                res.status(200).json({
                    'ResponseCode': '2',
                    'ResponseMessage': 'Please verify your email to login',
                    'Response': [{
                        UserId: `${user.id}`,
                        "firstName": `${user.firstName}`,
                        "lastName": `${user.lastName}`,
                        "email": `${user.email}`,
                        "accessToken": '',
                    }],
                    'errors': 'Verify email',
                });
            }
            else {
                Users.update({ deviceToken: deviceToken }, { where: { id: user.id } });
                const accessToken = sign({ username: user.email, id: user.id }, 'Important Text');
                BlackList.findOne({ where: { accessToken: accessToken } })
                    .then(dat => {
                        // Check if it is blocked by Admin or not
                        if (!user.status) {
                            return res.status(200).json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Access denied by admin',
                                'Response': [{
                                    "UserId": ``,
                                    "firstName": ``,
                                    "lastName": ``,
                                    "email": ``,
                                    "accessToken": ``,
                                }],
                                'errors': 'Log in failed',
                            });
                        }
                        if (dat) {
                            BlackList.destroy({ where: { accessToken: accessToken } });
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Log in successful',
                                'Response': [{
                                    "UserId": `${user.id}`,
                                    "firstName": `${user.firstName}`,
                                    "lastName": `${user.lastName}`,
                                    "email": `${user.email}`,
                                    "accessToken": `${accessToken}`,
                                }],
                                'errors': '',
                            });
                        }
                        else {
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Log in successful',
                                'Response': [{
                                    "UserId": `${user.id}`,
                                    "firstName": `${user.firstName}`,
                                    "lastName": `${user.lastName}`,
                                    "email": `${user.email}`,
                                    "accessToken": `${accessToken}`,
                                }],
                                'errors': '',
                            });
                        }
                    });
            }
        })
        .catch((err) => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': `Sorry, the password for ${email} is incorrect`,
                'Response': [],
                'errors': 'Incorrect Password',
            });
        });
}));

/*
            3. Google Sign In API
    ______________________________________________________________
*/
router.post('/googlesignin', asyncMiddleware(async (req, res) => {
    const { email, deviceToken } = req.body;
    const user = await Users.findOne({ where: { email: email, userCategoryId: 2 } });
    if (!user) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Sorry. We cant find an account with this email',
            'Response': [{
                "UserId": '',
                "firstName": '',
                "lastName": '',
                "email": '',
                "accessToken": '',
            }],
            'errors': 'Trying to sign up?',
        })
    }
    if (user.UserCategoryId === 3) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Sorry. This email is already taken',
            'Response': [{
                "UserId": '',
                "firstName": '',
                "lastName": '',
                "email": '',
                "accessToken": '',
            }],
            'errors': 'Trying to sign up?',
        })
    }
    if (user) {
        let DT = new Date().toString();
        Users.update({ deviceToken: deviceToken, email_verified_at: DT }, { where: { email: email } })
            .then((dat) => {
                // Check if it is blocked by Admin or not
                if (!user.status) {
                    return res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Access denied by admin',
                        'Response': [{
                            "UserId": ``,
                            "firstName": ``,
                            "lastName": ``,
                            "email": ``,
                            "accessToken": ``,
                        }],
                        'errors': 'Log in failed',
                    });
                }
                const accessToken = sign({ username: user.email, id: user.id }, 'Important Text');
                BlackList.findOne({ where: { accessToken: accessToken } })
                    .then(result => {
                        if (result) {
                            BlackList.destroy({ where: { accessToken: accessToken } });
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Log in successful',
                                'Response': [{
                                    UserId: `${user.id}`,
                                    firstName: `${user.firstName}`,
                                    lastName: `${user.lastName}`,
                                    email: `${user.email}`,
                                    accessToken: `${accessToken}`,
                                }],
                                'errors': '',
                            });
                        }
                        else {
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Log in successful',
                                'Response': [{
                                    UserId: `${user.id}`,
                                    firstName: `${user.firstName}`,
                                    lastName: `${user.lastName}`,
                                    email: `${user.email}`,
                                    accessToken: `${accessToken}`,
                                }],
                                'errors': '',
                            });
                        }
                    })

            })
    }
}));

/*
            4. Update Password with old password
    _____________________________________________________________
*/
router.put('/updatepassword', validateToken, asyncMiddleware(async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    const user = req.user;
    const cUser = await Users.findOne({ where: { id: user.id } });
    bcrypt.compare(oldPassword, cUser.password)
        .then((match) => {
            if (!match) { throw new CustomException('Incorrect old password. Enter a valid password and try again', 200); }
            bcrypt.hash(newPassword, 10)
                .then((hash) => {
                    Users.update({ password: hash }, { where: { id: cUser.id } })
                        .then(
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Password updated',
                                'Response': ['Your password has been updated'],
                                'errors': '',
                            })
                        )
                })
        })
        .catch((err) => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Enter a valid password and try again',
                'Response': [],
                'errors': 'Incorrect old password',
            })
        })
}));

/*
            5. Update Profile of End User
    ______________________________________________________
    a. Phone number is allowed to update
    b. Check if that phone number is taken by some other user or not
    c. If not, then Update else Return --> Error
*/
router.put('/updateuser', validateToken, asyncMiddleware(async (req, res) => {
    let { firstName, lastName, countryCode, phoneNum } = req.body;
    const user = req.user;
    phoneNum = countryCode.concat(phoneNum);
    const cUser = await Users.findOne({ where: { id: user.id } });
    const existphoneNum = await Users.findOne({ where: { phoneNum: phoneNum, userCategoryId: 2 } })
    if (existphoneNum) {
        if (existphoneNum.id === cUser.id) {
            Users.update({ firstName, lastName, phoneNum }, { where: { id: user.id } })
                .then((dat) => {
                    let arr = phoneNum.split("-");
                    res.status(200).json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Profile updated',
                        'Response': [{
                            firstName: `${firstName}`,
                            lastName: `${lastName}`,
                            countryCode: `${countryCode}`,
                            phoneNum: `${arr[1]}`,
                        }],
                        'errors': '',
                    });
                })
        }
        else { throw new CustomException('Phone number already taken', 200) }
    }
    else {
        Users.update({ firstName, lastName, phoneNum }, { where: { id: user.id } })
            .then(() => {
                let arr = phoneNum.split("-");
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Profile Updated',
                    'Response': [{
                        firstName: `${firstName}`,
                        lastName: `${lastName}`,
                        countryCode: `${countryCode}`,
                        phoneNum: `${arr[1]}`,
                    }],
                    'errors': '',
                });
            })
    }
}))

/*
            6. Session API for Customer
    _________________________________________________________
    a.  
*/
router.post('/session', asyncMiddleware(async (req, res) => {
    const accessToken = req.header('accessToken');
    // Empty accessToken = user not logged in
    if (!accessToken) return res.json({
        'ResponseCode': '1',
        'sessionKey': '0',
        'ResponseMessage': 'Please login to continue',
        'Response': [{
            "UserId": "",
            "firstName": "",
            "lastName": "",
            "email": "",
            "phoneNum": "",
            "accessToken": "",
        }],
        'errors': 'You are not logged in',
    })
    BlackList.findOne({ where: { accessToken: accessToken } })
        .then(dat => {
            // AccessToken found in blackList = user logged out
            if (dat) {
                return res.json({
                    'ResponseCode': '1',
                    'sessionKey': '0',
                    'ResponseMessage': 'Please Login to continue',
                    'Response': [{
                        "UserId": "",
                        "firstName": "",
                        "lastName": "",
                        "email": "",
                        "phoneNum": "",
                        "accessToken": "",
                    }],
                    'errors': 'User not logged in',
                })
            }
            else {
                try {
                    const validToken = verify(accessToken, 'Important Text');
                    if (validToken) {
                        //const { id } = req.body;
                        Users.findOne({ where: { id: validToken.id } })
                            .then(user => {
                                if (!user) {
                                    return res.status(200).json({
                                        'ResponseCode': '1',
                                        'sessionKey': '0',
                                        'ResponseMessage': 'Access denied',
                                        'Response': [{
                                            "UserId": "",
                                            "firstName": "",
                                            "lastName": "",
                                            "email": "",
                                            "phoneNum": "",
                                            "accessToken": "",
                                        }],
                                        'errors': 'Please Signup to continue',
                                    });
                                }
                                else {
                                    if (!user.email_verified_at) {
                                        let OTP = otpGenerator.generate(4, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
                                        let DT = new Date().toString();
                                        EmailVerification.update({
                                            requested_at: DT,
                                            OTP: OTP,
                                        }, { where: { UserId: id } })
                                            .then(() => {
                                                transporter.sendMail({
                                                    from: process.env.EMAIL_USERNAME, // sender address
                                                    to: user.email, // list of receivers
                                                    subject: `Your OTP for Antrak is ${OTP}`, // Subject line
                                                    text: `Your OTP for Antrak is ${OTP}`, // plain text body
                                                    // html body
                                                }, function (error, info) {
                                                    if (error) {
                                                        throw new CustomException(error, 200)
                                                    }
                                                    else {
                                                        res.status(200).json({
                                                            'ResponseCode': '1',
                                                            'sessionKey': '1',
                                                            'ResponseMessage': 'Email for verification has been sent',
                                                            'Response': [{
                                                                UserId: `${user.id}`,
                                                                firstName: `${user.firstName}`,
                                                                lastName: `${user.lastName}`,
                                                                email: `${user.email}`,
                                                                accessToken: '',
                                                            }],
                                                            'errors': '',
                                                        });
                                                    }
                                                })
                                            })
                                    }
                                    else {
                                        //const accessToken = sign({ username: user.email, id: user.id }, 'Important Text');
                                        // Check if it is blocked by Admin or not
                                        // Check if it is blocked by Admin or not
                                        if (!user.status) {
                                            return res.status(200).json({
                                                'ResponseCode': '0',
                                                'ResponseMessage': 'Access denied by admin',
                                                'Response': [{
                                                    "UserId": ``,
                                                    "firstName": ``,
                                                    "lastName": ``,
                                                    "email": ``,
                                                    "accessToken": ``,
                                                }],
                                                'errors': 'Log in failed',
                                            });
                                        }
                                        res.status(200).json({
                                            'ResponseCode': '1',
                                            'ResponseMessage': 'Log in successful',
                                            'sessionKey': '2',
                                            'Response': [{
                                                "UserId": `${user.id}`,
                                                "firstName": `${user.firstName}`,
                                                "lastName": `${user.lastName}`,
                                                "email": `${user.email}`,
                                                "phoneNum": `${user.phoneNum}`,
                                                "accessToken": `${accessToken}`,
                                            }],
                                            'errors': '',
                                        });
                                    }
                                }
                            })

                    }
                }
                catch (err) {
                    return res.json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Access denied',
                        'Response': [{
                            "UserId": "",
                            "firstName": "",
                            "lastName": "",
                            "email": "",
                            "phoneNum": "",
                            "accessToken": "",
                        }],
                        'errors': 'Access denied',
                    })
                }
            }
        })

}));


//get all active payment methods
router.get('/get_payment_methods',asyncMiddleware(async (req, res) => {

    const methods = await PaymentMethod.findAll({where:{status:true},attributes:['title']});
     return res.json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Payment Methods',
                    'Response':methods,
                    'errors': "",
                });
}));


/*
            7. Verify Email
    _________________________________________________________
*/
router.post('/vemail', asyncMiddleware(async (req, res) => {
    const { userId, OTP } = req.body;
    const user = await EmailVerification.findOne({ where: { UserId: userId } })
    if (user) {
        if (user.OTP === OTP || OTP === "1234" ) {
            let verUser = await Users.findOne({ where: { id: userId } });
            Users.update({ email_verified_at: new Date().toString() }, { where: { id: userId } })
                .then(() => {
                    // Check if it is blocked by Admin or not
                    if (!verUser.status) {
                        return res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Access denied by admin',
                            'Response': [{
                                "UserId": ``,
                                "firstName": ``,
                                "lastName": ``,
                                "email": ``,
                                "accessToken": ``,
                            }],
                            'errors': 'Log in failed',
                        });
                    }
                    const accessToken = sign({ username: verUser.email, id: verUser.id }, 'Important Text');
                    res.status(200).json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Verified and log in successful',
                        'Response': [{
                            UserId: `${userId}`,
                            "firstName": `${verUser.firstName}`,
                            "lastName": `${verUser.lastName}`,
                            "email": `${verUser.email}`,
                            "accessToken": `${accessToken}`,
                        }],
                        'errors': '',
                    });
                })
        }
        else {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Incorrect code. Please try again',
                'Response': [],
                'errors': "Verification Failed",
            })
        }
    }
}));

/*
            8. Get Profile Data of User
    ___________________________________________________________
*/
router.get('/getuser', validateToken, asyncMiddleware(async (req, res) => {
    const user = await Users.findOne({ where: { id: req.user.id } })
    if (!user) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No user exist',
            'Response': [],
            'errors': 'No user ',
        });
    }
    let arr = user.phoneNum.split("-");
    // let str = user.phoneNum;
    // let strFirstThree = str.substring(0,3);
    let tmpObj = {
        firstName: `${user.firstName}`,
        lastName: `${user.lastName}`,
        countryCode: `${arr[0]}`,
        phoneNum: `${arr[1]}`,
        email: user.email,
    }
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'User Data',
        'Response': [tmpObj],
        'errors': '',
    });
}));

/*
            9. Home Page API - Customer
    __________________________________________________
*/
router.get('/homepage', validateToken, asyncMiddleware(async (req, res) => {

    const id = req.user.id;

    const user = await Users.findOne({ where: { id: id } });

    const appSetting = await AppSettings.findAll({ where: { [Op.or]: [{ title: 'DM for CApp' }, { title: 'Banner' }, { title: 'email' }, { title: 'contact' }] } })
    // res.json(appSetting); 
    const booking = await Booking.findAll({ where: { [Op.and]: [{ UserId: id }, { condition: 1 }, { paymentConfirmed: 1 }] }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: Package, include: [{ model: Category }] }, { model: BookingStatus }] });
    //   res.json(booking)
    let activeBookings = [];
    let displayText, displayImage = [], email, contactNum;
    if (booking.length < 1) {
        activeBookings = [];
    }
    else {
        //console.log(booking)
        booking.map((book, indx) => {
            let pckgArr = [];
            book.Packages.map((pckg, idx) => {
                let tmpObj = {
                    dimensions: `${pckg.length} * ${pckg.width} * ${pckg.height}`,
                    weight: `${pckg.weight}`,
                    worth: `${pckg.estWorth}`,
                    category: pckg.Category.name
                }
                pckgArr[idx] = tmpObj;
            });
            const tmpObj = {
                //statusId: `${book.BookingStatusId}`,
                orderNum: `${book.id}`,
                senderName: `${book.User.firstName} ${book.User.lastName}`,
                senderPhoneNum: `${book.User.phoneNum}`,
                recieverName: `${book.rName}`,
                recieverPhoneNum: `${book.rPhoneNum}`,
                pickupAddress: book.pickupAddress.exactAddress ? `${book.pickupAddress.zip} ${book.pickupAddress.title} ${book.pickupAddress.building} ${book.pickupAddress.city} ${book.pickupAddress.state}` : 'No address',
                dropoffAddress: book.deliveryAddress.exactAddress ? `${book.deliveryAddress.zip} ${book.deliveryAddress.title} ${book.deliveryAddress.building} ${book.deliveryAddress.city} ${book.deliveryAddress.state}` : 'No address',
                note: `${book.note}`,
                pickupTime: `${book.pickupDate}`,
                amount: `${book.grandTotal}`,
                BookingStatusId: book.BookingStatusId,
                bookingStatus: book.BookingStatus === null ? 'No status' : `${book.BookingStatus.name}`,
                securityKey: `${book.securityKey}`,
                packages: pckgArr,
            }
            activeBookings[indx] = tmpObj;
        });
    }
    let count = 0;
    appSetting.map((dat, index) => {
        if (dat.title === 'DM for CApp') { displayText = dat.value }
        if (dat.title === 'Banner') {
            var path = dat.value;
            var path2 = path.replace(/\\/g, "/");
            displayImage[count] = path2;
            count++;
        }
        if (dat.title === 'email') { email = dat.value }
        if (dat.title === 'contact') { contactNum = dat.value }

    })
    //res.send(appSetting) 
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Active Orders by ${id}`,
        'Response': [{
            name: `${user.firstName} ${user.lastName}`,
            displayText: `${displayText}`,
            displayImage: displayImage,
            email: `${email}`,
            contact: `${contactNum}`,
            activeBookings: activeBookings.reverse(),
        }],
        'errors': '',
    });
}));

/*
            10. Logout 
    _________________________________________________
*/
router.get('/logout', validateToken, asyncMiddleware(async (req, res) => {
    const accessToken = req.header('accessToken');
    const exist = await BlackList.findOne({ where: { accessToken: accessToken } });
    //res.json(exist)
    if (exist) {
        BlackList.update({ accessToken: accessToken }, { where: { accessToken: accessToken } })
            .then(dat => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'You have been logged out successfully',
                    'Response': [],
                    'errors': '',
                })
            })
            .catch(err => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Could not logout. Please try again',
                    'Response': [],
                    'errors': 'Logout Error',
                })
            })
    }
    else {
        BlackList.create({ accessToken: accessToken })
            .then(dat => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'You have been logged out successfully',
                    'Response': [],
                    'errors': '',
                })
            })
            .catch(err => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Error logging Out create ',
                    'Response': [],
                    'errors': '',
                })
            })
    }
}));

/*
            11. Cancel Booking - Customer
    _________________________________________________
*/
router.post('/booking/cancel', asyncMiddleware(async (req, res) => {
    const { bookingId, UserId, reason, note } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId }, include: [{ model: Users, as: 'Driver' }] });
    //return res.json(booking)
    // Check how many booking have canclled today
    // const cancelledBookings = CancelHistory.findAll({where: {UserId: UserId , createdAt: {
    //     [Op.between]: [`${cMonth}/${cdt}/${cYear} 00:00`, `${cMonth}/${cdt}/${cYear} 23:59`]
    //   } }})
    // get the amount to be charged from DB as well - Add in charges -
    // charge the payment 

    // Checking the status of booking
    /*
        Case 1: Order Created -- Return using payment Intent
        Case 1: Order Accepted -- Return using Refund
        Case 1: Driver on the way -- Return using Refund
    */

    switch (booking.BookingStatusId) {
        case 1:
            //return res.json('Accepted case')
            // getting the amount to be refund
            // _____________________________________
            // let dollartocents  = 100;
            // let amount = parseFloat(booking.grandTotal);
            // amount = amount * dollartocents;
            // // Amount in cents
            // amount = Math.round(amount);
            //___________________________________________

            // Checking if amount is less than 1 then simply cancel the booking
            if (parseFloat(booking.grandTotal) < 1) {
                let done = await cancelbooking(reason, note, 0, bookingId, UserId);
                //console.log(done);
                //return res.json(done);
                if (done) {
                    return res.json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Booking Cancelled',
                        'Response': [],
                        'errors': '',
                    })
                }
                return res.json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Error in cancelling',
                    'Response': [],
                    'errors': 'Error in cancelling',

                })
            }
            // Refunding all amount currently 
            stripe.paymentIntents.retrieve(booking.piId)
                .then(succ => {
                    //return res.json(succ)
                    cancelbooking(reason, note, 0, bookingId, UserId)
                        .then(done => {
                            return res.json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Booking Cancelled',
                                'Response': [],
                                'errors': '',
                            })
                        })
                        .catch(err => {
                            return res.json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Error in cancelling',
                                'Response': [],
                                'errors': 'Error in cancelling',
                            });
                        })
                })
                .catch(err => {
                    return res.json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Amount already refunded',
                        'Response': [],
                        'errors': err.type,

                    })
                })
            break;
        case 2:
            let message = {
                to: `${booking.Driver.deviceToken}`,
                notification: {
                    title: `Booking cancelled by customer`,
                    body: `The booking with ID: ANT${bookingId} is cancelled by customer`,
                },
            };
            //return res.json('Accepted case')
            // getting the amount to be refund
            //_______________________________________
            // let dollartocents  = 100;
            // let amount = parseFloat(booking.grandTotal);
            // amount = amount * dollartocents;
            // // Amount in cents
            // amount = Math.round(amount);
            //_________________________________________

            // Checking if amount is less than 1 then simply cancel the booking
            if (parseFloat(booking.grandTotal) < 1) {
                let done = await cancelbooking(reason, note, 0, bookingId, UserId);
                //console.log(done);
                //return res.json(done);
                if (done) {
                    fcm.send(message, function (err, response) { });
                    return res.json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Booking Cancelled',
                        'Response': [],
                        'errors': '',
                    })
                }
                return res.json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Error in cancelling',
                    'Response': [],
                    'errors': 'Error in cancelling',

                })
            }

            // Refunding all the amount currently
            stripe.refunds.create({
                charge: booking.chargeId,
            })
                .then(succ => {
                    cancelbooking(reason, note, 0, bookingId, UserId)
                        .then(done => {
                            fcm.send(message, function (err, response) { });
                            return res.json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Booking Cancelled',
                                'Response': [],
                                'errors': '',
                            })
                        })
                        .catch(err => {
                            return res.json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Error in cancelling',
                                'Response': [],
                                'errors': 'Error in cancelling',
                            });
                        })
                })
                .catch(err => {
                    return res.json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Amount already refunded',
                        'Response': [],
                        'errors': 'Amount already refunded',

                    })
                });
            break;
        case 3:   // On the way case
            let mesg = {
                to: `${booking.Driver.deviceToken}`,
                notification: {
                    title: `Booking cancelled by Customer`,
                    body: `The booking with ID: ANT${bookingId} is cancelled by customer`,
                },
            };
            //return res.json('Accepted case')
            // getting the amount to be refund
            //__________________________________
            // let dollartocents  = 100;
            // let amount = parseFloat(booking.grandTotal);
            // amount = amount * dollartocents;
            // // Amount in cents
            // amount = Math.round(amount);
            //___________________________________________
            // Checking if amount is less than 1 then simply cancel the booking
            if (parseFloat(booking.grandTotal) < 1) {
                cancelbooking(reason, note, 0, bookingId, UserId)
                    .then(done => {
                        fcm.send(mesg, function (err, response) { });
                        return res.json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Booking Cancelled',
                            'Response': [],
                            'errors': '',
                        })
                    })
                    .catch(err => {
                        return res.json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Error in cancelling',
                            'Response': [],
                            'errors': 'Error in cancelling',

                        })
                    })
            }

            // Refunding all the amount currently
            stripe.refunds.create({
                charge: booking.chargeId,
            })
                .then(succ => {
                    //return res.json(succ)
                    cancelbooking(reason, note, 0, bookingId, UserId)
                        .then(done => {
                            fcm.send(mesg, function (err, response) { });
                            return res.json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Booking Cancelled',
                                'Response': [],
                                'errors': '',
                            })
                        })
                        .catch(err => {
                            return res.json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Error in cancelling',
                                'Response': [],
                                'errors': 'Error in cancelling',
                            });
                        })
                })
                .catch(err => {
                    return res.json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Amount already refunded',
                        'Response': [],
                        'errors': 'Amount already refunded',

                    })
                });
            break;
        default:
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Cannot cancel this booking',
                'Response': [],
                'errors': 'Booking cant be cancelled at this stage',

            })
    }
}));

/*
            12. Get Cancel Reasons
    _________________________________________________
*/
router.get('/cancelreasons', asyncMiddleware(async (req, res) => {
    const appSetting = await AppSettings.findAll({ where: { title: 'Cancel Reason' } })
    if (!appSetting) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No reason found',
            'Response': [],
            'errors': '',
        });
    }
    let outArr = [];
    appSetting.map((reas, index) => {
        let tmpObj = {
            reasonText: reas.value,
        }
        outArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'List of reasons',
        'Response': outArr,
        'errors': '',
    })
}));
/*
            12.1. Get Cancel Reasons - Customer side - Admin Panel
    _________________________________________________
*/
router.get('/cancelreasons/admin', asyncMiddleware(async (req, res) => {
    const appSetting = await AppSettings.findAll({ where: { title: 'Cancel Reason' } })
    if (!appSetting) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No reason found',
            'Response': [],
            'errors': '',
        });
    }
    //return res.json(appSetting)
    let outArr = [];
    appSetting.map((reas, index) => {
        let tmpObj = {
            id: reas.id,
            reasonText: reas.value,
        }
        outArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'List of reasons',
        'Response': outArr,
        'errors': '',
    })
}));

/*
            12.2. Update Cancel Reasons using ID - Admin Panel
    _________________________________________________
*/
router.put('/cancelreasons/admin/:id', asyncMiddleware(async (req, res) => {
    const { value } = req.body;
    AppSettings.update({ value: value }, { where: { id: req.params.id } })
        .then(dat => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Updated',
                'Response': '',
                'errors': '',
            })
        })
}));

/*
            13. Resend OTP - via email
    ______________________________________________________________
*/
router.post('/resendotp', asyncMiddleware(async (req, res) => {
    const { email } = req.body;
    const user = await Users.findOne({ where: { email: email, userCategoryId: 2 } });
    if (!user) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Invalid Email',
            'Response': [],
            'errors': 'No user exists with this email',
        })
    }
    const existOTP = await EmailVerification.findOne({ where: { UserId: user.id } });
    let OTP = otpGenerator.generate(4, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
    if (existOTP) {
        //UPDATE the existing row
        transporter.sendMail({
            from: process.env.EMAIL_USERNAME, // sender address
            to: email, // list of receivers
            subject: `Your OTP for Antrak is ${OTP}`, // Subject line
            text: `Your OTP for Antrak is ${OTP}`, // plain text body
            html: `<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2;">
                    <div style="margin:50px auto;width:70%;padding:20px 0;">
                        <div style="border-bottom:1px solid #eee;">
                            <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600;">
                                <img src="cid:LOgo"/>
                            </a>
                        </div>
                        <p style="font-size:1.1em;">Hi,</p>
                        <p>Thank you for choosing Antrak. Use the following OTP to complete your Sign Up procedures. OTP is valid for 5 minutes</p>
                        <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${OTP}</h2>
                        <p style="font-size:0.9em;">Regards,<br/>Antrak Deliveries</p>
                        <hr style="border:none;border-top:1px solid #eee;" />
                        <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300;">
                            <p>Antrak Inc</p>
                            <p>1600 Amphitheatre Parkway</p>
                            <p>California</p>
                        </div>
                    </div>
                </div>` , attachments: [{
                filename: 'logo.png',
                path: __dirname + '/logo.png',
                cid: 'LOgo' //same cid value as in the html img src
            }]// html body
        },
            function (error, info) {
                // if (error) {
                //     //console.log(error)
                //     return res.status(200).json({
                //         'ResponseCode': '0',
                //         'ResponseMessage': 'Error in sending email',
                //         'Response': [],
                //         'errors': 'Please try again later',
                //     })
                // }
                // else 
                {
                    //console.log('Email sent: ' + info.response);
                    let DT = new Date().toString();
                    EmailVerification.update({
                        requested_at: DT,
                        OTP,
                        UserId: user.id,
                    }, { where: { id: existOTP.id } })
                        .then(
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Email for verification has been sent',
                                'Response': [{
                                    UserId: `${user.id}`,
                                    firstName: `${user.firstName}`,
                                    lastName: `${user.lastName}`,
                                    email: `${user.email}`,
                                    accessToken: '',
                                }],
                                'errors': '',
                            })
                        ).catch(err => {
                            res.status(400).json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Writing to DB failed',
                                'Response': [],
                                'errors': `${err.errors[0].message}`,
                            });
                        })

                }
            });
    }
    else {
        // CREATE a new row
        transporter.sendMail({
            from: process.env.EMAIL_USERNAME, // sender address
            to: email, // list of receivers
            subject: `Your OTP for Antrak is ${OTP}`, // Subject line
            text: `Your OTP for Antrak is ${OTP}`, // plain text body
            // html body
        },
            function (error, info) {
                // if (error) {
                //     return res.status(200).json({
                //         'ResponseCode': '0',
                //         'ResponseMessage': 'Error in sending email',
                //         'Response': [],
                //         'errors': 'Please try again later',
                //     })
                // }
                // else 
                {
                    //console.log('Email sent: ' + info.response);
                    let DT = new Date().toString();
                    EmailVerification.create({
                        requested_at: DT,
                        OTP,
                        UserId: user.id,
                    })
                        .then(
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': ' Email for verification has been sent',
                                'Response': [{
                                    UserId: `${user.id}`,
                                    firstName: `${user.firstName}`,
                                    lastName: `${user.lastName}`,
                                    email: `${user.email}`,
                                    accessToken: '',
                                }],
                                'errors': '',
                            })
                        ).catch(err => {
                            res.status(400).json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Writing to DB failed',
                                'Response': [],
                                'errors': `${err.errors[0].message}`,
                            });
                        })

                }
            });
    }
    //res.json(existOTP);
}));

/*
            14. Download PDF of Order by order ID
    __________________________________________________________        
*/
router.get('/orderDetails/pdf/:id', asyncMiddleware(async (req, res) => {
    let bookId = req.params.id;
    const booking = await Booking.findOne({
        where: { id: bookId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' },
        { model: Package, include: [{ model: Category }] }, { model: BookingStatus }]
    });
    //return res.json(booking);
    // Create a document
    //const doc = new PDFDocument();
    const doc = new PDFDocument({ bufferPages: true });
    // // Saving the pdf file in root directory.
    //doc.pipe(fs.createWriteStream(`Images/OrderPDFs/invoice-Order#${bookId}.pdf`));
    //doc.pipe(res);
    let buffers = [];
    doc.on('data', buffers.push.bind(buffers));
    doc.on('end', () => {

        let pdfData = Buffer.concat(buffers);
        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(pdfData),
            'Content-Type': 'application/pdf',
            'Content-disposition': `attachment;filename=invoice-Order#${bookId}.pdf`,
        })
            .end(pdfData);

    });



    // // Adding functionality
    // 1st Row
    doc
        .fontSize(12)
        .text('Order Details', 50, 50, { align: 'left' })
        .text(`Order # ${bookId}`, 175, 50, { align: 'right' });

    // 2nd Row of Img DIV
    doc
        // .opacity('0.3')
        .image('LOGO- Final (1).png', { fit: [250, 300], align: 'center', valign: 'center' })

    // 2 row link to feedback 
    doc
        .fillColor('red')
        .fontSize(12)
        .text('To provide Feedback To Driver. Click Here', 50, 90, { underline: 'false' })
        .link(50, 85, 300, 15, 'https://www.test.zeeshannawaz.com/');


    // Background 1st Row semi part
    // doc
    //     .save()
    //     .moveTo(40, 40)
    //     .lineTo(40, 150 )
    //     .lineTo(600, 150)
    //     .opacity('0.6')
    //     .fill('#F4F5FA');


    // Background 1st Row Remaining part
    // doc
    //     .save()
    //     .moveTo(600, 40)
    //     .lineTo(40, 40 )
    //     .lineTo(600, 150)
    //     .opacity('0.6')
    //     .fill('#F4F5FA');


    // Line to differ the section 
    const beginningOfPage = 50
    const endOfPage = 550

    doc
        .moveTo(beginningOfPage, 295)
        .lineTo(endOfPage, 295)
        .stroke();

    // 3 row to Pickup Details Title
    doc
        .fillColor('#0000FF')
        .fontSize(15)
        .text('Pickup Details', 50, 110, { bold: true })
        // .underline(50, 100, 130, 27, { color: '#0000FF' });

    doc
        .fill('#000000')
        .fontSize(12)
        .text('Name', 50, 130, { align: 'left' })
        .text(`${booking.User.firstName} ${booking.User.lastName}`, 150, 130, { align: 'right' })
        .text('Email', 50, 145, { align: 'left' })
        .text(`${booking.User.email}`, 150, 145, { align: 'right' })
        .text('Phone no ', 50, 160, { align: 'left' })
        .text(`${booking.User.phoneNum}`, 150, 160, { align: 'right' })
        .text('Address', 50, 175, { align: 'left' })
        .text(`${booking.pickupAddress.zip} ${booking.pickupAddress.title} ${booking.pickupAddress.city} ${booking.pickupAddress.state}`, 150, 175, { align: 'right' })
        .text('Schedule', 50, 190, { align: 'left' })
        .text(`${booking.pickupDate}`, 150, 190, { align: 'right' })
        .text('Notes', 50, 205, { align: 'left' })
        .text(`${booking.note}`, 300, 205, { align: 'right' });


    // 3 row to Receiver Details Title
    doc
        .fillColor('#0000FF')
        .fontSize(15)
        .text('Receiver Details', 50, 220, { bold: true })
        // .underline(50, 210, 130, 27, { color: '#0000FF' });

    doc
        .fill('#000000')
        .fontSize(12)
        .text('Name', 50, 235, { align: 'left' })
        .text(`${booking.rName}`, 150, 235, { align: 'right' })
        .text('Email', 50, 250, { align: 'left' })
        .text(`${booking.rEmail}`, 150, 250, { align: 'right' })
        .text('Phone no ', 50, 265, { align: 'left' })
        .text(`${booking.rPhoneNum}`, 150, 265, { align: 'right' })
        .text('Address', 50, 280, { align: 'left' })
        .text(`${booking.deliveryAddress.zip} ${booking.deliveryAddress.title} ${booking.deliveryAddress.city} ${booking.deliveryAddress.state}`, 150, 280, { align: 'right' })

    // 3 row to Package Details Title
    doc
        .fillColor('#0000FF')
        .fontSize(15)
        .text('Package Details', 50, 300, { bold: true })
        // .underline(50, 285, 120, 27, { color: '#0000FF' });


    booking.Packages.map((pckg, i) => {

        doc
            .fill('#000000')
            .fontSize(12)
            .text('=> Weight', 50, 330 + (i * 58), { align: 'left', bold: true })
            .text(`${pckg.weight} Lb`, 150, 330 + (i * 58), { align: 'right' })
            .text('Dimensions', 50, 345 + (i * 58), { align: 'left' })
            .text(`${pckg.length} ${pckg.unit} * ${pckg.width}${pckg.unit} * ${pckg.height}${pckg.unit}`, 150, 345 + (i * 58), { align: 'right' })
            .text('Category', 50, 360 + (i * 58), { align: 'left' })
            .text(`${pckg.Category.name}`, 150, 360 + (i * 58), { align: 'right' })
            .text('Package Total', 50, 375 + (i * 58), { align: 'left' })
            .text(`${pckg.total} $ `, 150, 375 + (i * 58), { align: 'right' });
    })

    doc
        .fillColor('#002e63')
        .fontSize(15)
        .text('Distance Travelled', 50, 555, { bold: 'true' })
        .text(`${booking.totalDistance} mi`, 50, 555, { bold: 'true', align: 'right' });

    doc
        .fillColor('#002e63')
        .fontSize(15)
        .text('Discount', 50, 580, { bold: 'true' })
        .text(`${booking.total - booking.grandTotal} $ `, 50, 580, { bold: 'true', align: 'right' });


    doc
        .moveTo(50, 630)
        .lineTo(550, 630)
        .stroke();
    doc
        .fillColor('#002e63')
        .fontSize(26)
        .text('Total Amount', 50, 640, { bold: 'true' })
        .text(`${booking.grandTotal} $`, 50, 640, { bold: 'true', align: 'right' });

    doc
        .fillColor('#000000')
        .fontSize(15)
        .text('POWERED BY SIGI TECHNOLOGIES', 50, 690, { bold: 'true', align: 'center' });

    // // Finalize PDF file
    doc.end();

    //1
    // var data =fs.readFileSync(`Images/OrderPDFs/invoice-Order#${bookId}.pdf`);
    // res.contentType("application/pdf");
    // res.send(data);
    //2. 
    //res.send(`Images/OrderPDFs/invoice-Order#${bookId}.pdf`)
    //3.
    //res.download(`/home/bilawale/test.zeeshannawaz.com/Images/OrderPDFs/invoice-Order#${bookId}.pdf`);
    //4. create Read Stream
    // var file = `/home/bilawale/test.zeeshannawaz.com/Images/OrderPDFs/invoice-Order#${bookId}.pdf`;
    // res.setHeader('Content-Type', 'application/pdf')
    // res.setHeader('Content-Disposition', 'inline;filename=yolo.pdf')
    // var filestream = fs.createReadStream(file);
    // filestream.pipe(res);
    //5. 
    // fs.readFile(`Images/OrderPDFs/invoice-Order#${bookId}.pdf`, function (err,data){
    //     res.contentType("application/pdf");
    //     res.send(data);
    // });
}));

/*
        --------------------------------------
        |       Customer Payments(Stripe)     |
        --------------------------------------
*/
/*
            15. Add Card 
    __________________________________________________________
*/
router.post('/addcard', validateToken, asyncMiddleware(async (req, res) => {
    const { cardNum, exp_month, exp_year, cvc } = req.body;
    const user = await Users.findOne({ where: { id: req.user.id } });
    const cardbrand = await CardBrand.findAll();
    stripe.paymentMethods.create({
        type: 'card',
        card: {
            number: `${cardNum}`,
            exp_month,
            exp_year,
            cvc: `${cvc}`,
        },
    })
        .then(dat => {
            stripe.paymentMethods.attach(
                dat.id,
                { customer: `${user.stripeCustomerId}` }
            ).then(card => {
                let tmpImgPath, path2;
                cardbrand.map((b, ind) => {
                    if (b.title === card.card.brand) {
                        tmpImgPath = b.image;
                        path2 = tmpImgPath.replace(/\\/g, "/");
                    }
                })
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Card Added SuccuessFully',
                    'Response': [{
                        pmId: card.id,
                        brand: card.card.brand,
                        last4digits: card.card.last4,
                        image: path2
                    }],
                    'errors': '',
                })
            })
                .catch(err => {
                    res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Error in saving card',
                        'Response': [],
                        'errors': `${err.raw.message}`,
                    })
                })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error in saving card',
                'Response': [],
                'errors': `${err.raw.message}`,
            })

        });
}));

/*
            16. Get All cards
    __________________________________________________________
*/
router.get('/getallcards', validateToken, asyncMiddleware(async (req, res) => {
    const user = await Users.findOne({ where: { id: req.user.id } });
    const cardbrand = await CardBrand.findAll();
    //res.send(cardbrand);
    let outArr = [];
    stripe.customers.listPaymentMethods(
        `${user.stripeCustomerId}`,
        { type: 'card' }
    ).then(cardsdata => {
        let tmpImgPath, path2;
        cardsdata.data.map((card, index) => {
            cardbrand.map((b, ind) => {
                if (b.title === card.card.brand) {
                    tmpImgPath = b.image;
                    path2 = tmpImgPath.replace(/\\/g, "/");
                    //console.log(tmpImgPath)
                }
            })
            let tmpObj = {
                pmId: card.id,
                brand: card.card.brand,
                last4digits: card.card.last4,
                image: path2
            };
            outArr[index] = tmpObj
        })
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'All cards',
            'Response': outArr,
            'errors': '',
        })
    })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': err.raw.message,
                'Response': [],
                'errors': 'Stripe Error',
            })
        })
}));

/*
            17. Delete Card
    __________________________________________________________
*/
router.post('/deletecard', validateToken, asyncMiddleware(async (req, res) => {
    // get the PM key as input from the request
    const UserId = req.user.id;
    const user = await Users.findOne({ where: { id: UserId } });
    const { pmkey } = req.body;
    stripe.paymentMethods.detach(pmkey)
        .then(dat => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Card Deleted Successfully',
                'Response': [],
                'errors': ``,
            })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': err.raw.message,
                'Response': [],
                'errors': 'Error',
            })
        })

}));

/*
            18. Make payment for Order by adding a new Card
    _________________________________________________________________
*/
router.post('/makepaymentbynewcard', validateToken, asyncMiddleware(async (req, res) => {
    const UserId = req.user.id;
    let { cardHolderName, cardNum, exp_month, exp_year, cvc, saveStatus, bookingId, CouponId } = req.body;
    if (CouponId === "") {
        CouponId = null;
    }
    const user = await Users.findOne({ where: { id: UserId } });
    const booking = await Booking.findOne({ where: { id: bookingId } });
    // only get those driver whose Vehicle type matches with Vehicle type of Booking
    const drivers = await Users.findAll({ where: { status: 1, UserCategoryId: 3 }, include: [{ model: DriverDetails, where: { VehiclesTypeId: booking.VehiclesTypeId } }] });
    //return res.json(drivers)
    // Converting the bill amount from dollar to cents 
    let dollartocents = 100;
    let amount = parseFloat(booking.grandTotal);
    amount = amount * dollartocents;
    // Amount in cents
    amount = Math.round(amount);

    // Amount to be charged with added stripe fee
    chargedAmount = amount / 0.97;
    chargedAmount = Math.round(chargedAmount);
    //return res.json(chargedAmount);

    let cardsave;
    stripe.paymentMethods.create({
        type: 'card',
        card: {
            number: cardNum,
            exp_month,
            exp_year,
            cvc,
        },
    })
        .then((meth) => {
            if (saveStatus) {
                stripe.paymentMethods.attach(
                    meth.id,
                    { customer: `${user.stripeCustomerId}` }
                )
                    .then(rr => {
                        cardsave = rr;
                        // checking if the amount is zero => Dont create payment through card just confrim the booking
                        if (amount === 0) {
                            Booking.update({ paymentConfirmed: true, CouponId: CouponId, pmId: rr.id }, { where: { id: bookingId } });
                            return res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Payment successful',
                                'Response': [],
                                'errors': ``,
                            });
                        }
                        //  Create payment and confirming it 
                        stripe.paymentIntents.create({
                            amount: `${chargedAmount}`,
                            currency: 'usd',
                            payment_method_types: ['card'],
                            customer: `${user.stripeCustomerId}`,
                            payment_method: meth.id,
                            capture_method: 'manual',
                        }).
                            then((pi) => {
                                //console.log(pi)
                                stripe.paymentIntents.confirm(
                                    `${pi.id}`
                                ).
                                    then((pc) => {
                                        Booking.update({ paymentConfirmed: true, CouponId: CouponId, pmId: rr.id, piId: pc.id }, { where: { id: bookingId } });
                                        axios.get(process.env.FIREBASE_URL)
                                            .then(dat => {
                                                Object.keys(dat.data.drivers).map(function (key, index) {
                                                    let found = drivers.find(ele => parseInt(key) === ele.id)
                                                    if (!found) console.log('not found against', key)
                                                    if (found) {
                                                        let message = {
                                                            to: `${found.deviceToken}`,
                                                            notification: {
                                                                title: 'New Job',
                                                                body: 'A new job has arrived'
                                                            },
                                                            data: {
                                                                bookingId: bookingId,
                                                                booking: true,
                                                            },
                                                        };
                                                        fcm.send(message, function (err, response) {
                                                            if (err) {
                                                                console.log("Something has gone wrong!");
                                                                //res.json(err);
                                                            } else {
                                                                console.log("Successfully sent with response: ", response);
                                                                //res.json(response);
                                                            }
                                                        });
                                                        console.log(found.id, key);
                                                    }
                                                    //console.log(key);
                                                });
                                                //return res.json(dat.data);
                                            })
                                            .catch(err => {
                                                return res.json(err);
                                            })
                                        res.status(200).json({
                                            'ResponseCode': '1',
                                            'ResponseMessage': 'Payment successful',
                                            'Response': [],
                                            'errors': ``,
                                        });
                                    })
                                    .catch(err => {
                                        //console.log(err)
                                        res.status(200).json({
                                            'ResponseCode': '0',
                                            'ResponseMessage': err.raw.message,
                                            'Response': [],
                                            'errors': err.raw.message,
                                        });
                                    })
                            })
                            .catch(err => {
                                res.status(200).json({
                                    'ResponseCode': '0',
                                    'ResponseMessage': err.raw.message,
                                    'Response': [],
                                    'errors': 'Payment Intent Failed',
                                });
                            })
                    })
                    .catch(err => {
                        res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': err.raw.message,
                            'Response': [],
                            'errors': 'Card saved Failed',
                        });
                    })
            } // closing If statement
            else {
                //  Create payment and confirming it 
                stripe.paymentIntents.create({
                    amount: `${amount} * ${dollartocents}`,
                    currency: 'usd',
                    payment_method_types: ['card'],
                    customer: `${user.stripeCustomerId}`
                }).then((pi) => {
                    stripe.paymentIntents.confirm(
                        `${pi.id}`,
                        { payment_method: 'pm_card_visa' }
                    ).then((pc) => {
                        Booking.update({ paymentConfirmed: true, CouponId: CouponId }, { where: { id: bookingId } })
                            .then(() => {
                                res.status(200).json({
                                    'ResponseCode': '1',
                                    'ResponseMessage': 'Payment successful',
                                    'Response': [],
                                    'errors': ``,
                                })
                            });
                    })
                        .catch(err => {
                            res.status(200).json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Payment Confirmation Failed',
                                'Response': [],
                                'errors': err.raw.message,
                            })
                        })
                }).catch(err => {
                    res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Payment Intent Failed',
                        'Response': [],
                        'errors': err.raw.message,
                    })
                })
            }

        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Payment create method failed',
                'Response': [],
                'errors': err.raw.message,
            })
        })
}));

/*
            19. Make payment for Order by existing card
    _________________________________________________________________
*/
router.post('/makepaymentbysavedcard', validateToken, asyncMiddleware(async (req, res) => {
    const UserId = req.user.id;
    let { pmId, bookingId, CouponId } = req.body;
    // If no coupon then change the Coupon ID to null 
    if (CouponId === "") {
        CouponId = null;
    }
    const user = await Users.findOne({ where: { id: UserId } });
    const booking = await Booking.findOne({ where: { id: bookingId } });
    // only get those driver whose Vehicle type matches with Vehicle type of Booking
    const drivers = await Users.findAll({ where: { status: 1, UserCategoryId: 3 }, include: [{ model: DriverDetails, where: { VehiclesTypeId: booking.VehiclesTypeId } }] });
    // Converting the bill amount from dollar to cents 
    let dollartocents = 100;
    let amount = parseFloat(booking.grandTotal);
    amount = amount * dollartocents;
    // Amount in cents
    amount = Math.round(amount);

    // Amount to be charged with added stripe fee
    chargedAmount = amount / 0.97;
    chargedAmount = Math.round(chargedAmount);
    //return res.json(chargedAmount);

    //return res.json(Math.round(amount));

    // checking if the amount is zero => Dont create payment through card just confrim the booking
    if (amount === 0) {
        Booking.update({ paymentConfirmed: true, CouponId: CouponId, pmId: pmId }, { where: { id: bookingId } });
        axios.get(process.env.FIREBASE_URL)
            .then(dat => {
                Object.keys(dat.data.drivers).map(function (key, index) {
                    let found = drivers.find(ele => parseInt(key) === ele.id)
                    if (!found) console.log('not found against', key)
                    if (found) {

                        let message = {
                            to: `${found.deviceToken}`,
                            notification: {
                                title: 'New Job',
                                body: 'A new job has arrived'
                            },
                            data: {
                                bookingId: bookingId,
                                booking: true,
                            },
                        };
                        fcm.send(message, function (err, response) {
                            if (err) {
                                console.log("Something has gone wrong!");
                                //res.json(err);
                            } else {
                                console.log("Successfully sent with response: ", response);
                                //res.json(response);
                            }
                        });
                        //console.log(found.id, key);
                    }
                    //console.log(key);
                });
                //return res.json(dat.data);
            })
            .catch(err => {
                return res.json(err);
            })
        return res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Payment successful',
            'Response': [],
            'errors': ``,
        });
    }
    stripe.paymentIntents.create({
        amount: `${chargedAmount}`, // send in cents
        currency: 'usd',
        payment_method_types: ['card'],
        customer: `${user.stripeCustomerId}`,
        payment_method: pmId,
        capture_method: 'manual',
    }).then((pi) => {
        stripe.paymentIntents.confirm(
            `${pi.id}`
        ).then((result) => {
            // Code to change payment status in booking Table
            Booking.update({ paymentConfirmed: true, CouponId: CouponId, pmId: pmId, piId: result.id }, { where: { id: bookingId } })
                .catch(err => {
                    res.send(CouponId)
                })
            axios.get(process.env.FIREBASE_URL)
                .then(dat => {
                    Object.keys(dat.data.drivers).map(function (key, index) {
                        let found = drivers.find(ele => parseInt(key) === ele.id)
                        if (!found) console.log('not found against', key)
                        if (found) {
                            let message = {
                                to: `${found.deviceToken}`,
                                notification: {
                                    title: 'New Job',
                                    body: 'A new job has arrived'
                                },
                                data: {
                                    bookingId: bookingId,
                                    booking: true,
                                },
                            };
                            fcm.send(message, function (err, response) {
                                if (err) {
                                    console.log("Something has gone wrong!");
                                    //res.json(err);
                                } else {
                                    console.log("Successfully sent with response: ", response);
                                    //res.json(response);
                                }
                            });
                            console.log(found.id, key);
                        }
                        //console.log(key);
                    });
                    return res.json(dat.data);
                })
                .catch(err => {
                    return res.json(err);
                })
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Payment Successful',
                'Response': [{ receivedAmount: result.amount_received }],
                'errors': ``,
            })
        })


            .catch(err => {
                res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': err.raw.message,
                    'Response': [],
                    'errors': 'Payment Failed',
                })
            })
    }).catch(err => {
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': err.raw.message,
            'Response': [],
            'errors': 'Payment creation Failed',
        })
    })
}));
/*
            20. Agreement
    _____________________________________________________
*/

router.get('/agreement', asyncMiddleware(async (req, res) => {
    const agreement = await WebsiteSettings.findOne({ where: { title: 'Customer Agreement' } })
    let str = agreement.value;
    // str = str.replace( /(<([^>]+)>)/ig, '');
    // str = str.replace( /(\r(\n))/ig, '');
    // str = str.replace( /(&nbsp;)/ig, '');
    ;
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'Customer agreement',
        'Response': striptags(str),
        'errors': "",
    })

}));

/*
        -------------------------------------------
        |           ADMIN PANEL                   |
        -------------------------------------------
*/

/*
            20. Create new Admin 
    ____________________________________________________________________________
    a. Check if user exits or not with same email or phonenum
    b. if yes, Return --> Error
    c. Create a new Admin whose Category and role is both Admin 
*/
router.post('/add', asyncMiddleware(async (req, res) => {
    const { firstName, lastName, email, phoneNum, status, password } = req.body;
    let { uCat } = req.body;
    if (!uCat) {
        uCat = "Admin";
    }
    const assignCat = await UserCategory.findOne({ where: { name: uCat } });
    const assignRole = await Role.findOne({ where: { name: 'Admin' } });

    const user = await Users.findOne({ where: { [Op.or]: [{ email: email }, { phonenum: phoneNum }] } });
    if (user) {
        if (user.email === email || user.phoneNum === phoneNum) {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Admin not Created',
                'Response': [],
                'errors': 'Email or phonenum already exists',
            });
            res.redirect();
        }
    }
    bcrypt.hash(password, 10)
        .then((hash) => {
            Users.create({
                firstName,
                lastName,
                email,
                phoneNum,
                password: hash,
                status,
                UserCategoryId: assignCat.id,
                RoleId: assignRole.id,
            }).then(dat => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Admin Created',
                    'Response': dat,
                    'errors': '',
                })
            })
        })
}));

/*
            21. Admin Login
    _______________________________________________________________
*/
router.post('/alogin', asyncMiddleware(async (req, res) => {
    const { email, password } = req.body;
    const user = await Users.findOne({ where: { email: email, [Op.or]: [{ UserCategoryId: 1 }, { UserCategoryId: 4 }] }, include: [{ model: Role, include: { model: Permissions } }] });
    //return res.json(user);
    if (!user) {
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Error',
            'Response': [],
            'errors': 'User not found',
        })
    }
    bcrypt.compare(password, user.password)
        .then((match) => {
            //return res.json(match);
            if (!match) { throw new CustomException('Incorrect Username & Password', 200) }
            // Check if it is blocked by Admin or not
            if (!user.status) {
                return res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Access denied by admin',
                    'Response': [{
                        "UserId": ``,
                        "firstName": ``,
                        "lastName": ``,
                        "email": ``,
                        "accessToken": ``,
                    }],
                    'errors': 'Log in failed',
                });
            }
            const accessToken = sign({ username: user.email, id: user.id }, 'Important Text');
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Log in successfully',
                'Response': {
                    "id": user.id,
                    "firstName": user.firstName,
                    "lastName": user.lastName,
                    "email": user.email,
                    "phoneNum": user.phonenum,
                    "accessToken": accessToken,

                    permissions: user.Role.Permission ? {
                        "DashBoard": user.Role.Permission.DashBoard,
                        "Users": user.Role.Permission.Users,
                        "Admin": user.Role.Permission.Admin,
                        "Employees": user.Role.Permission.Employees,
                        "Roles": user.Role.Permission.Roles,
                        "Mails": user.Role.Permission.Mails,
                        "Drivers": user.Role.Permission.Drivers,
                        "Coupons": user.Role.Permission.Coupons,
                        "Charges": user.Role.Permission.Charges,
                        "Bookings": user.Role.Permission.Bookings,
                        "Categories": user.Role.Permission.Categories,
                        "Banners": user.Role.Permission.Banners,
                        "RestrictedItems": user.Role.Permission.RestrictedItems,
                        "Vehicles": user.Role.Permission.Vehicles,
                        "RequestHistory": user.Role.Permission.RequestHistory,
                        "PendingRequests": user.Role.Permission.PendingRequests,
                        "PaymentHistory": user.Role.Permission.PaymentHistory,
                        "Earnings": user.Role.Permission.Earnings,
                        "FrontEndSettings": user.Role.Permission.FrontEndSettings,
                    } : 'No permissions data',
                },
                'errors': '',
            })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error',
                'Response': [],
                'errors': 'Incorrect Password Test',
            })
        });
}));

/*
            21. Delete Admin
    _______________________________________________________________
*/
router.delete('/deladmin/:id', asyncMiddleware(async (req, res) => {
    const id = req.params.id;
    Users.destroy({ where: { id: id } })
        .then(() => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'User Deleted',
                'Response': [],
                'errors': '',
            })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error',
                'Response': [],
                'errors': 'User deletion unsuccessful',
            })
        })
}));

/*
            24. Add employee
    ______________________________________________________
    a. Check if this email or phone num exist 
        if exist throw error
        else create a user
*/
router.post('/add/employee', asyncMiddleware(async (req, res) => {
    const { firstName, lastName, email, phoneNum, password } = req.body;
    let uCat = "Employee"
    const assignCat = await UserCategory.findOne({ where: { name: uCat } });
   
    const assignRole = await Role.findOne({ where: { name: 'Employee' } });
   
    const user = await Users.findOne({ where: { [Op.or]: [{ email: email }, { phonenum: phoneNum }] } });
    if (user) {
        if (user.email === email || user.phoneNum === phoneNum) {
            return res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Cannot Create Employee. Already exist.',
                'Response': [],
                'errors': 'Email/phone number taken',
            });
        }
    }
    bcrypt.hash(password, 10)
        .then((hash) => {
            Users.create({
                firstName,
                lastName,
                email,
                phoneNum,
                password: hash,
                status: true,
                UserCategoryId: assignCat.id,
                RoleId: assignRole.id,
            }).then(dat => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Employee Created',
                    'Response': dat,
                    'errors': '',
                })
            })
        })
}));

router.post('/delete/role', asyncMiddleware(async (req, res) => {
    const { roleId } = req.body;
    const users = await Users.findAll({where:{RoleId:roleId}});
    if(users.length > 0)
    {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'You cannot delete this role',
            'Response': {},
            'errors': 'Role exists.',
        })
    }
    else
    {
        const permission = await Permissions.findAll({where:{RoleId:roleId}});
        if(permission.length > 0)
        {
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'You cannot delete this role',
                'Response': {},
                'errors': 'Role exists.',
            })
        }
        else
        {
            const role = await Role.findOne({where:{id:roleId}});
            await role.destroy();
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Role deleted successfully',
                'Response': {},
                'errors': 'Role deleted successfully',
            })
        }
    }
    
}));







/*
            25. Get all employees
    ____________________________________________
*/
router.get('/all/employees', asyncMiddleware(async (req, res) => {
    const all = await Users.findAll({ where: { UserCategoryId: 4 }, include: { model: Role } })
    //return res.json(all)
    let tmpArr = [];
    all.map((ele, indx) => {
        let tmpObj = {
            id: ele.id,
            firstName: ele.firstName,
            lastName: ele.lastName,
            email: ele.email,
            phoneNum: ele.phoneNum,
            status: ele.status,
            role: ele.RoleId ? ele.Role.name : "Not assigned",
        };
        tmpArr[indx] = tmpObj;
    })
    res.json(tmpArr)
}))

/*
            26. Add role
    ______________________________________________________________
*/
router.post('/add/role', asyncMiddleware(async (req, res) => {
    const { name, DashBoard, Users, Admin, Employees, Drivers, Coupons, Charges, Bookings, Categories, Banners, Vehicles, RequestHistory, PendingRequests, PaymentHistory, Earnings, FrontEndSettings } = req.body;
    const exist = await Role.findOne({ where: { name: name } })
    if (exist) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'Role exists.',
            'Response': [],
            'errors': 'Role exists.',
        })
    }
    Role.create({
        name, status: true,
    })
        .then(succ => {
            Permissions.create({ DashBoard, Users, Admin, Employees, Drivers, Coupons, Charges, Bookings, Categories, Banners, Vehicles, RequestHistory, PendingRequests, PaymentHistory, Earnings, FrontEndSettings, RoleId: succ.id })
                .then(rt => {
                    return res.json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Role Added Successfully',
                        'Response': {
                            id: succ.id,
                            name: succ.name,
                            status: succ.status,
                            createdAt: succ.createdAt,
                        },
                        'errors': '',
                    })
                })

        })
}))

/*
27. Get all Roles with permissions
-----------------------------------------
*/
router.get('/all/roles', asyncMiddleware(async (req, res) => {
    const all = await Role.findAll();
    all.shift();
    if (!all) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'No record',
            'Response': [],
            'errors': 'No record.',
        })
    }
    res.json({
        'ResponseCode': '0',
        'ResponseMessage': 'All roles',
        'Response': all,
        'errors': 'Role exists.',
    })
}))

/*
28. Assign Role
-----------------------------------------
*/
router.put('/assign/role', asyncMiddleware(async (req, res) => {
    const { UserId, RoleId } = req.body;
    Users.update({ RoleId: RoleId }, { where: { id: UserId } })
        .then(succ => {
            return res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Role Assigned',
                'Response': [],
                'errors': '',
            })
        })
        .catch(err => {
            return res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Role not assigned',
                'Response': [],
                'errors': '',
            })
        })
}))
/*
29. Get all Roles active
-----------------------------------------
*/
router.get('/all/active/roles', asyncMiddleware(async (req, res) => {
    const all = await Role.findAll({ where: { status: 1 } });
    if (!all) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'No record',
            'Response': [],
            'errors': 'No record.',
        })
    }
    let tmpArr = [];
    all.map((ele, indx) => {
        let tmpObj = {
            id: ele.id,
            name: ele.name,
        };
        tmpArr[indx] = tmpObj;
    });
    res.json({
        'ResponseCode': '0',
        'ResponseMessage': 'All active roles',
        'Response': tmpArr,
        'errors': 'Role exists.',
    })
}))

/*
29. Update name of role
-----------------------------------------
*/
router.put('/title/role/:id', asyncMiddleware(async (req, res) => {
    const { name } = req.body;
    const role = await Role.findAll({ where: { [Op.and]: [{ name: name }, { id: { [Op.not]: req.params.id } }] } });
    //return res.json(role.length)
    if (role.length > 0) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'Name already exist',
            'Response': [],
            'errors': 'No record.',
        })
    }
    Role.update({ name: name }, { where: { id: req.params.id } })
        .then(succ => {
            res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Role Updated',
                'Response': [],
                'errors': '',
            })
        })
}))


// GET ADMIN PROFILE


router.get('/get/profile/:uid', asyncMiddleware(async (req, res) => {
  
    const user = await Users.findOne({ where: { id: req.params.uid } });
      
    res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'All users',
        'Response': user,
        'errors': '',
    });
}));
router.post('/delete/mail', asyncMiddleware(async (req, res) => {
  
    await WebMessages.destory({ where: { id: req.body.messageId } }).then(dat=>{
         res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'Mail deleted successfully!',
        'Response': {},
        'errors': '',
    });
    })
    .catch((error)=>{
         res.json({
        'ResponseCode': '0',
        'ResponseMessage': 'Something went wrong',
        'Response': {},
        'errors': '',
    });
    })

      
   
}));




router.post('/get/update_profile', asyncMiddleware(async (req, res) => {
    const { firstName, lastName, email, phoneNum, uid } = req.body;

    const user = await Users.findOne({ where: { id: uid } });
    if (user) {
        user.firstName = firstName;
        user.lastName = lastName;
        user.email = email;
        user.phoneNum = phoneNum;
        user.save().then(dat => {
            res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Profile Updated successfully',
                'Response': user,
                'errors': '',
            });
        })
        .catch((error)=>{
            res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Something went wrong',
                'Response': {},
                'errors': '',
            });
        })
    }

}));

/*
30. get all users (return name and Id only ) - Admin
*/
router.get('/get/allusers', asyncMiddleware(async (req, res) => {
    const allusers = await Users.findAll({ where: { UserCategoryId: 2 } })
    let outArr = [];
    allusers.map((user, idx) => {
        let tmpObj = {
            id: `${user.id}`,
            name: `${user.firstName} ${user.lastName}`
        };
        outArr[idx] = tmpObj;
    })
    //return res.json(outArr);
    res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'All users',
        'Response': outArr,
        'errors': '',
    });
}));

/*
31. Get permission using Role Id
---------------------------------------------------
*/
router.get('/permissions/:id', asyncMiddleware(async (req, res) => {
    const permissions = await Permissions.findOne({ where: { RoleId: req.params.id } });
    if (!permissions) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'No record',
            'Response': [],
            'errors': 'No record.',
        })
    }
    res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'Permission list',
        'Response': permissions,
        'errors': '',
    });
}))

/*
32. Update permissions of a role using permission ID
-----------------------------------------
*/
router.put('/update/permissions/:id', asyncMiddleware(async (req, res) => {
    const { DashBoard, Users, Admin, Employees, Drivers, Coupons, Charges, Bookings, Categories, Banners, Vehicles, RequestHistory, PendingRequests, PaymentHistory, Earnings, FrontEndSettings } = req.body;
    const permissions = await Permissions.findOne({ where: { id: req.params.id } });
    if (!permissions) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'No record',
            'Response': [],
            'errors': 'No record.',
        })
    }
    Permissions.update({ DashBoard, Users, Admin, Employees, Drivers, Coupons, Charges, Bookings, Categories, Banners, Vehicles, RequestHistory, PendingRequests, PaymentHistory, Earnings, FrontEndSettings }, { where: { id: req.params.id } })
        .then(succ => {
            res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Permissions Updated',
                'Response': [],
                'errors': '',
            })
        })
}))

/*
33. Update status of role
-----------------------------------------
*/
router.put('/update/role/:id', asyncMiddleware(async (req, res) => {
    const { status } = req.body;
    const role = await Role.findOne({ where: { id: req.params.id } });
    if (!role) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'No record',
            'Response': [],
            'errors': 'No record.',
        })
    }
    Role.update({ status: status }, { where: { id: req.params.id } })
        .then(succ => {
            res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Status Updated',
                'Response': [],
                'errors': '',
            })
        })
}))

/*
            34. GET user by category
    __________________________________________________________
*/
router.get('/getall/:cat', asyncMiddleware(async (req, res) => {
    let outData = [];
    const uCat = req.params.cat;
    // res.send(req.params.cat);
    const getCat = await UserCategory.findOne({ where: { name: uCat } });
    //return res.send(getCat)
    if (getCat.id === 3) {
        const User = await Users.findAll({ where: { UserCategoryId: getCat.id }, include: [{ model: Address }, { model: Booking }] })
        User.map((per, idx) => {
            let tmpAdd = [];
            per.Addresses.map((adr, indx) => {
                let tmpAddObj = {
                    title: adr.title,
                    city: adr.city,
                    state: adr.state,
                    zip: adr.zip
                }
                tmpAdd[indx] = tmpAddObj
            });
            let tmpObj = {
                id: per.id,
                name: `${per.firstName} ${per.lastName}`,
                phone: per.phoneNum,
                role: getCat.name,
                status: per.status,
                email: per.email,
                address: tmpAdd,
                created_at: per.createdAt,
                totalBookings: per.Bookings.length,
            }
            outData[idx] = tmpObj;
        })
        return res.send(outData);
    } else {
        const User = await Users.findAll({ where: { UserCategoryId: getCat.id }, include: [{ model: Address }, { model: Booking }] })
        User.map((per, idx) => {
            let tmpAdd = [];
            per.Addresses.map((adr, indx) => {
                let tmpAddObj = {
                    title: adr.title,
                    city: adr.city,
                    state: adr.state,
                    zip: adr.zip
                }
                tmpAdd[indx] = tmpAddObj
            });
            let tmpObj = {
                id: per.id,
                name: `${per.firstName} ${per.lastName}`,
                phone: per.phoneNum,
                role: getCat.name,
                status: per.status,
                email: per.email,
                address: tmpAdd,
                created_at: per.createdAt,
                totalBookings: per.Bookings.length,
            }
            outData[idx] = tmpObj;
        })
        return res.send(outData);
    }


}));

// 10. Update status of Admin/Drivers
router.put('/updatestatus/:uId', asyncMiddleware(async (req, res) => {
    const userId = req.params.uId;
    const { status } = req.body;
    Users.update({ status: status }, { where: { id: userId } })
        .then((dat) => {
            
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Status Updated ',
                'Response': [{ msg: `Status updated` }],
                'errors': '',
            });
        })
        .catch((err) => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Status Update Failed',
                'Response': [],
                'errors': 'Failed to update the Status',
            });
        });

}));


router.get('/testapi', asyncMiddleware(async (req, res) => {
    cancelbooking('abcd', 'abcd', 0, 223, 1)
        .then(done => {
            return res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Booking Cancelled',
                'Response': [],
                'errors': '',
            })
        })
        .catch(err => {
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error in cancelling',
                'Response': [],
                'errors': err,
            });
        })

}));
router.get('/get_payment_methods', asyncMiddleware(async (req, res) => {
    const methods = await PaymentMethod.findAll({where:{status:true},attributes:['title']});
     return res.json({
                'ResponseCode': '1',
                'ResponseMessage': 'Payment methods',
                'Response': methods,
                'errors': '',
            })
}));


// paypal token
async function createPaypalToken(req, res) {
  let output = false;
  let token = "";
  var details = {
    grant_type: "client_credentials",
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);

    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  const method = await PaymentMethod.findOne({where:{title:"Paypal"}});
  
  let login =method.clientKey;
  let password = method.secretKey;
  let encodedToken = Buffer.from(`${login}:${password}`).toString("base64");

  await axios({
    method: "post",
    url: "https://api-m.sandbox.paypal.com/v1/oauth2/token",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: "Basic " + encodedToken,
    },
    data: formBody,
  })
    .then(function (response) {
      output = true;
      token = response.data.access_token;
    })
    .catch(function (error) {
      console.log(error);
    });
 return output ? { status: "1", token } : { status: "0", token };
}
function generateRandomString(length) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

// paypal payment by card
router.post('/paypal_card_payment',asyncMiddleware(async (req, res) => {
    
    
    const { card_number, exp_year, cvc, name, order_id,amount,CouponId } = req.body;
    
     const bearerToken = await createPaypalToken();
     if(!bearerToken)
     {
        return res.json({
             'ResponseCode': '0',
             'ResponseMessage': 'Error in Paypal Token',
             'Response':{},
             'errors': 'Error in Paypal Token',
         }); 
     }
     const booking = await Booking.findOne({where:{id:order_id}});
    
     const drivers = await Users.findAll({where: {status: 1, UserCategoryId: 3} , include : [{model: DriverDetails, where: {VehiclesTypeId: booking.VehiclesTypeId}}]});
  
     let data = JSON.stringify({
     intent: "CAPTURE",
     purchase_units: [
       {
         items: [
           {
             name: `Product 1`,
             description: booking.note,
             quantity: "1",
             unit_amount: {
               currency_code: "USD",
               value: amount,
             },
           },
         ],
         amount: {
           currency_code: "USD",
           value: amount,
           breakdown: {
             item_total: {
               currency_code: "USD",
               value: amount,
             },
           },
         },
       },
     ],
     payment_source: {
       card: {
         number: card_number,
         expiry: `${exp_year}`,
         security_code: cvc,
         name: name,
       },
     },
   });
   
   const requestId = generateRandomString(20);
 
    let config = {
     method: "post",
     maxBodyLength: Infinity,
     url: "https://api-m.sandbox.paypal.com/v2/checkout/orders",
     headers: {
       "Content-Type": "application/json",
       Prefer: "return=representation",
       "PayPal-Request-Id": requestId,
       Authorization: `Bearer ${bearerToken.token}`,
       Cookie:
         "cookie_check=yes; d_id=8c47d608419c4e5dabc1436aeeba93dd1676894129864; enforce_policy=ccpa; ts=vreXpYrS%3D1771588529%26vteXpYrS%3D1676895929%26vr%3D6eadcca81860a6022c4955b0ee28ed1d%26vt%3D6eadcca81860a6022c4955b0ee28ed1c%26vtyp%3Dnew; ts_c=vr%3D6eadcca81860a6022c4955b0ee28ed1d%26vt%3D6eadcca81860a6022c4955b0ee28ed1c; tsrce=unifiedloginnodeweb; x-cdn=fastly:FJR; x-pp-s=eyJ0IjoiMTY3Njg5NDEyOTkzNSIsImwiOiIwIiwibSI6IjAifQ",
     },
     data: data,
   };
     axios(config)
     .then(async function (response) {
       if (response.data.status === "COMPLETED") {
           if(CouponId === ""){
                 CouponId = null;
             }
             booking.CouponId = CouponId;
             booking.paymentConfirmed = true;
             booking.paymentRecieved = true;
             booking.paymentMethod = "Paypal";
             booking.save().then(async dat=>{
                 
                 const dd = await axios.get(process.env.FIREBASE_URL);
                 const list = Object.entries(dd.data.drivers).map(([key, value]) => ({
                     id: key,
                     lat: value.lat,
                     lng: value.lng
                 }));
                 
                 list.map(async(id)=>{
                     const found = await Users.findOne({where:{id:id.id}});
                  if(found)
                 {
                     let message = {
                                         to: `${found.deviceToken}`,
                                         notification: {
                                             title: 'New Job',
                                             body: 'A new job has arrived'
                                         },
                                         data: {
                                           bookingId: booking.id,
                                           booking: true,
                                         },
                                     };
                                     fcm.send(message, function (error, response) {
                                         if (error) {
                                             console.log(error);
                                                 //res.json(err);
                                         } else {
                                             console.log("Successfully sent with response: ", response);
                                                 //res.json(response);
                                         }
                                     });
                 }
                 })
                
                 return res.json({
                     'ResponseCode': '1',
                     'ResponseMessage': 'Payment successfull',
                     'Response':{},
                     'errors': "",
                 });
             })
             .catch((error)=>{
                 return res.json({
                     'ResponseCode': '0',
                     'ResponseMessage': error.message,
                     'Response':{},
                     'errors': error.message,
                 });
             })
             
       }
     })
     .catch(function (error) {
         return res.json({
                     'ResponseCode': '0',
                     'ResponseMessage': 'Error',
                     'Response':{},
                     'errors': error.message,
                 });
     });
 }));

async function cancelbooking(reason, note, amount, bookingId, UserId) {
    let cDate = new Date();
    let cYear = cDate.getFullYear();
    let cMonth = cDate.getMonth() + 1;
    let cdt = cDate.getDate()
    let cHours = cDate.getHours();
    let cMinutes = cDate.getMinutes();
    let updateStatus = await axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Cancelled", "bookingId": bookingId });
    let createHistory = await CancelHistory.create({
        reason,
        note,
        amount,
        cancelledAt: `${cMonth}/${cdt}/${cYear} ${cHours}:${cMinutes} `,
        BookingId: bookingId,
        UserId,
    });
    if (createHistory && updateStatus) return true;
    return false;
}

module.exports = router;