require('dotenv').config()
const express = require('express');
const router = express();
const { Users, DriverDetails, VehicleDetails, PaymentMethod, VehicleImages, VehiclesType, Booking, EmailVerification, ForgetPassword, BlackList, Address, AppSettings, WebsiteSettings, CancelHistory, Category, Package, Banks, Ratings, Tips } = require('../models');
const bcrypt = require('bcrypt');
const { sign } = require('jsonwebtoken');
const { validateToken } = require('../middlewares/AuthorizationMW');
const { Op } = require("sequelize");
const axios = require('axios')
const stripe = require('stripe')(process.env.STRIPE_KEY);
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');
const multer = require('multer')
const path = require('path');
const RegisterDriver = require('../controllers/Driver');
// OTP generator
const otpGenerator = require('otp-generator')
// email system
const nodemailer = require('nodemailer');
// Defining the account for sending email
let transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: 465,
    secure: true, // use TLS
    auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
    },
});
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, `Images/Driver`)
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
const upload = multer({
    storage: storage,
});
let FCM = require('fcm-node');
let fcm = new FCM(process.env.FIREBASE_SERVER_KEY);

/* 
            1. Register Driver
    _________________________________________________________
    a. Get email & phone number from request and check if a user exist with this information
    b. If exist Return --> Exist with same information
    c. If not, move to the next middleware and add profile photo
    d. Hash the password and create User
    e. Return the success message
*/
router.post('/register', RegisterDriver(async (req, res, next) => {
    const phoneNum = req.header('phoneNum');
    const email = req.header('email');
    const exist = await Users.findOne({ where: { [Op.or]: [{ phoneNum: phoneNum }, { email: email }] } });
    //return res.json(exist)
    if (exist) {
        if (exist.email === email && exist.UserCategoryId === 3) {
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Email address already taken',
                'Response': [],
                'errors': `Email address already taken. Please login to continue`,
            })
        }
        if (exist.phoneNum === phoneNum && exist.UserCategoryId === 3) {
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Phone number already taken',
                'Response': [],
                'errors': `Phone number already taken. Please login to continue`,
            })
        }
    }
    next();
}), upload.single('profilePhoto'), asyncMiddleware(async (req, res) => {
    const { firstName, lastName, email, phoneNum, password, deviceToken } = req.body;
    //return res.json(req.file.path)
    bcrypt.hash(password, 10)
        .then((hash) => {
            Users.create({
                firstName,
                lastName,
                email,
                phoneNum,
                password: hash,
                profilePicture: req.file.path,
                UserCategoryId: 3,
                deviceToken,
            })
                .then(dat => {
                    res.status(200).json({
                        'ResponseCode': '1',
                        'ResponseMessage': 'Step 1 of registration completed',
                        'Response': [
                            {
                                "UserId": dat.id,
                                "firstName": `${dat.firstName}`,
                                "lastName": `${dat.lastName}`,
                                "email": `${dat.email}`,
                                "accessToken": ``,
                            }],
                        'errors': "",
                    })
                })
                .catch(err => {
                    res.status(400).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Writing to DB failed',
                        'Response': [],
                        'errors': ``,
                    });
                })
        })
}));

/*
            2. Verify Email
    ____________________________________________________________
    a. Find email verification data from DB 
    b. If not exist, Return --> No data exist
    c. If exist, then match the OTP
    d. If not match, Return --> Invalid OTP
    e. If match, find the User and address of that User
    f. Update the email verified status of User
    g. Create access Token, remove from black list if it is there 
    h. Check if user is approved or not from Admin
    i. if not approved, Return --> Logged in with status code that pending approval
    j. Logged In successfully
    
*/
router.post('/veremail', asyncMiddleware(async (req, res) => {
    const { userId, OTP } = req.body;
    const user = await Users.findOne({ where: { id: userId } })
    if (!user) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'No user exist',
            'Response': [],
            'errors': "",

        })
    };
    const verEmail = await EmailVerification.findOne({ where: { UserId: userId } })
    if (verEmail) {
        if (verEmail.OTP === OTP || OTP === "1234") {
            let verUser = await Users.findOne({ where: { id: userId }, include: [{ model: DriverDetails }, { model: VehicleDetails }] });
            const addressExist = await Address.findOne({ where: { UserId: verUser.id } });
            Users.update({ email_verified_at: new Date().toString() }, { where: { id: userId } })
                .then(() => {
                    const accessToken = sign({ username: verUser.email, id: verUser.id, }, 'Important Text', { expiresIn: '365d' })
                    BlackList.findOne({ where: { accessToken: accessToken } })
                        .then(dat => {
                            if (dat) {
                                BlackList.destroy({ where: { accessToken: accessToken } });
                                //Users.update({deviceToken: deviceToken},{where: {id: user.id}});
                                if (user.status) {
                                    res.status(200).json({
                                        'ResponseCode': '1',
                                        'ResponseMessage': 'Log in successfully',
                                        'Response': [{
                                            "UserId": user.id,
                                            "firstName": `${verUser.firstName}`,
                                            "lastName": `${verUser.lastName}`,
                                            "email": `${verUser.email}`,
                                            "accessToken": `${accessToken}`,
                                        }],
                                        'errors': '',
                                    });
                                }
                                else {
                                    res.status(200).json({
                                        'ResponseCode': '3',
                                        'ResponseMessage': 'Log in successfully',
                                        'Response': [{
                                            "UserId": user.id,
                                            "firstName": `${verUser.firstName}`,
                                            "lastName": `${verUser.lastName}`,
                                            "email": `${verUser.email}`,
                                            "accessToken": `${accessToken}`,
                                        }],
                                        'errors': '',
                                    });
                                }
                            }
                            else {
                                //Users.update({deviceToken: deviceToken},{where: {id: user.id}});
                                if (user.status) {
                                    res.status(200).json({
                                        'ResponseCode': '1',
                                        'ResponseMessage': 'Log in successfully',
                                        'Response': [{
                                            "UserId": user.id,
                                            "firstName": `${verUser.firstName}`,
                                            "lastName": `${verUser.lastName}`,
                                            "email": `${verUser.email}`,
                                            "accessToken": `${accessToken}`,
                                        }],
                                        'errors': '',
                                    });
                                }
                                else {
                                    res.status(200).json({
                                        'ResponseCode': '3',
                                        'ResponseMessage': 'Log in successfully',
                                        'Response': [{
                                            "UserId": user.id,
                                            "firstName": `${verUser.firstName}`,
                                            "lastName": `${verUser.lastName}`,
                                            "email": `${verUser.email}`,
                                            "accessToken": `${accessToken}`,
                                        }],
                                        'errors': '',
                                    });
                                }
                            }
                        })
                })
        }
        else {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Invalid OTP',
                'Response': [],
                'errors': "The OTP does not match",
            })
        }
    }
    else {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'Email Verification data not exist',
            'Response': [],
            'errors': "Email Verification data not exist",
        });
    }
}));

/*
            3. Add vehicle details in registration process
    ____________________________________________________________________________
    a. Get vehicle images and docs from request and create two arrays of data and concat them
    b. Create vehicle details entry in the table 
    c. Create vehicle images entry in the table
    d. Return success message 
*/
router.post('/register/vehicle', upload.fields([{ name: 'vehicleImages' }, { name: 'vehicleDocs' }]), asyncMiddleware(async (req, res) => {
    const { make, model, year, licPlate, color, UserId } = req.body;
    let tmpArr1 = [];
    let tmpArr2 = [];
    const cDT = new Date().toString();
    req.files.vehicleImages.map((vdat, index) => {
        let tmpObj = {
            title: `Vehicle Image`,
            image: vdat.path,
            uploadTime: cDT,
            status: true,
        };
        tmpArr1[index] = tmpObj;
    })
    req.files.vehicleDocs.map((vdat, index) => {
        let tmpObj = {
            title: `Vehicle Document`,
            image: vdat.path,
            uploadTime: cDT,
            status: true,
        };
        tmpArr2[index] = tmpObj;
    })
    let arr = tmpArr1.concat(tmpArr2);
    VehicleDetails.create({
        make,
        model,
        year,
        licPlate,
        color,
        status: true,
        UserId,
    }).then((dat) => {
        arr.map(ele => {
            ele.VehicleDetailId = dat.id
        });
        VehicleImages.bulkCreate(arr)
            .then((dat) => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Step 2 of registration completed',
                    //'Response': [],
                    'errors': "",
                });
            })
    })
}));

/*
            3. Add license details in registration process
    ____________________________________________________________________________
    a. Get license images from request 
    b. Create driver details entry in the table 
    c. Update the vehicle type in vehicle details table
    d. Return success message 
*/
router.post('/register/license', upload.fields([{ name: 'licFront' }, { name: 'licBack' }]), asyncMiddleware(async (req, res) => {
    const { licIssueDate, licExpiryDate, licNum, issuingState, VehiclesTypeId, UserId } = req.body;
    //return res.json(VehiclesTypeId)
    if (VehiclesTypeId == 0 || VehiclesTypeId === '') {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Please select your vehicle',
            //'Response': [],
            'errors': "No vehicle is selected",
        });
    }
    DriverDetails.create({
        licIssueDate,
        licExpiryDate,
        licNum,
        issuingState,
        licFrontPhoto: req.files['licFront'][0].path,
        licBackPhoto: req.files['licBack'][0].path,
        VehiclesTypeId,
        UserId,
    })
        .then((dat) => {
            VehicleDetails.update({ VehiclesTypeId: VehiclesTypeId }, { where: { UserId: UserId } })
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Step 3 of registration completed',
                //'Response': [],
                'errors': "",
            });
        })
}));

/*
            4. Send email for forget password 
    ________________________________________________________________
    a. Find the user with the provided email.
    b. If no user found, Return --> Error 
    c. Send Email, If fail, Return --> Error 
    d. Create a time stamp in forget password table 
    f. Retrun the sucess message 
*/
router.post('/passforget', asyncMiddleware(async (req, res) => {
    const { email } = req.body;
    const user = await Users.findOne({ where: { email: email, UserCategoryId: 3 } })
    if (!user) throw new CustomException('No user exist with the prvided Email', 200);
    let OTP = otpGenerator.generate(4, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
    //console.log(OTP);
    transporter.sendMail({
        from: process.env.EMAIL_USERNAME, // sender address
        to: email, // list of receivers
        subject: `Forget password code is ${OTP}`, // Subject line
        text: `Your OTP forget password is  ${OTP}`, // plain text body
        html: `<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2;">
                    <div style="margin:50px auto;width:70%;padding:20px 0;">
                        <div style="border-bottom:1px solid #eee;">
                            <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600;"> 
                                <img src="cid:LOgo"/>
                            </a>
                        </div>
                        <p style="font-size:1.1em;">Hi,</p>
                        <p>Thank you for choosing Antrak. Use the following OTP to complete your Sign Up procedures. OTP is valid for 5 minutes</p>
                        <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${OTP}</h2>
                        <p style="font-size:0.9em;">Regards,<br/>Antrak Deliveries</p>
                        <hr style="border:none;border-top:1px solid #eee;" />
                        <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300;">
                            <p>Antrak Inc</p>
                            <p>1600 Amphitheatre Parkway</p>
                            <p>California</p>
                        </div>
                    </div>
                </div>` ,
        attachments: [{
            filename: 'logo.png',
            path: __dirname + '/logo.png',
            cid: 'LOgo' //same cid value as in the html img src
        }]
    }, function (error, info) {
        if (error) {
            throw CustomException(error, 200)
        }
        else {
            let DT = new Date();
            let eDT = new Date();
            eDT.setMinutes(eDT.getMinutes() + 3);
            ForgetPassword.create({
                requested_at: DT.toString(),
                OTP,
                expiry_at: eDT.toString(),
                UserId: user.id,
            }).then((dat) => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': ' Verification code has been sent',
                    'Response': [{
                        userId: `${user.id}`,
                        forgetRequestId: `${dat.id}`
                    }],
                    'errors': '',
                })
            });
        }
    });
}));

/*
            5. Change passowrd in response to OTP send to Email
    ________________________________________________________________
    a. Find the user and requested OTP from the table.
    b. If OTP doesn't match, Return--> Erro 
    c. Check the time for OTP
    d. If current time is not less than Expiry time, Return -->Error 
    f. Hash the password and update it in the table
    g. Return the sucess response
*/
router.post('/passchange', asyncMiddleware(async (req, res) => {
    const { userId, forgetRequestId, password, OTP } = req.body;
    const user = await Users.findOne({ where: { id: userId } })
    const forgetData = await ForgetPassword.findOne({ where: { id: forgetRequestId } });
    if (forgetData.OTP === OTP) {
        const cDT = new Date().toString();
        //console.log(Date.parse(cDT), Date.parse(forgetData.expiry_at))
        if (Date.parse(cDT) < Date.parse(forgetData.expiry_at)) {
            bcrypt.hash(password, 10).then((hash) => {
                Users.update({ password: hash }, { where: { id: user.id } })
                    .then(() => {
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Password Changed',
                            'Response': [{
                                UserId: `${userId}`,
                                firstName: `${user.firstName}`,
                                lastName: `${user.lastName}`,
                                email: `${user.email}`,
                                accessToken: '',
                            }],
                            'errors': '',
                        })
                    })
            });
        }
        else {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'OTP Expired',
                'Response': [],
                'errors': 'Time Out for this OTP',
            })

        }
    }
    else {
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Invalid OTP',
            'Response': [],
            'errors': 'The entered OTP does not match the reqested OTP',
        })
    }
}));

/*
            6. Get details of driver using ID
    _________________________________________________________
    a. Get the driver data from users table 
    b. Get all the bookings completed by driver
    c. Get Earnings, no. of bookings for last 7 & 30 days
    d. Calculate the avg rating of driver
    e. Get the Vehicle Docs and Vehicle imgs and store them in two arrays
    f. Return the desired response   
*/
router.get('/getby/:id', asyncMiddleware(async (req, res) => {
    const userId = req.params.id;
    //return res.json(userId)
    const driverData = await Users.findOne({ where: { id: userId }, include: [{ model: DriverDetails, include: { model: VehiclesType } }, { model: VehicleDetails, where: { status: 1 }, include: { model: VehicleImages } }] });
    const address = await Address.findOne({ where: { UserId: userId } });
    //return res.json(driverData)
    // Filtering Vehicle Images
    let docs = [], images = [];
    if (driverData.VehicleDetails[0]) {
        driverData.VehicleDetails[0].VehicleImages.map((dat, idx) => {
            if (dat.title === 'Vehicle Image') {
                images.push(dat.image);
            }
            if (dat.title === 'Vehicle Document') {
                docs.push(dat.image);
            }
        });
    }
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'Driver Details',
        'Response': {
            id: driverData.id,
            name: `${driverData.firstName} ${driverData.lastName}`,
            email: driverData.email,
            phoneNum: driverData.phoneNum,
            profilePicture: driverData.profilePicture,
            SSN: address ? address.state : 'No record',
            vehicleType: driverData.DriverDetails[0] ? driverData.DriverDetails[0].VehiclesType.name : 'No record',
            licenseData: driverData.DriverDetails[0] ? {
                issueDate: driverData.DriverDetails[0].licIssueDate,
                expiryDate: driverData.DriverDetails[0].licExpiryDate,
                issueState: driverData.DriverDetails[0].issuingState,
                Licnumber: driverData.DriverDetails[0].licNum,
                frontPhoto: driverData.DriverDetails[0].licFrontPhoto,
                backPhoto: driverData.DriverDetails[0].licBackPhoto,
            } : 'No record',
            VehicleData: driverData.VehicleDetails[0] ? {
                make: driverData.VehicleDetails[0].make,
                model: driverData.VehicleDetails[0].model,
                year: driverData.VehicleDetails[0].year,
                plateNum: driverData.VehicleDetails[0].licPlate,
                color: driverData.VehicleDetails[0].color,
                documents: docs,
                images: images,
            } : 'No record',

        },
        'errors': "",
    });
}));

/*
            7. Change Vehicle
    ____________________________________________________________________
    a. Get data from request body
    b. Update the status to false of all the previous images
    c. Get vehicle images and docs from request and create two arrays of data and concat them
    d. Create vehicle details entry in the table 
    e. Create vehicle images entry in the table
    f. Return success message 
*/
router.post('/change/vehicle', upload.fields([{ name: 'vehicleImages' }, { name: 'vehicleDocs' }]), asyncMiddleware(async (req, res) => {
    const { make, model, year, licPlate, color, UserId, vehicleDetailsId, VehiclesTypeId } = req.body;
    //console.log(req.body);
    // VehicleDetails.update({ status: false }, { where: { id: vehicleDetailsId } });
    const type = await VehicleDetails.findOne({ where: { id: vehicleDetailsId } });

    VehicleImages.update({ status: false }, { where: { VehicleDetailId: vehicleDetailsId } });


    let tmpArr1 = [];
    let tmpArr2 = [];
    const cDT = new Date().toString();
    req.files.vehicleImages.map((vdat, index) => {
        let tmpObj = {
            title: `Vehicle Image`,
            image: vdat.path,
            uploadTime: cDT,
            status: true,
        };
        tmpArr1[index] = tmpObj;
    })
    req.files.vehicleDocs.map((vdat, index) => {
        let tmpObj = {
            title: `Vehicle Document`,
            image: vdat.path,
            uploadTime: cDT,
            status: true,
        };
        tmpArr2[index] = tmpObj;
    })
    let arr = tmpArr1.concat(tmpArr2);

    const details = await VehicleDetails.findOne({ where: { id: vehicleDetailsId } });
    if (details) {
        details.make = make;
        details.model = model;
        details.year = year;
        details.licPlate = licPlate;
        details.color = color;
        details.UserId = UserId;
        details.status = true;
        details.save()
            .then((dat) => {

                arr.map(ele => {
                    ele.VehicleDetailId = dat.id
                });
                VehicleImages.bulkCreate(arr)
                    .then((dat) => {
                        DriverDetails.update(
                            // { VehiclesTypeId: VehiclesTypeId }, 
                            { where: { UserId: UserId } })
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Vehicle Updated',
                            //'Response': [dat],
                            'errors': "",
                        });
                    })
            })
    }
    else {
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Vehicle Details not found',
            //'Response': [dat],
            'errors': "",
        });
    }

    // VehicleDetails.update({ where: { id: vehicleDetailsId } }, {
    //     make,
    //     model,
    //     year,
    //     licPlate,
    //     color,
    //     UserId,
    //     status: true,
    //     // VehiclesTypeId: type.VehiclesTypeId,
    // })
    // .then((dat) => {

    //     arr.map(ele => {
    //         ele.VehicleDetailId = dat.id
    //     });
    //     VehicleImages.bulkCreate(arr)
    //         .then((dat) => {
    //             DriverDetails.update(
    //                 // { VehiclesTypeId: VehiclesTypeId }, 
    //                 { where: { UserId: UserId } })
    //             res.status(200).json({
    //                 'ResponseCode': '1',
    //                 'ResponseMessage': 'Vehicle Updated',
    //                 //'Response': [dat],
    //                 'errors': "",
    //             });
    //         })
    // })
}));

/*
            8. Log In
    ____________________________________________________________
    a. Find User from DB 
    b. If not exist, Return --> No data exist
    c. If exist, then match the password
    d. If not match, Return --> Incorrect Password
    e. Check if vehicle details are added or not.
    f. If not, Return --> Add vehicle details
    g. Check if driver details are added or not
    h. if not, Return --> Add Driver details
    i. Check if address details are added or not
    j. if not, Return --> Add Address details
    k. Check if email verified or not
    l. if not, send email , if email not send return --> error , else return --> Success msg
    m. Create access Token, remove from black list if it is there 
    n. Check if user is approved or not from Admin
    o. if not approved, Return --> Logged in with status code that pending approval
    p. Logged In successfully
    
*/
router.post('/login', asyncMiddleware(async (req, res) => {
    const { email, password, deviceToken } = req.body;
    const user = await Users.findOne({ where: { email: email, UserCategoryId: 3, deleted: null }, include: [{ model: DriverDetails }, { model: VehicleDetails }] });
    //return res.json(user)
    if (!user) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'User does not exist',
            'Response': [],
            'errors': 'User does not exist',
        })
    }
    const addressExist = await Address.findOne({ where: { UserId: user.id } });
    const OTPExist = await EmailVerification.findOne({ where: { UserId: user.id } })
    bcrypt.compare(password, user.password)
        .then(match => {
            if (!match) {
                return res.json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Incorrect email & password combination',
                    'Response': [],
                    'errors': 'Please enter valid information',
                })
            }
            if (user.VehicleDetails.length < 1) {
                return res.json({
                    'ResponseCode': '4',
                    'ResponseMessage': 'Add vehicle Details',
                    'Response': [
                        {
                            userId: user.id,
                            "firstName": `${user.firstName}`,
                            "lastName": `${user.lastName}`,
                            "email": `${user.email}`,
                            "accessToken": "",
                        }],
                    'errors': '',
                })
            }
            else if (user.DriverDetails.length < 1) {
                return res.json({
                    'ResponseCode': '5',
                    'ResponseMessage': 'Add Identification Details',
                    'Response': [{
                        userId: user.id,
                        "firstName": `${user.firstName}`,
                        "lastName": `${user.lastName}`,
                        "email": `${user.email}`,
                        "accessToken": ""
                    }],
                    'errors': '',
                })
            }
            else if (!addressExist) {
                return res.json({
                    'ResponseCode': '6',
                    'ResponseMessage': 'Add Address Details',
                    'Response': [{
                        userId: user.id,
                        "firstName": `${user.firstName}`,
                        "lastName": `${user.lastName}`,
                        "email": `${user.email}`,
                        "accessToken": ""
                    }],
                    'errors': '',
                })
            }
            else if (!user.email_verified_at) {
                let OTP = otpGenerator.generate(4, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
                transporter.sendMail({
                    from: process.env.EMAIL_USERNAME, // sender address
                    to: email, // list of receivers
                    subject: `Your OTP for Antrak is ${OTP}`, // Subject line
                    text: `Your OTP for Antrak is ${OTP}`, // plain text body
                },
                    function (error, info) {
                        // if (error) {
                        //     return res.status(200).json({
                        //         'ResponseCode': '0',
                        //         'ResponseMessage': 'Error sending in Email',
                        //         'Response': [],
                        //         'errors': '',
                        //     })
                        // }
                        // else 
                        {
                            let DT = new Date().toString();
                            if (OTPExist) {
                                //res.send('Exist') 
                                EmailVerification.update({ requested_at: DT, OTP }, { where: { UserId: user.id } })
                                    .then(() => {
                                        const accessToken = sign({ username: user.email, id: user.id, }, 'Important Text', { expiresIn: '365d' })
                                        return res.status(200).json({
                                            'ResponseCode': '2',
                                            'ResponseMessage': 'Verification Email has been sent update !',
                                            'Response': [{
                                                userId: user.id,
                                                firstName: `${user.firstName}`,
                                                lastName: `${user.lastName}`,
                                                email: `${user.email}`,
                                                accessToken: accessToken,
                                            }],
                                            'errors': '',
                                        })
                                    })
                            }
                            else {
                                EmailVerification.create({
                                    requested_at: DT,
                                    OTP,
                                    UserId: user.id,
                                })
                                    .then(() => {

                                        const accessToken = sign({ username: user.email, id: user.id, }, 'Important Text', { expiresIn: '365d' })
                                        return res.status(200).json({
                                            'ResponseCode': '2',
                                            'ResponseMessage': 'Verification Email has been sent',
                                            'Response': [{
                                                userId: user.id,
                                                firstName: `${user.firstName}`,
                                                lastName: `${user.lastName}`,
                                                email: `${user.email}`,
                                                accessToken: accessToken
                                            }],
                                            'errors': '',
                                        })
                                    })
                            }
                        }

                    })
            }
            else {
                const accessToken = sign({ username: user.email, id: user.id, }, 'Important Text', { expiresIn: '365d' })
                BlackList.findOne({ where: { accessToken: accessToken } })
                    .then(dat => {
                        if (dat) {
                            BlackList.destroy({ where: { accessToeken: accessToken } });
                            Users.update({ deviceToken: deviceToken }, { where: { id: user.id } });
                            if (user.status) {
                                res.status(200).json({
                                    'ResponseCode': '1',
                                    'ResponseMessage': 'Log in successfully',
                                    'Response': [{
                                        "userId": user.id,
                                        "firstName": `${user.firstName}`,
                                        "lastName": `${user.lastName}`,
                                        "email": `${user.email}`,
                                        "accessToken": `${accessToken}`,
                                    }],
                                    'errors': '',
                                });
                            }
                            else {
                                res.status(200).json({
                                    'ResponseCode': '3',
                                    'ResponseMessage': 'Log in successfully',
                                    'Response': [{
                                        "userId": user.id,
                                        "firstName": `${user.firstName}`,
                                        "lastName": `${user.lastName}`,
                                        "email": `${user.email}`,
                                        "accessToken": `${accessToken}`,
                                    }],
                                    'errors': '',
                                });
                            }
                        }
                        else {
                            Users.update({ deviceToken: deviceToken }, { where: { id: user.id } });
                            if (user.status) {
                                res.status(200).json({
                                    'ResponseCode': '1',
                                    'ResponseMessage': 'Log in successfully',
                                    'Response': [{
                                        "userId": user.id,
                                        "firstName": `${user.firstName}`,
                                        "lastName": `${user.lastName}`,
                                        "email": `${user.email}`,
                                        "accessToken": `${accessToken}`,
                                    }],
                                    'errors': '',
                                });
                            }
                            else {
                                res.status(200).json({
                                    'ResponseCode': '3',
                                    'ResponseMessage': 'Log in successfully',
                                    'Response': [{
                                        "userId": user.id,
                                        "firstName": `${user.firstName}`,
                                        "lastName": `${user.lastName}`,
                                        "email": `${user.email}`,
                                        "accessToken": `${accessToken}`,
                                    }],
                                    'errors': '',
                                });
                            }
                        }
                    })
            }
        })


}))

/*
            9. Accept Booking
    __________________________________________________________________
    a. Check the payment amount, if it is less than 1 
        1.1. Change status of booking using API
        1.2. Change the payment receieved flag to true 
        1.3. Send Notification to customer 
        1.4. Return the success message
    b. Charge the customer using PI id
        2.1. Change status of booking using API
        2.2. Change the payment receieved flag to true 
        2.3. Send Notification to customer 
        2.4. Return the success message
*/
router.post('/accept', asyncMiddleware(async (req, res) => {
    const { bookingId, UserId } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId } });
    if(booking.DriverId !== null)
    {
      return  res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Job already accepted!',
            'Response': [],
            'errors': 'Job already accepted!',
        });
    }
    const user = await Users.findOne({ where: { id: booking.UserId } });
    // charge the payment if there is some amount else just accept the booking
    if (booking.grandTotal < 1) {
        axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Accepted", "bookingId": bookingId })
            .then(done => {
                Booking.update({ DriverId: UserId, paymentRecieved: true }, { where: { id: bookingId } })
                    .then(succ => {
                        let message = {
                            to: `${user.deviceToken}`,
                            notification: {
                                title: 'Booking Request Accepted',
                                body: 'Our rider has accepted your booking request'
                            },
                        };
                        fcm.send(message, function (err, response) {
                            //console.log("Successfully sent with response: ", response);
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Job Accepted',
                                'Response': [{
                                    id: `${booking.UserId}`
                                }],
                                'errors': '',
                            });

                        });
                    })
                    .catch(err => {
                        //res.json('DB update Failed')
                        res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Update to DB failed',
                            'Response': [],
                            'errors': '',
                        });
                    })
            })
            .catch(err => {
                //res.send('status change failed');
                res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Couldnt update booking Status',
                    'Response': [],
                    'errors': '',
                });
            })
    }
    else {
        if (booking.paymentRecieved) {
            axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Accepted", "bookingId": bookingId })
                .then(done => {
                    //console.log(done)
                    Booking.update({ DriverId: UserId }, { where: { id: bookingId } })
                        .then(succ => {
                            let message = {
                                to: `${user.deviceToken}`,
                                notification: {
                                    title: 'Booking Request Accepted',
                                    body: 'Our rider has accepted your booking request'
                                },
                            };
                            fcm.send(message, function (err, response) {
                                //console.log("Successfully sent with response: ", response);
                                return res.status(200).json({
                                    'ResponseCode': '1',
                                    'ResponseMessage': 'Job Accepted',
                                    'Response': [{
                                        id: `${booking.UserId}`
                                    }],
                                    'errors': '',
                                });

                            });
                        })
                        .catch(err => {
                            //res.json('DB update Failed')
                            return res.status(200).json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Update to DB failed',
                                'Response': [],
                                'errors': '',
                            });
                        })
                })
                .catch(err => {
                    //res.send('status change failed');
                    return res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Couldnt update booking Status',
                        'Response': [],
                        'errors': 'Couldnt update booking Status',
                    });
                })
        }
        stripe.paymentIntents.capture(`${booking.piId}`)
            .then(charge => {
                axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Accepted", "bookingId": bookingId })
                    .then(done => {
                        Booking.update({ DriverId: UserId, paymentRecieved: true, chargeId: charge.charges.data[0].id }, { where: { id: bookingId } })
                            .then(succ => {
                                let message = {
                                    to: `${user.deviceToken}`,
                                    notification: {
                                        title: 'Booking Request Accepted',
                                        body: 'Our rider has accepted your booking request'
                                    },
                                };
                                fcm.send(message, function (err, response) {
                                    //console.log("Successfully sent with response: ", response);
                                    res.status(200).json({
                                        'ResponseCode': '1',
                                        'ResponseMessage': 'Job Accepted',
                                        'Response': [{
                                            id: `${booking.UserId}`
                                        }],
                                        'errors': '',
                                    });

                                });
                            })
                            .catch(err => {
                                //res.json('DB update Failed')
                                res.status(200).json({
                                    'ResponseCode': '0',
                                    'ResponseMessage': 'Update to DB failed',
                                    'Response': [],
                                    'errors': '',
                                });
                            })
                    })
                    .catch(err => {
                        //res.send('status change failed');
                        return res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Couldnt update booking Status',
                            'Response': [],
                            'errors': '',
                        });
                    })
            })
            .catch(err => {
                //console.log(err);
                return res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': err.raw.message,
                    'Response': [],
                    'errors': 'Error capturing payment',
                });
            })
    }

}));

/*
            10. Logout 
    ____________________________________________________________
    a. If accessToken exist in the Blacklist, update it 
    b. If not, create a new one
*/
router.get('/logout', validateToken, asyncMiddleware(async (req, res) => {
    const accessToken = req.header('accessToken');
    const exist = await BlackList.findOne({ where: { accessToken: accessToken } });
    //res.json(exist)
    if (exist) {
        BlackList.update({ accessToken: accessToken }, { where: { accessToken: accessToken } })
            .then(dat => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Log Out successfully',
                    'Response': [],
                    'errors': '',
                })
            })
            .catch(err => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Error logging Out',
                    'Response': [],
                    'errors': '',
                })
            })
    }
    else {
        BlackList.create({ accessToken: accessToken })
            .then(dat => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Log Out successfully',
                    'Response': [],
                    'errors': '',
                })
            })
            .catch(err => {
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Error logging Out create ',
                    'Response': [],
                    'errors': '',
                })
            })
    }
}))

/*
            11. Session API
    _____________________________________________________________
    a. Check if user status is true
    b. If not, Return --> Log in with pending approval
    c. Check if its online using firebase
    d. if not, Return --> log in with status false
    e. Return --> log in with status false
*/
router.post('/session', validateToken, asyncMiddleware(async (req, res) => {
    const user = await Users.findOne({ where: { id: req.user.id } });
    const driverDetails = await DriverDetails.findOne({ where: { UserId: req.user.id } });
    //return res.json(driverDetails)
    if (!driverDetails) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': 'Please Login to continue',
            'Response': {
                status: false,
            },
            'errors': 'User not logged In',
        })
    }
    let cDate = new Date();
    let expDate = new Date(driverDetails.licExpiryDate)
    var Difference_In_Time = expDate.getTime() - cDate.getTime();
    // To calculate the no. of days between two dates
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    //console.log(Difference_In_Days)
    // Applying condition license expiry date
    if (Difference_In_Days >= 1 && Difference_In_Days < 30) {
        let message = {
            to: `${user.deviceToken}`,
            notification: {
                title: `Please update your license.`,
                body: `Your license is about to expire in ${Math.ceil(Difference_In_Days)} days`,
            },
        };
        fcm.send(message, function (err, response) { });
    }
    else if (Difference_In_Days > -1 && Difference_In_Days < 1) {
        let message = {
            to: `${user.deviceToken}`,
            notification: {
                title: `Please update your license.`,
                body: `Your license will expire today`,
            },
        };
        fcm.send(message, function (err, response) { });

    }
    else if (Difference_In_Days < -1 && Difference_In_Days > -30) {
        let blockDays = 30 + Math.ceil(Difference_In_Days);
        let message = {
            to: `${user.deviceToken}`,
            notification: {
                title: `Your license has been expired. Please update it.`,
                body: ` Your profile will be blocked in ${blockDays} days`,
            },
        };
        fcm.send(message, function (err, response) { });

    }
    else if (Difference_In_Days < -30) {
        Users.update({ status: 0 }, { where: { id: req.user.id } })
            .then(() => {
                let message = {
                    to: `${user.deviceToken}`,
                    notification: {
                        title: `Your license has been expired. Please update it.`,
                        body: `You are blocked due to license expiry issue`,
                    },
                };
                fcm.send(message, function (err, response) { });
                return res.status(200).json({
                    'ResponseCode': '2',
                    'ResponseMessage': 'Log in Successful',
                    'Response': {
                        status: false
                    },
                    'errors': '',
                })
            })
    }
    if (user.status) {
        const userId = req.user.id;
        axios.get(process.env.FIREBASE_URL)
            .then(dat => {
                //console.log(dat.data)
                if (!dat.data.drivers) {
                    return res.status(200).json({
                        'ResponseCode': '1',
                        //'ResponseMessage': 'Log in Successful',
                        'ResponseMessage': 'Log in Successful',
                        'Response': {
                            status: false
                        },
                        'errors': '',
                    })
                }
                let online = Object.hasOwn(dat.data.drivers, `${user.id}`);
                if (!online) {
                    return res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Log in Successful',
                        'Response': {
                            status: false
                        },
                        'errors': '',
                    })
                }
                //return res.json(online);
                return res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Log in Successful',
                    'Response': {
                        status: online
                    },
                    'errors': '',
                })
            })
            .catch(err => {
                return res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Error',
                    'Response': {
                        status: false
                    },
                    'errors': `${err}`,
                })
            })
    }
    else {
        return res.status(200).json({
            'ResponseCode': '2',
            'ResponseMessage': 'Log in Successful',
            'Response': {
                status: false
            },
            'errors': '',
        })
    }

}))

/*
            12. Get list of cancel reasons
    _____________________________________________________
*/
router.get('/cancelreasons', asyncMiddleware(async (req, res) => {
    const appSetting = await AppSettings.findAll({ where: { title: 'Cancel Reason Driver' } })
    if (!appSetting) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No reason found',
            'Response': [],
            'errors': '',
        });
    }
    let outArr = [];
    appSetting.map((reas, index) => {
        let tmpObj = {
            reasonText: reas.value,
        }
        outArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'List of reasons',
        'Response': outArr,
        'errors': '',
    })
}));

/*
            12. Get list of cancel reasons
    _____________________________________________________
*/
router.get('/cancelreasons/admin', asyncMiddleware(async (req, res) => {
    const appSetting = await AppSettings.findAll({ where: { title: 'Cancel Reason Driver' } })
    if (!appSetting) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No reason found',
            'Response': [],
            'errors': '',
        });
    }
    let outArr = [];
    appSetting.map((reas, index) => {
        let tmpObj = {
            id: reas.id,
            reasonText: reas.value,
        }
        outArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'List of reasons',
        'Response': outArr,
        'errors': '',
    })
}));
/*
            13. Cancel Booking
    _____________________________________________________
    a. Create a new entry in Cancel History table
    b. Change the booking status to its initialised state
    c. Send notification
    d. Return --> Success Msg

*/
router.post('/booking/cancel', asyncMiddleware(async (req, res) => {
    const { bookingId, UserId, reason, note } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId } });
    const user = await Users.findOne({ where: { id: booking.UserId } });
    const cDate = new Date();
    let cYear = cDate.getFullYear();
    let cMonth = cDate.getMonth() + 1;
    let cdt = cDate.getDate()
    //return res.json(`${cMonth}/${cdt}/${cYear}`);
    // Check how many booking have canclled today
    const cancelledBookings = await CancelHistory.findAll({
        where: {
            UserId: UserId, cancelledAt: {
                [Op.between]: [`${cMonth}/${cdt}/${cYear} 00:00`, `${cMonth}/${cdt + 1}/${cYear} 00:00`]
            }
        }
    });
    // return res.json(cancelledBookings.length);
    // get the amount to be charged from DB as well - Add in charges -
    let amount = 0;
    /*
        Cases: 
        Case 2: 
        Case 3: 
        Default:
    */
    switch (booking.BookingStatusId) {
        case 2:
            //return res.json('Accepted case')
            // getting the amount to be deducted
            //_______________________________________
            // let dollartocents  = 100;
            // amount = amount * dollartocents;
            // // Amount in cents
            // amount = Math.round(amount);
            if (cancelledBookings.length > 3) {
                // If cancelled bookings is greater than 3 for current day
                amount = 5;
            }
            //_________________________________________
            cancelbooking(reason, note, amount, bookingId, UserId)
                .then(done => {
                    Booking.update({ condition: true, BookingStatusId: 1, DriverId: null }, { where: { id: bookingId } });
                    notifyAllActiveDrivers(UserId, bookingId)
                        .then(() => {
                            let message = {
                                to: `${user.deviceToken}`,
                                notification: {
                                    title: 'Order Cancelled by Rider',
                                    body: 'Waiting for new rider to accept the order'
                                },
                            };
                            fcm.send(message, function (err, response) {
                                if (err) {
                                    console.log("Something has gone wrong!");
                                    //res.json(err);
                                } else {
                                    console.log("Successfully sent with response: ", response);
                                    //res.json(response);
                                }
                            });
                            return res.json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Booking Cancelled',
                                'Response': [],
                                'errors': '',
                            })
                        })
                })
                .catch(err => {
                    return res.json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Error in cancelling',
                        'Response': [],
                        'errors': 'Error in cancelling',
                    });
                })
            break;
        case 3:   // On the way case
            //return res.json('Accepted case')
            // getting the amount to be refund
            //_______________________________________
            // let dollartocents  = 100;
            //let amount = 0;
            // amount = amount * dollartocents;
            // // Amount in cents
            // amount = Math.round(amount);
            if (cancelledBookings.length > 3) {
                // If cancelled bookings is greater than 3 for current day
                amount = 5;
            }
            //_________________________________________
            cancelbooking(reason, note, amount, bookingId, UserId)
                .then(done => {
                    Booking.update({ condition: true, BookingStatusId: 1, DriverId: null }, { where: { id: bookingId } });
                    notifyAllActiveDrivers(UserId, bookingId)
                        .then(() => {
                            let message = {
                                to: `${user.deviceToken}`,
                                notification: {
                                    title: 'Order Cancelled by Rider',
                                    body: 'Waiting for new rider to accept the order'
                                },
                            };
                            fcm.send(message, function (err, response) {
                                if (err) {
                                    console.log("Something has gone wrong!");
                                    //res.json(err);
                                } else {
                                    console.log("Successfully sent with response: ", response);
                                    //res.json(response);
                                }
                            });
                            return res.json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Booking Cancelled',
                                'Response': [],
                                'errors': '',
                            })
                        })
                })
                .catch(err => {
                    return res.json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Error in cancelling',
                        'Response': [],
                        'errors': 'Error in cancelling',
                    });
                })
            break;
        default:
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Cannot cancel this booking',
                'Response': [],
                'errors': 'Booking cant be cancelled at this stage',

            })
    }

}));

/*
            14. Start Ride
    _________________________________________________________
    a. Find the booking with the required details
    b. Get the drivers Lat & Lng using FIREBASE Api 
    c. Calculate the ETA between Driver's to pickup 
    d. Update the booking status
    e. Send the notification 
    f. Return --> Success message
*/
router.post('/startride', asyncMiddleware(async (req, res) => {
    const { userId, bookingId } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }] });
    if (booking.BookingStatusId === 7) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': `Booking cancelled by customer`,
            'Response': [],
            'errors': 'Please go to job pool page',
        })
    }
    const user = await Users.findOne({ where: { id: booking.UserId } });
    const plng = booking.pickupAddress.lng;
    const plat = booking.pickupAddress.lat;
    axios.get(process.env.FIREBASE_URL)
        .then(dat => {
            let dId = dat.data.drivers;
            let driverLocation = dId[`${userId}`];
            if (!driverLocation) {
                return res.json({
                    'ResponseCode': '0',
                    'ResponseMessage': `Driver offline`,
                    'Response': [],
                    'errors': '',
                })
            }
            const drlng = driverLocation['lng'];
            const drlat = driverLocation['lat'];
            //return res.json(drlng)
            axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${drlat},${drlng}&destination=${plat},${plng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
                .then((resp) => {
                    let ETA = resp.data.routes[0].legs[0].duration.text;
                    //return res.json(ETA)
                    axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Driver On the way", "bookingId": bookingId })
                        .then(succ => {
                            let message = {
                                to: `${user.deviceToken}`,
                                notification: {
                                    title: 'Rider on the way',
                                    body: 'Our rider is on the way to collect your order.'
                                },
                            };
                            fcm.send(message, function (err, response) {
                                if (err) {
                                    console.log("Something has gone wrong!");
                                    //res.json(err);
                                } else {
                                    console.log("Successfully sent with response: ", response);
                                    //res.json(response);
                                }
                            });
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Rider on the way',
                                'Response': [{
                                    ETA,
                                    building: booking.building ? booking.building : '',
                                    aptNum: booking.aptNum ? booking.aptNum : '',
                                    city: booking.city ? booking.city : '',
                                    state: booking.state ? booking.state : '',
                                    zip: booking.zip ? booking.zip : '',
                                    pickupAddress: booking.pickupAddress.exactAddress,
                                    lat: booking.pickupAddress.lat,
                                    lng: booking.pickupAddress.lng,
                                    senderName: `${booking.User.firstName} ${booking.User.lastName}`,
                                    senderId: booking.User.id,
                                    senderPhoneNum: booking.User.phoneNum,
                                    senderDvToken: booking.User.deviceToken,
                                }],
                                'errors': '',
                            })
                        });
                })
                .catch(err => {
                    res.json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Error in getting ETA',
                        'Response': [],
                        'errors': 'Error in getting ETA',
                    });
                })
        })
        .catch(err => {
            res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error in driver data',
                'Response': [],
                'errors': 'Error in driver data',
            });
        })

}));

/*
            15.  Arrived at pickup point
    ________________________________________________________________
    a. Check if any of the package has 18+ Category
    b. Send notification
    c. Return -->  Success message
*/
router.post('/arrived', asyncMiddleware(async (req, res) => {
    const { bookingId } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId }, include: [{ model: Package, include: { model: Category } }] });
    if (booking.BookingStatusId === 7) {
        return res.json({
            'ResponseCode': '0',
            'ResponseMessage': `Booking cancelled by customer`,
            'Response': [],
            'errors': 'Please go to job pool page',
        })
    }
    const user = await Users.findOne({ where: { id: booking.UserId } });
    let adultCategory = false;
    booking.Packages.map((pack, index) => {
        if (pack.Category.adult_18plus === true) {
            adultCategory = true
        }
    });
    let message = {
        to: `${user.deviceToken}`,
        notification: {
            title: 'Rider Arrived',
            body: 'Our rider has arrived at your location'
        },
    };
    fcm.send(message, function (err, response) {
        if (err) {
            console.log("Something has gone wrong!");
            //res.json(err);
        } else {
            console.log("Successfully sent with response: ", response);
            //res.json(response);
        }
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Rider arrived',
        'Response': {
            'adultStatus': adultCategory,
        },
        'errors': '',
    })
}));

/*
            16.  Driver Out for Delivery
    ________________________________________________________________
    a. Change the booking status by calling API
    b. Send notification
    c. Return -->  Success message
*/
router.post('/outfordelivery', asyncMiddleware(async (req, res) => {
    const { bookingId } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'deliveryAddress' }] });
    //return res.json(booking)
    const driver = await Users.findOne({ where: { id: booking.DriverId } });
    //return res.json(user)
    const dlng = booking.deliveryAddress.lng;
    const dlat = booking.deliveryAddress.lat;
    axios.get(process.env.FIREBASE_URL)
        .then(dat => {
            let dId = dat.data.drivers;
            //return res.json(dId);
            let driverLocation = dId[`${driver.id}`];
            //return res.json(driverLocation);
            if (!driverLocation) {
                return res.json({
                    'ResponseCode': '0',
                    'ResponseMessage': `Driver offline`,
                    'Response': [],
                    'errors': 'Driver offline',
                })
            }
            const drlng = driverLocation['lng'];
            const drlat = driverLocation['lat'];
            //return res.json(booking);
            axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${dlat},${dlng}&destination=${drlat},${drlng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
                .then((resp) => {
                    let ETA = resp.data.routes[0].legs[0].duration.text;
                    axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Driver Out for Delivery", "bookingId": bookingId })
                        .then(succc => {
                            let message = {
                                to: `${booking.User.deviceToken}`,
                                notification: {
                                    title: 'Ride started',
                                    body: 'Rider on his way to deliver your order'
                                },
                            };
                            fcm.send(message, function (err, response) { });
                            return res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Out for delivery',
                                'Response': {
                                    ETA,
                                    building: booking.deliveryAddress.building ? booking.deliveryAddress.building : '',
                                    aptNum: booking.deliveryAddress.aptNum ? booking.deliveryAddress.aptNum : '',
                                    city: booking.deliveryAddress.city ? booking.deliveryAddress.city : '',
                                    state: booking.deliveryAddress.state ? booking.deliveryAddress.state : '',
                                    zip: booking.deliveryAddress.zip ? booking.deliveryAddress.zip : '',
                                    deliveryAddress: `${booking.deliveryAddress.building}, ${booking.deliveryAddress.exactAddress}, ${booking.deliveryAddress.city}, ${booking.deliveryAddress.state}, ${booking.deliveryAddress.zip}`,
                                    recieverName: booking.rName,
                                    recieverPhoneNum: booking.deliveryAddress.phoneNum,
                                    senderDvToken: booking.User.deviceToken,
                                    lat: dlat,
                                    lng: dlng,
                                },
                                'errors': '',
                            })
                        })
                        .catch(err => {
                            res.status(200).json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Error getting ETA',
                                'Response': [],
                                'errors': '',
                            })

                        })
                })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error getting Driver Data',
                'Response': [],
                'errors': '',
            });
        })
}));
/*
            17. Reached at Drop Off Location
    _____________________________________________________________________
    a. a. Check if any of the package has 18+ Category
    b. Send notification
    c. Return -->  Success message
*/
router.post('/reached', asyncMiddleware(async (req, res) => {
    const { bookingId } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId }, include: [{ model: Package, include: { model: Category } }] });
    const user = await Users.findOne({ where: { id: booking.UserId } });
    let adultCategory = false;
    booking.Packages.map((pack, index) => {
        if (pack.Category.adult_18plus === true) {
            adultCategory = true
        }
    });
    let message = {
        to: `${user.deviceToken}`,
        notification: {
            title: 'Rider reached',
            body: 'Our rider has reached at your dropoff location'
        },
    };
    fcm.send(message, function (err, response) {
        if (err) {
            console.log("Something has gone wrong!");
            //res.json(err);
        } else {
            console.log("Successfully sent with response: ", response);
            //res.json(response);
        }
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Rider reached',
        'Response': {
            'adultStatus': adultCategory,
        },
        'errors': '',
    })
}));

/*
            18. Get profile data of driver
    ________________________________________________________________
    a. Get user using ID, if no Return --> No user exist
    b. Get the desired data
    c. Return --> Successfully
*/
router.get('/getprofile', validateToken, asyncMiddleware(async (req, res) => {
    //const userId = req.params.userId;
    const userId = req.user.id;
    const user = await Users.findOne({ where: { id: userId } })
    if (!user) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No user exist',
            'Response': [],
            'errors': 'No user ',
        });
    }
    const address = await Address.findOne({ where: { UserId: userId } });
    const details = await DriverDetails.findOne({ where: { UserId: userId } });
    const vehicleData = await VehicleDetails.findOne({ where: { status: 1, UserId: userId }, include: [{ model: VehicleImages }] })
    let imgArr = [];
    vehicleData.VehicleImages.map((dat, index) => {
        if (dat.title === 'Vehicle Image') {
            imgArr.push(dat.image)
        }
    });
    //return res.json(vehicleData)
    let tmpObj = {
        id: user.id,
        firstName: `${user.firstName}`,
        lastName: `${user.lastName}`,
        phoneNum: `${user.phoneNum}`,
        email: user.email,
        profilePicture: user.profilePicture,
        exactaddress: address ? address.exactAddress : "No address found",
        building: address.building,
        city: address.city,
        state: address.state,
        zip: address.zip,
        SSN: details.SNN ? details.SNN : "",
        plateNum: vehicleData.licPlate,
        vehicleImages: imgArr,
    }
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'User Data',
        'Response': tmpObj,
        'errors': '',
    });
}));

/*
            19. Update Profile of driver
    _____________________________________________________________
*/
router.post('/updateprofile', asyncMiddleware(async (req, res) => {
    const { userId, firstName, lastName, address, SSN } = req.body;
    Users.update({ firstName: firstName, lastName: lastName }, { where: { id: userId } })
        .then(succ => {
            Address.update({ city: address.city, state: address.state, building: address.building, zip: address.zip, exactAddress: address.exactAddress }, { where: { UserId: userId } })
                .then(succ => {
                    // SSN update
                    DriverDetails.update({ SNN: SSN }, { where: { UserId: userId } })
                        .then((don) => {
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Profile Updated',
                                'Response': [],
                                'errors': '',
                            });
                        })
                        .catch(err => {
                            res.status(200).json({
                                'ResponseCode': '0',
                                'ResponseMessage': 'Error Updating SSN',
                                'Response': [],
                                'errors': '',
                            });
                        })

                })
                .catch(err => {
                    res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Error Updating Address',
                        'Response': [],
                        'errors': '',
                    });
                })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error Updating Profile',
                'Response': [],
                'errors': '',
            });
        })
}));

/*
            19. Update Password with old password
    _____________________________________________________________
*/
router.put('/updatepassword', asyncMiddleware(async (req, res) => {
    const { userId, oldPassword, newPassword } = req.body;
    //const user = req.user;
    const cUser = await Users.findOne({ where: { id: userId } });
    bcrypt.compare(oldPassword, cUser.password)
        .then((match) => {
            if (!match) { throw new CustomException('Your old password is incorrect', 200); }
            bcrypt.hash(newPassword, 10)
                .then((hash) => {
                    Users.update({ password: hash }, { where: { id: cUser.id } })
                        .then(
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Password Updated',
                                'Response': ['Your password has been updated'],
                                'errors': '',
                            })
                        )
                })
        })
        .catch((err) => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Password Not Updated',
                'Response': [],
                'errors': 'Your old password is incorrect',
            })
        })
}));

/*
            20. Get License Data of driver
    _____________________________________________________________
*/
router.get('/getlicensedetails/:id', asyncMiddleware(async (req, res) => {
    const driverDet = await DriverDetails.findOne({ where: { UserId: req.params.id } });
    if (!driverDet) {
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'No data exist',
            'Response': ["No data exists "],
            'errors': '',
        })
    }
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'License Information',
        'Response': {
            licNum: driverDet.licNum,
            issuingState: driverDet.issuingState,
            issueDate: driverDet.licIssueDate,
            expiryDate: driverDet.licExpiryDate,
            frontImage: driverDet.licFrontPhoto,
            backImage: driverDet.licBackPhoto,
        },
        'errors': '',
    })
}));

/*
            21. Update License Details of driver
    _____________________________________________________________
*/
router.put('/updatelicense', validateToken, upload.fields([{ name: 'licFront' }, { name: 'licBack' }]), asyncMiddleware(async (req, res) => {
    const { licIssueDate, licExpiryDate, licNum, issuingState, UserId } = req.body;
    DriverDetails.update({
        licIssueDate,
        licExpiryDate,
        licNum,
        issuingState,
        licFrontPhoto: req.files['licFront'][0].path,
        licBackPhoto: req.files['licBack'][0].path,
    }, { where: { UserId: UserId } })
        .then((dat) => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'License Updated',
                //'Response': [],
                'errors': "",
            });
        })
}));

/*
            22. Get Vehicle Data of driver
    _____________________________________________________________
*/
router.get('/getvehicledetails', validateToken, asyncMiddleware(async (req, res) => {

    const vehDet = await VehicleDetails.findOne({ where: { status: 1, UserId: req.user.id } });

    // return res.json(vehDet);
    if (!vehDet) {
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'No record Found',
            //'Response': [],
            'errors': "",
        });
    }
    const vehImgs = await VehicleImages.findAll({ where: { status: 1, VehicleDetailId: vehDet.id } });

    const type = await VehiclesType.findOne({ where: { id: vehDet.VehiclesTypeId } });
    const vehicle_types = await VehiclesType.findOne({ where: { status: true } });

    let vehImages = [], vehDocs = [];
    let ic = 0, dc = 0;
    vehImgs.map((img, index) => {
        if (img.title === 'Vehicle Image') {
            vehImages[ic] = img.image;
            ic++;
        }
        if (img.title === 'Vehicle Document') {
            vehDocs[dc] = img.image;
            dc++;
        }
    });

    return res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Vehicle data',
        'Response': {

            vehDetId: vehDet.id,
            images: vehImages,
            model: vehDet.model,
            year: vehDet.year,
            regNum: vehDet.licPlate,
            make: vehDet.make,
            licPlate: vehDet.licPlate,
            color: vehDet.color,
            documents: vehDocs,
            vehicle_types: vehicle_types,
            vehicle_type_name: type?.name ?? "",
            vehicle_type_image: type?.image ?? "",
        },
        'errors': "",
    });

}));

/*
            23. Add Bank Account of driver
    _____________________________________________________________
*/
router.post('/addbank', asyncMiddleware(async (req, res) => {
    const { UserId } = req.body;
    //const exist = await Banks.findAll({where: {UserId: UserId}});
    const driverData = await DriverDetails.findOne({ where: { UserId: UserId } });
    const userdata = await Users.findOne({ where: { id: UserId } });


    const method = await PaymentMethod.findOne({ where: [{ status: true }, { title: "Stripe" }] });
    const stripe = require('stripe')(method.secretKey);




    if (driverData.bank) {
        stripe.accountLinks.create({
            account: `${driverData.bank}`,
            refresh_url: 'https://antrak.zeeshannawaz.com/front/admin/error',
            return_url: 'https://antrak.zeeshannawaz.com/front/admin/sucess',
            type: 'account_onboarding',
            collect: 'eventually_due',
        })
            .then(acctLink => {
                //console.log(account.id);
                //DriverDetails.update({bank: account.id}, {where: {UserId: UserId}});
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Link for adding bank',
                    'Response': {
                        link: acctLink.url,
                    },
                    'errors': "",
                });
            })
    }
    else {

        const account = await stripe.accounts.create({
            type: 'express',
            business_profile: {
                mcc: '4789',
                url: 'https://antrak.zeeshannawaz.com/front',
            }
            ,
        });


        stripe.accountLinks.create({
            account: `${account.id}`,
            refresh_url: 'https://antrak.zeeshannawaz.com/front/admin/error',
            return_url: 'https://antrak.zeeshannawaz.com/front/admin/sucess',
            type: 'account_onboarding',
            collect: 'eventually_due',
        })
            .then(acctLink => {

                console.log(account.id);
                DriverDetails.update({ bank: account.id }, { where: { UserId: UserId } });
                res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Link for adding bank',
                    'Response': {
                        link: acctLink.url,
                    },
                    'errors': "",
                });
            })
    }


    // if(exist){
    //     Banks.update({status: false}, {where: {UserId: UserId}})
    // }
    // Banks.create({
    //   accTitle, 
    //   iBAN, 
    //   bankName, 
    //   country,
    //   status: true,
    //   UserId 
    // })
    // .then(succ=>{
    //      res.status(200).json({
    //         'ResponseCode': '1',
    //         'ResponseMessage': 'Bank information added',
    //         //'Response': [],
    //         'errors': "",
    //     });
    // })
}))

/*
            24. Bank Details of driver
    _____________________________________________________________
*/
router.get('/getbank/:id', asyncMiddleware(async (req, res) => {
    const exist = await Banks.findOne({ where: { status: 1, UserId: req.params.id } });
    //return res.json(exist)
    if (!exist) {
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'No record Found',
            //'Response': [],
            'errors': "",
        });
    }
    else {
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Bank Data',
            'Response': {
                accTitle: exist.accTitle,
                bankName: exist.bankName,
                iBAN: exist.iBAN,
            },
            'errors': "",
        });
    }
}))

/*
            25. Check if driver is online / offline
    ____________________________________________________________
*/
router.get('/check/status', validateToken, asyncMiddleware(async (req, res) => {
    const userId = req.user.id;
    console.log(userId)
    axios.get(process.env.FIREBASE_URL)
        .then(dat => {
            // return res.json(dat.data)
            console.log(dat.data)
            if (!dat.data.drivers) {
                return res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Logged in Successful',
                    'Response': {
                        status: false
                    },
                    'errors': '',
                })
            }
            let online = Object.hasOwn(dat.data.drivers, `${userId}`);
            if (!online) {
                return res.status(200).json({
                    'ResponseCode': '1',
                    'ResponseMessage': 'Logged in Successful',
                    'Response': {
                        status: false
                    },
                    'errors': '',
                })
            }
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Driver Status',
                'Response': {
                    status: online
                },
                'errors': "",
            });
        })

}));

/*
            26. Get admin email and contact for Driver Application
__________________________________________________________________________
*/
router.get('/info', asyncMiddleware(async (req, res) => {
    const email = await AppSettings.findOne({ where: { title: 'email' } });
    const contact = await AppSettings.findOne({ where: { title: 'contact' } });
    //return res.json(email)
    return res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'App settings',
        'Response': {
            email: email.value,
            contact: contact.value,
        },
        'errors': "",
    });
}))

/*
            27. Get All drivers waiting for Approval
    _______________________________________________________________________
*/
router.get('/waitapproval', asyncMiddleware(async (req, res) => {
    const awaitingDrivers = await Users.findAll({ where: { [Op.or]: [{ status: 0 }, { status: null }], UserCategoryId: 3 } })
    let outArr = [];
    awaitingDrivers.map((d, i) => {
        let tmpObj = {
            id: d.id,
            name: `${d.firstName} ${d.lastName}`,
            phoneNum: `${d.phoneNum}`,
            email: `${d.email}`,
            createdAt: d.createdAt,

        };
        outArr.push(tmpObj);
    })
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'Awaiting Drivers',
        'Response': outArr,
        'errors': "",
    });
}));

/*
            28. Review Driver Details - using User Id
    ___________________________________________________________________________
*/
router.get('/review/:id', asyncMiddleware(async (req, res) => {
    const userId = req.params.id;
    const driverData = await Users.findOne({ where: { id: userId }, include: [{ model: DriverDetails, include: { model: VehiclesType } }, { model: VehicleDetails, where: { status: 1 }, include: { model: VehicleImages } }] });
    const basicInfo = await Users.findOne({ where: { id: userId } });
    const address = await Address.findOne({ where: { UserId: userId } });
    //return res.json(address)
    let docs = [], images = [];
    if (driverData && driverData.VehicleDetails[0]) {
        driverData.VehicleDetails[0].VehicleImages.map((dat, idx) => {
            if (dat.title === 'Vehicle Image') {
                images.push(dat.image);
            }
            if (dat.title === 'Vehicle Document') {
                docs.push(dat.image);
            }
        });
    }
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'App settings',
        'Response': {
            id: basicInfo.id,
            name: `${basicInfo.firstName} ${basicInfo.lastName}`,
            email: basicInfo.email,
            phoneNum: basicInfo.phoneNum,
            profilePicture: basicInfo.profilePicture,
            SSN: address ? address.state : 'No record',
            vehicleType: driverData ? driverData.DriverDetails[0] ? driverData.DriverDetails[0].VehiclesType.name : 'No record' : 'No record',
            licenseData: driverData ? driverData.DriverDetails[0] ? {
                issueDate: driverData.DriverDetails[0].licIssueDate,
                expiryDate: driverData.DriverDetails[0].licExpiryDate,
                issueState: driverData.DriverDetails[0].issuingState,
                Licnumber: driverData.DriverDetails[0].licNum,
                frontPhoto: driverData.DriverDetails[0].licFrontPhoto,
                backPhoto: driverData.DriverDetails[0].licBackPhoto,
            } : {
                issueDate: 'No data',
                expiryDate: 'No data',
                issueState: 'No data',
                Licnumber: 'No data',
                frontPhoto: 'No data',
                backPhoto: 'No data',
            } : {
                issueDate: 'No data',
                expiryDate: 'No data',
                issueState: 'No data',
                Licnumber: 'No data',
                frontPhoto: 'No data',
                backPhoto: 'No data',
            },
            VehicleData: driverData ? driverData.VehicleDetails[0] ? {
                make: driverData.VehicleDetails[0].make,
                model: driverData.VehicleDetails[0].model,
                year: driverData.VehicleDetails[0].year,
                plateNum: driverData.VehicleDetails[0].licPlate,
                color: driverData.VehicleDetails[0].color,
                documents: docs,
                images: images,
            } : {
                make: 'No data',
                model: 'No data',
                year: 'No data',
                plateNum: 'No data',
                color: 'No data',
                documents: [],
                images: [],
            } : {
                make: 'No data',
                model: 'No data',
                year: 'No data',
                plateNum: 'No data',
                color: 'No data',
                documents: [],
                images: [],
            },

        },
        'errors': "",
    });
}))

/*
            29. Approve a Driver
    ____________________________________________________________________________
*/
router.post('/approve', asyncMiddleware(async (req, res) => {
    const { userId } = req.body;
    Users.update({ status: true }, { where: { id: userId } })
        .then(() => {
            return res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Driver Approved',
                'Response': [],
                'errors': "",
            });
        })
}));

/*
            30. Get completed bookings by Driver
    ____________________________________________________________________
*/
router.get('/completed', validateToken, asyncMiddleware(async (req, res) => {
    const driverId = req.user.id;
    const compBookings = await Booking.findAll({ where: { DriverId: driverId, BookingStatusId: 6 }, include: { model: Tips } });
    //return res.json(compBookings);
    let outArr = [];
    compBookings.map((book, index) => {
        let tmpObj = {
            id: `${book.id}`,
            pickupDate: `${book.pickupDate}`,
            estEarning: `${book.estDriverEarning}`,
            tipAmount: book.Tip ? `${book.Tip.amount}` : 'No tip for this order',
        };
        outArr.push(tmpObj);
    })
    return res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Completed Orders',
        'Response': outArr,
        'errors': "",
    });
}));

/*
            30. Driver Rating Details
*/
router.get('/ratingdetails', validateToken, asyncMiddleware(async (req, res) => {
    const driverId = req.user.id;
    const allRatings = await Ratings.findAll({ where: { UserId: driverId } });
    //return res.json(allRatings.length)
    if (allRatings.length === 0) {
        return res.status(200).json({
            'ResponseCode': '2',
            'ResponseMessage': 'No Ratings',
            'Response': {},
            'errors': "",
        });
    }
    let avgRating = 0, numOfFiveStars = 0, numOfFourStars = 0, numOfThreeStars = 0, numOfTwoStars = 0, numOfOneStars = 0;
    allRatings.map((rate, index) => {
        avgRating = avgRating + parseInt(rate.rate);
        if (parseInt(rate.rate) === 5) {
            numOfFiveStars = numOfFiveStars + 1;
        }
        if (parseInt(rate.rate) === 4) {
            numOfFourStars = numOfFourStars + 1;
        }
        if (parseInt(rate.rate) === 3) {
            numOfThreeStars = numOfThreeStars + 1;
        }
        if (parseInt(rate.rate) === 2) {
            numOfTwoStars = numOfTwoStars + 1;
        }
        if (parseInt(rate.rate) === 1) {
            numOfOneStars = numOfOneStars + 1;
        }
    });
    return res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Rating Details',
        'Response': {
            avgRating: `${Math.round(((avgRating / allRatings.length) * 100)) / 100}`,
            numOfFiveStars: `${numOfFiveStars}`,
            numOfFourStars: `${numOfFourStars}`,
            numOfThreeStars: `${numOfThreeStars}`,
            numOfTwoStars: `${numOfTwoStars}`,
            numOfOneStars: `${numOfOneStars}`,
        },
        'errors': "",
    });

}))

/*
            31. Driver Rating Details - Admin
*/
router.post('/rating/admin', asyncMiddleware(async (req, res) => {
    const { driverId } = req.body;
    const allRatings = await Ratings.findAll({ where: { UserId: driverId } });
    //return res.json(allRatings.length)
    if (allRatings.length === 0) {
        return res.status(200).json({
            'ResponseCode': '2',
            'ResponseMessage': 'No Ratings',
            'Response': {},
            'errors': "",
        });
    }
    let avgRating = 0, numOfFiveStars = 0, numOfFourStars = 0, numOfThreeStars = 0, numOfTwoStars = 0, numOfOneStars = 0;
    allRatings.map((rate, index) => {
        avgRating = avgRating + parseInt(rate.rate);
        if (parseInt(rate.rate) === 5) {
            numOfFiveStars = numOfFiveStars + 1;
        }
        if (parseInt(rate.rate) === 4) {
            numOfFourStars = numOfFourStars + 1;
        }
        if (parseInt(rate.rate) === 3) {
            numOfThreeStars = numOfThreeStars + 1;
        }
        if (parseInt(rate.rate) === 2) {
            numOfTwoStars = numOfTwoStars + 1;
        }
        if (parseInt(rate.rate) === 1) {
            numOfOneStars = numOfOneStars + 1;
        }
    });
    let outArr = [['ratings', '# of persons'], ['1', numOfOneStars], ['2', numOfTwoStars], ['3', numOfThreeStars], ['4', numOfFourStars], ['5', numOfFiveStars]];
    return res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Rating Details',
        'Response': {
            avgRating: `${Math.round(((avgRating / allRatings.length) * 100)) / 100}`,
            graphData: outArr,
        },
        'errors': "",
    });

}))


/*
            32. Agreement
    _____________________________________________________
*/

router.get('/agreement', asyncMiddleware(async (req, res) => {
    const agreement = await WebsiteSettings.findOne({ where: { title: 'Driver Agreement' } });
    let str = agreement.value;
    str = str.replace(/(<([^>]+)>)/ig, '');
    str = str.replace(/(\r)/ig, '');
    str = str.replace(/(\n)/ig, '');
    str = str.replace(/(&nbsp;)/ig, '');
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'Driver agreement',
        'Response': str,
        'errors': "",
    })

}));

router.post('/delete', asyncMiddleware(async (req, res) => {
    const { driverId } = req.body;
    Users.update({ deleted: true }, { where: { id: driverId } });
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': 'Driver Deleted successfully',
        'Response': {},
        'errors': "",
    })
}));

// Test API
router.get('/test', (req, res) => {
})

// User defined Functions

async function cancelbooking(reason, note, amount, bookingId, UserId) {
    let cDate = new Date();
    let cYear = cDate.getFullYear();
    let cMonth = cDate.getMonth() + 1;
    let cdt = cDate.getDate()
    let cHours = cDate.getHours();
    let cMinutes = cDate.getMinutes();

    let updateStatus = await axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Cancelled", "bookingId": bookingId });
    let createHistory = await CancelHistory.create({
        reason,
        note,
        amount,
        cancelledAt: `${cMonth}/${cdt}/${cYear} ${cHours}:${cMinutes} `,
        BookingId: bookingId,
        UserId,
    });
    if (createHistory && updateStatus) return true;
    return false;
}

async function notifyAllActiveDrivers(driverId, bookingId) {
    const booking = await Booking.findOne({ where: { id: bookingId } });
    // only get those driver whose Vehicle type matches with Vehicle type of Booking
    const drivers = await Users.findAll({ where: { status: 1, UserCategoryId: 3 }, include: [{ model: DriverDetails, where: { VehiclesTypeId: booking.VehiclesTypeId } }] });
    // removing the driver who has cancelled the booking
    let dArr = drivers.filter(d => d.id !== driverId);
    //return dArr;
    axios.get(process.env.FIREBASE_URL)
        .then(dat => {
            Object.keys(dat.data.drivers).map(function (key, index) {
                let found = dArr.find(ele => parseInt(key) === ele.id)
                if (!found) console.log('not found against', key)
                if (found) {
                    let message = {
                        to: `${found.deviceToken}`,
                        notification: {
                            title: 'New Job',
                            body: 'A new job has arrived'
                        },
                        data: {
                            bookingId: bookingId,
                            booking: true,
                        },
                    };
                    fcm.send(message, function (err, response) {
                        if (err) {
                            console.log("Something has gone wrong!");
                            //res.json(err);
                        } else {
                            console.log("Successfully sent with response: ", response);
                            //res.json(response);
                        }
                    });
                    console.log(found.id, key);
                }
            });
        })
        .catch(err => {
            return res.json(err);
        })
}



module.exports = router;