const express = require('express');
const router = express();
const { Address } = require('../models');
const { validateToken } = require('../middlewares/AuthorizationMW');
const { Op } = require("sequelize");
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');

/*
                1. Add Address for Customer
    ______________________________________________________________
 a. Check the exact location of address 
    if it already exist , update the previous data 
    otherwise create a new address
    waqar
*/
router.post('/add', asyncMiddleware(async (req, res) => {
    let {title, city, building, aptNum, state, zip, countryCode , phoneNum,lng, lat, UserId, exactAddress  } = req.body;
    phoneNum = countryCode.concat(phoneNum);
    // to remove spaces let text2 =  exactAddress.replace(/ /g, "");
    //return res.json(text2);
    const exist = await Address.findOne({where: {exactAddress: exactAddress, UserId: UserId}});
    let address = {
        title, city, building, aptNum, state, zip, phoneNum,lng, lat, UserId, exactAddress, status: true,
    };
    if(exist){
        Address.update(address, {where: {id: exist.id}})
        .then((dat) => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Address Updated',
                'Response': [{ addressId: `${exist.id}` }],
                'errors': '',
            })
        })
    }
    else{
        Address.create(address)
        .then((dat) => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Address Saved',
                'Response': [{ addressId: `${dat.id}` }],
                'errors': '',
            })
        })
    }
    
}));

/*
               2. Get all Address by User ID
   ____________________________________________________________________
  a. Uses the validation to get the user Id
  b. Get all the addresses where User Id and status is 1

*/
router.get('/getbyuserid', validateToken ,asyncMiddleware(async (req, res) => {
    const userId = req.user.id;
   
    let addresses = await Address.findAll({where: { [Op.and]: [{UserId: userId} , {status: 1}] }})
    //return res.json(addresses);
    //let addresses = await Address.findAll({where: { UserId: userId}});
    if(!addresses){
        return res.status(200).send({
            'ResponseCode': '0',
            'ResponseMessage': 'No address found',
            'Response': [],
            'errors': 'No Address Added',
        });
    }
    let outArr = [];
    addresses.map((add,idx)=>{
       let tmpObj={
           id: add.id,
           title: add.title? add.title: '',
           building: add.building? add.building: '',
           aptNum: add.aptNum? add.aptNum: '',
           city: add.city? add.city: '',
           state: add.state? add.state:'',
           zip: add.zip? add.zip: '' ,
           phoneNum: add.phoneNum? add.phoneNum: "",
           lat: add.lat? add.lat: '',
           lng: add.lng? add.lng:'',
        //    exactAddress: add.exactAddress? add.exactAddress:'',
           exactAddress: `${add.title} ${add.building} ${add.zip} ${add.city} ${add.state}`,
           UserId: add.UserId,
       };
       outArr[idx] = tmpObj; 
       
    })
    res.status(200).send({
        'ResponseCode': '1',
        'ResponseMessage': 'Saved Addresses',
        'Response': outArr.reverse(),
        'errors': '',
    });
}));

/*
                 3. Delete Address by AddressID
        __________________________________________________________
  a. Deleteing a specific address by using ID of the address table
  b. It does not actually delete the address but change the status to 0.
*/
router.post('/delete', asyncMiddleware(async (req, res) => {
    let {addressId} = req.body;
    const found = await Address.findOne({where: {id: addressId}})
    if(!found) return res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Address not found',
        'Response': [],
        'errors': 'Address not found',
    }); 
    Address.update({status: false}, {where: {id: addressId } })
        .then(() => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Address Deleted',
                'Response': [],
                'errors': '',
            });
        })
        .catch((err)=>{
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Address not found',
                'Response': [],
                'errors': '',
            });
        })
}));

/*
                        4. Add Address Driver
_________________________________________________________________________
  a. Create address for driver
*/
router.post('/add/driver', asyncMiddleware(async (req, res) => {
    let {city, building, state, zip, UserId, exactAddress  } = req.body;
    let address = {
        city, building, state, zip, UserId, exactAddress, status: true,
    };
    Address.create(address)
        .then((dat) => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Address Saved',
                //'Response': [{ addressId: `${dat.id}` }],
                'errors': '',
            })
        })
}));

module.exports = router;