const express = require('express');
const router = express();
const { Ratings, Tips, Users, Booking } = require('../models');
const { validateToken } = require('../middlewares/AuthorizationMW');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
const stripe = require('stripe')(process.env.STRIPE_KEY);


let FCM = require('fcm-node');
let fcm = new FCM(process.env.FIREBASE_SERVER_KEY);

/*
            1. Add Rating and Tip
    _______________________________________________________________
    a. Find if the rating already exists or not
    b. If not, create a new enrty in the table
    c. If yes, update the entry with current rating
    d. Check if the amount is greater than zero or not 
    f. If not, Return --> Error 
    g. Create Payment intent, if fails, Return --> Error = Invalid Payment Method
    h. PI confirm, if fails, Return --> Error = Stripe Error
    i. If True, Create a new entry in the Tips table
    j. If writing fails, Return --> Error
    k. Return the success msg 
*/
router.post('/add', validateToken, asyncMiddleware(async (req, res) => {
    const { rate, amount, bookingId } = req.body;
    let amtinCents = parseFloat(amount) * 100;
    // Amount to be charged with added stripe fee
    chargedAmount = amtinCents/0.97;
    chargedAmount = Math.round(chargedAmount);
    //return res.json(`${chargedAmount}`);
    const booking = await Booking.findOne({ where: { id: bookingId } });
    const user = await Users.findOne({ where: { id: req.user.id } });
    const driver = await Users.findOne({ where: { id: booking.DriverId } });
    const ratingExist = await Ratings.findOne({ where: { BookingId: bookingId } })
    if (!ratingExist) {
        Ratings.create({
            rate,
            BookingId: bookingId,
            UserId: booking.DriverId,
        });
    }
    else {
        Ratings.update({
            rate
        }, { where: { BookingId: bookingId } });
    }
    if (amount > 0) {
        stripe.paymentIntents.create({
            amount:`${chargedAmount}` , // send in cents
            currency: 'usd',
            payment_method_types: ['card'],
            customer: `${user.stripeCustomerId}`,
            payment_method: `${booking.pmId}`,
        })
        .then((piC) => {
            stripe.paymentIntents.confirm(`${piC.id}`)
                .then((result) => {
                    // Add Code to charge payment
                    Tips.create({
                        amount,
                        BookingId: bookingId,
                        UserId: booking.DriverId ,
                    })
                    .then(() => {
                        let message = {
                            to: `${driver.deviceToken}`,
                            notification: {
                                title: 'Tip paid',
                                body: `You are paid $${amount} as tip for booking ID: ANT${bookingId}`,
                            },
                        };
                        fcm.send(message, function(err, response){
                            if(err) console.log(err)
                            else console.log(response)
                        });
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Rating & Tip Added',
                            'Response': [{ receivedAmount: result.amount_received }],
                            'errors': ``,
                        })
                    })
                    .catch(err => {
                        res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Tip Payment Failed',
                            'Response': [],
                            'errors': 'Error writing to DB',
                        })
                    })
                })
                .catch(err => {
                    res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Tip Payment Failed',
                        'Response': [],
                        'errors': err.raw.code,
                    })
                })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Tip Payment Failed',
                'Response': [],
                'errors': 'Invalid Payment Method',
            })
        })
    }
    else{
        return res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Rating Added',
            'Response': [],
            'errors': ``,
        });
    }
}));

module.exports = router;