const express = require('express');
const router = express();
const {Coupons, Booking} = require('../models');
const { Op } = require("sequelize");
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');


/*
            1. Add coupon 
    ________________________________________________________
    a. Check if coupon already exist or not
    b. If exist Return res --> Already Exist with the same name
    c. Create coupon in the table
    d. Return the success response
*/
router.post('/add',  asyncMiddleware(async (req, res) => {
    const {name, discount,from,to, status, type, conditionAmount } = req.body;
    const exist = await Coupons.findOne({where: {name: name}})
    // if(exist) throw new CustomException('Already exist', 200);
    if(exist) {
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Coupon exist',
            'Response': [],
            'errors': '',
        });
        res.redirect();
    }
    Coupons.create({
        name,
        discount,
        from,
        to,
        status, 
        type,
        conditionAmount,
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Coupon Added',
            'Response': [dat],
            'errors': '',
        })
    })
}));

/*
            2. Apply coupon 
    ________________________________________________________
    a. Check if coupon already exist or not
    b. If exist Return res --> Already Exist with the same name
    c. Create coupon in the table
    d. Return the success response
*/
router.post('/apply',asyncMiddleware(async (req, res) => {
    const {name, BookingId} = req.body;
    const exist = await Coupons.findOne({where: {name: name}});
    if(!exist) throw new CustomException('Invalid Coupon code', 200);
    const booking = await Booking.findOne({where: {id: BookingId}})
    // Case 1 : Applying different/ same coupon to same booking
    const alreadyApplied = await Booking.findOne({where: {[Op.and]: [{id: booking.id} , {CouponId: {[Op.not]: null} }]} })
    if(alreadyApplied) throw new CustomException('A coupon has already applied to this booking', 200);
    //return res.json(alreadyApplied)
    // Case 2: Applying same coupon to some other booking ( checking a user apply a coupon only once)
    const alreadyAppliedByUser = await Booking.findAll({where: {[Op.and]: [{UserId: booking.UserId}, {paymentConfirmed: 1} , {CouponId: exist.id }]} })
    if(alreadyAppliedByUser.length>=1) throw new CustomException('You have already availed this coupon', 200);
    //return res.json(alreadyAppliedByUser)
    if(exist.status && Date.parse(exist.from) < Date.now() && Date.parse(exist.to) > Date.now()){
        if(exist.type === 'Flat'){
            let newAmount = parseFloat(booking.total) - parseFloat(exist.discount);
            if(newAmount<0) newAmount = 0.0;
            else if(newAmount>0 && newAmount<1) newAmount=1.0;
            newAmount = Math.round((newAmount * 100)) / 100;
            //console.log(newAmount);
           Booking.update({CouponId: exist.id ,grandTotal: newAmount} ,{ where: { id: BookingId }})
           .then(()=>{
               res.status(200).json({
                 'ResponseCode': '1',
                 'ResponseMessage': 'Coupon Applied',
                 'Response': [{
                     CouponId: exist.id,
                     total: `${newAmount}`,
                     discount: `${exist.discount}`,
                 }],
                 'errors': '',  
               })
           })
           .catch(err=>{
               res.status(200).json({
                 'ResponseCode': '0',
                 'ResponseMessage': 'Error',
                 'Response': [],
                 'errors': err.code,  
               })
           })
        }
        else if(exist.type === 'Amount_dependent') {
            if(parseFloat(booking.total)> parseFloat(exist.conditionAmount)){
               let newAmount = parseFloat(booking.total) - parseFloat(exist.discount);
               if(newAmount<0) newAmount = 0.0;
               else if(newAmount>0 && newAmount<1) newAmount=1.0;
                Booking.update({CouponId: exist.id, grandTotal: newAmount} ,{ where: { id: BookingId }})
                    .then(()=>{
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Coupon Applied',
                             'Response': [{
                                 CouponId: exist.id,
                                 total: `${newAmount}`,
                                 discount: `{exist.discount}` ,
                            }],
                            'errors': '',  
                        })
                    })
                    .catch(err=>{
                        res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Error',
                            'Response': [],
                            'errors': err.code,  
                         })
                    }) 
            }else{
                res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Coupon not Applicable',
                    'Response': [],
                    'errors': 'Coupon not Applicable',  
                         })
               }
            }
        }
    else{
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Expired Coupon',
            'Response': [],
            'errors': "Coupon Expired",  
               })
    }
}));

// 3. Change Coupon Status
/*
            3. Change Coupon Status
    _________________________________________________________
    a. Check if Coupon is valid or not, if not Return --> Error
    b. Update the status and Return --> Success Message
    c. if fail to update Return --> Error
*/
router.put('/updatestatus', asyncMiddleware(async (req, res) => {
    const { name, status } = req.body;
    const exist = await Coupons.findOne({ where: { name: name } })
    if (!exist) throw new CustomException('Invalid Coupon code', 200)
    Coupons.update({ status: status }, { where: { name: name } })
        .then((dat) => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Coupon Status Updated',
                'Response': [{ CouponId: dat }],
                'errors': '',
            })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Error',
                'Response': [],
                'errors': err,
            })
        })

}));

/*
            4. Get all coupons
    ____________________________________________________________
*/
router.get('/getall',  asyncMiddleware(async (req, res) => {
    const allCoupons = await Coupons.findAll()
    let tmpArr = [];
    allCoupons.map((coup, index)=>{
        let tmpObj = {
            id : coup.id,
            name: coup.name,
            status: coup.status,
            discount: coup.discount,
            from: coup.from,
            to: coup.to,
        }
        tmpArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'All Coupons',
        'Response': tmpArr,
        'errors': '',
    })
}));

/*
            5. Get Coupon details by using Coupon ID
    _____________________________________________________________
*/
router.get('/getby/:id',  asyncMiddleware(async (req, res) => {
    const coupon = await Coupons.findOne({where:{id:req.params.id}})
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Coupon Details',
        'Response': coupon,
        'errors': '',
    })
}));

/*
            6. Update Coupon 
    _____________________________________________
*/
router.post('/update_coupon', asyncMiddleware(async (req, res) => {
    const { name, status, id, discount, from, to } = req.body;
    const exist = await Coupons.findOne({where: {id: id}});
    exist.update({
        discount: discount,
        name: name,
        from: from,
        to: to,
        status: status
    }, { where: { id: id } })
        .then(result => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Coupon Updated',
                'Response': '',
                'errors': '',
            })
        })
}));

// 3. UPDATE COUPON TUFAIL API
// router.post('/update_coupon',asyncMiddleware(async (req, res) => {
    
//     const {name, status,id,discount,from,to} = req.body;
//     return res.json(req.body);
//     const coupon = await Coupons.findOne({where: {id: id}});
    
    // res.send(exist);
   
    
    // exist.update({
    //     discount:discount,
    //     name:name,
    //     from:from,
    //     to:to,
    //     status:status
       
    // });
    // res.status(200).json({
    //     'ResponseCode': '1',
    //     'ResponseMessage': 'Coupon Updated',
    //     'Response': exist,
    //     'errors': '',
    // })
     
// }));


module.exports = router;