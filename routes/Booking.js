require('dotenv').config()
const express = require('express');
const router = express();
const { Users, Booking, Address, Package, Category, BookingStatus, BookingHistory, Charge, VehiclesType, VehicleDetails, CancelHistory, DriverDetails, Coupons, Ratings, Tips } = require('../models');
const { validateToken } = require('../middlewares/AuthorizationMW');
const { Op } = require("sequelize");
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// OTP generator
const otpGenerator = require('otp-generator')
const axios = require('axios');

/*
            1. Add a booking
    ____________________________________________________________________________
    a. Gathering the required data from different tables
    b. Get the distance between pickup and drop Off using Google Maps API
    c. Create a security key for the booking
    d. Calculating price based on the given criteria 
    e. Create a new booking in the table with all the calculated data
    f. Create a time stamp that the booking has been created
    g. Add packages to DB by calling a API 
    h. Return the success response  
*/
router.post('/add', asyncMiddleware(async (req, res) => {
    const { package, rName, rEmail, rAlPhoneNum, pickupDate, note, UserId, VehiclesTypeId, pickupId, deliveryId, load_unload } = req.body;
    // Gathering data - Pickup and delivery Address
    const pickupAddress = await Address.findOne({ where: { id: pickupId } });
    const deliveryAddress = await Address.findOne({ where: { id: deliveryId } });

    // Gathering Data - Charges data
    const vehicle = await VehiclesType.findOne({ where: { id: VehiclesTypeId } });
    console.log(vehicle)
    //return res.json(vehicle)
    const baseWeightRate = await Charge.findOne({ where: { title: 'baseWeightRate' } });
    const perLbRate = await Charge.findOne({ where: { title: 'perLbRate' } });
    const baseVolumeRate = await Charge.findOne({ where: { title: 'baseVolumeRate' } });
    const perCM3Rate = await Charge.findOne({ where: { title: 'perCM3Rate' } });
    const load_unload_rate = await Charge.findOne({ where: { title: 'load_unload' } });
    const Insurance = await Charge.findOne({ where: { title: 'Insurance' } });
    const baseDistance = await Charge.findOne({ where: { title: 'baseDistance' } });
    const adminCommission = await Charge.findOne({ where: { title: 'Admin Commission' } });
    let adminCommissionPercent = 1 - (parseFloat(adminCommission.amount) / 100);
    // Defining Variable to store the cahrges
    let weight_Amount = 0, volume_Amount = 0, insurance_Amount = 0, load_Amount = 0, distance_Amount = 0, total_weight = 0;
    let packageTotal = 0;
    let packTotalArr = [];
    let estDriverEarning = 0;
    // Getting lat & lng for pickup and delivery
    const plng = pickupAddress.lng;
    const plat = pickupAddress.lat;
    const dlng = deliveryAddress.lng;
    const dlat = deliveryAddress.lat;
    //return res.json(dlat)
    console.log(plng, plat, dlng, dlat)
    axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${plat},${plng}&destination=${dlat},${dlng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
        .then((resp) => {
            //return res.json(resp.data);
            console.log(resp.data)
            // Getting the distance and converting to miles
            if (resp.data.routes.length == 0) {
                return res.json({
                    "ResponseCode": "0",
                    "ResponseMessage": "Use a different address",
                    "Response": [],
                    "errors": "We cannot find a way there"
                })
            }
            let totalDistance = resp.data.routes[0].legs[0].distance.value * 0.000621371;
            let securityKey = otpGenerator.generate(5, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });

            // -------------------
            // Price Calculation
            // -------------------
            //return res.json(securityKey)
            package.map((pkg, index) => {
                let BVR_value, BVR_amount, perUnit_value, perUnit_amount;
                let L, W, H;
                // total weight of Booking for load and Unload
                total_weight = total_weight + parseFloat(pkg.weight);
                // Weight Calculation
                if (parseFloat(pkg.weight) <= parseFloat(baseWeightRate.value)) {
                    weight_Amount = weight_Amount + parseFloat(baseWeightRate.amount)
                    //console.log('weight less', weight_Amount);
                }
                else {
                    let extraWeight = parseFloat(pkg.weight) - parseFloat(baseWeightRate.value);
                    //console.log('extra', extraWeight)
                    weight_Amount = weight_Amount + parseFloat(baseWeightRate.amount) + (extraWeight * parseFloat(perLbRate.amount))
                }
                console.log(total_weight)
                // Volume calculation
                if (pkg.unit === 'cm') {
                    L = parseFloat(pkg.length);
                    W = parseFloat(pkg.width);
                    H = parseFloat(pkg.height);
                }
                else if (pkg.unit === 'in') {
                    L = parseFloat(pkg.length) * 2.54;
                    W = parseFloat(pkg.width) * 2.54;
                    H = parseFloat(pkg.height) * 2.54;
                }
                let vol = L * W * H;
                BVR_value = parseFloat(baseVolumeRate.value);
                BVR_amount = parseFloat(baseVolumeRate.amount);
                perUnit_value = parseFloat(perCM3Rate.value);
                perUnit_amount = parseFloat(perCM3Rate.amount);

                if (vol <= BVR_value) {
                    volume_Amount = volume_Amount + BVR_amount;
                    //volume_Amount = Math.ceil(volume_Amount);
                    //console.log('volume less', weight_Amount);
                }
                else {
                    let extraVolume = parseFloat(vol) - BVR_value;
                    let extraUnits = extraVolume / perUnit_value;
                    //console.log(Math.ceil(extraUnits));
                    volume_Amount = volume_Amount + BVR_amount + (Math.ceil(extraUnits) * perUnit_amount);
                    //volume_Amount = Math.ceil(volume_Amount);
                }
                console.log(volume_Amount)
                // // Insurance Calculation
                if (pkg.insurance) {
                    let amt = parseFloat(pkg.estWorth) * (parseFloat(Insurance.amount) / 100);
                    insurance_Amount = insurance_Amount + amt;
                }
                console.log('INSURANCE', insurance_Amount)
                // // Distance calculation
                if (totalDistance <= parseFloat(baseDistance.value)) {
                    distance_Amount = distance_Amount + parseFloat(vehicle.baseRateVec)
                    //console.log('volume less', weight_Amount);
                }
                else {
                    let extraDistance = parseFloat(totalDistance) - parseFloat(baseDistance.value);
                    //let extraUnits = extraVolume/parseFloat(charges[5].value);
                    //console.log(Math.ceil(extraUnits));
                    distance_Amount = distance_Amount + parseFloat(vehicle.baseRateVec) + (extraDistance * parseFloat(vehicle.baseRateMile))
                }
                console.log(' DISTANCE', distance_Amount)
                packageTotal = weight_Amount + volume_Amount + insurance_Amount + distance_Amount;
                //console.log('total', packageTotal, 'W', weight_Amount, 'V',  volume_Amount, 'I', insurance_Amount, 'D', distance_Amount )
                estDriverEarning = estDriverEarning + ((weight_Amount + volume_Amount + distance_Amount) * adminCommissionPercent);
                packageTotal = Math.round((packageTotal * 100)) / 100;
                packTotalArr[index] = packageTotal;
                weight_Amount = 0; volume_Amount = 0; insurance_Amount = 0; distance_Amount = 0;
            })
            //return res.json(packageTotal)

            if (load_unload) {
                if (total_weight <= parseFloat(load_unload_rate.value)) {
                    load_Amount = load_Amount + parseFloat(load_unload_rate.amount)
                    //console.log('volume less', weight_Amount);
                }
                else {
                    let extraload = total_weight - parseFloat(load_unload_rate.value);
                    let extraUnits = extraload / parseFloat(load_unload_rate.value);
                    //console.log(Math.ceil(extraUnits));
                    load_Amount = load_Amount + parseFloat(load_unload_rate.amount) + (extraUnits * parseFloat(load_unload_rate.amount))
                }

            }
            //console.log(load_Amount)
            var sum = packTotalArr.reduce(function (a, b) { return a + b; }, 0);
            let total = sum + load_Amount;
            // For limiting the values upto 2 
            total = Math.round((total * 100)) / 100;
            //return res.json(total)
            estDriverEarning = estDriverEarning + (load_Amount * adminCommissionPercent);
            estDriverEarning = Math.round((estDriverEarning * 100)) / 100;
            //console.log(estDriverEarning);
            //res.json(packTotalArr);
            Booking.create({
                rName: rName,
                rEmail: rEmail,
                rPhoneNum: pickupAddress.phoneNum,
                rAlPhoneNum: rAlPhoneNum,
                pickupDate,
                note,
                pickupId,
                deliveryId,
                UserId,
                BookingStatusId: 1,
                condition: true,
                totalDistance: Math.round((totalDistance * 100)) / 100,
                VehiclesTypeId,
                securityKey,
                load_unload,
                total,
                grandTotal: total,
                estDriverEarning,
            })
                .then((dat) => {
                    let cDate = new Date();
                    let date = cDate.getDate();
                    let month = cDate.getMonth() + 1;
                    let year = cDate.getFullYear();
                    let hours = cDate.getHours();
                    let minutes = cDate.getMinutes();
                    BookingHistory.create({
                        time: `${month}/${date}/${year} ${hours}:${minutes}`,
                        BookingId: dat.id,
                        BookingStatusId: 1,
                    });
                    package.map((p, index) => {
                        p.BookingId = dat.id,
                            p.total = packTotalArr[index]
                    });
                    axios.post(`${process.env.BASE_URL}/package/add`, { package: package })
                        .then(out => {
                            //console.log(out)
                            // For limiting the values upto 2 
                            // let dist = Math.round((dat.totalDistance * 100)) / 100;
                            // let tot = Math.round((dat.total * 100)) / 100;
                            // console.log(dist)
                            res.status(200).json({
                                'ResponseCode': '1',
                                'ResponseMessage': 'Booking Added',
                                'Response': [{
                                    BookingId: `${dat.id}`,
                                    packageIds: out.data.Response,
                                    total: `${dat.total}`,
                                    distance: `${dat.totalDistance}`,
                                    loadAmount: `${Math.round((load_Amount * 100)) / 100}`,
                                }],
                                'errors': '',
                            })
                        })
                })
                .catch(err => {
                    return res.json({
                        "ResponseCode": "0",
                        "ResponseMessage": "Error",
                        "Response": [],
                        "errors": `${err}`
                    })
                })
        })
        .catch(err => {
            return res.json({
                "ResponseCode": "0",
                "ResponseMessage": "Error",
                "Response": [],
                "errors": `${err}`
            })
        })
}));
/*
            2. Update a booking
    ____________________________________________________________________________
    a. Gathering the required data from different tables
    b. Get the distance between pickup and drop Off using Google Maps API
    c. Calculating price based on the given criteria 
    d. Update the booking in the table with all the calculated data
    e. Create a time stamp that the booking has been created
    f. Update packages to DB by calling a API 
    g. Return the success response  
*/
router.put('/update', asyncMiddleware(async (req, res) => {
    const { bookId } = req.body;
    const { package, rName, rEmail, rAlPhoneNum, pickupDate, note, UserId, VehiclesTypeId, pickupId, deliveryId, load_unload } = req.body;
    const pickupAddress = await Address.findOne({ where: { id: pickupId } });
    const deliveryAddress = await Address.findOne({ where: { id: deliveryId } });

    // Charges Calculation
    const vehicle = await VehiclesType.findOne({ where: { id: VehiclesTypeId } });
    const baseWeightRate = await Charge.findOne({ where: { title: 'baseWeightRate' } });
    const perLbRate = await Charge.findOne({ where: { title: 'perLbRate' } });
    const baseVolumeRate = await Charge.findOne({ where: { title: 'baseVolumeRate' } });
    const perCM3Rate = await Charge.findOne({ where: { title: 'perCM3Rate' } });
    const load_unload_rate = await Charge.findOne({ where: { title: 'load_unload' } });
    const Insurance = await Charge.findOne({ where: { title: 'Insurance' } });
    const baseDistance = await Charge.findOne({ where: { title: 'baseDistance' } });
    const adminCommission = await Charge.findOne({ where: { title: 'Admin Commission' } });
    let adminCommissionPercent = 1 - (parseFloat(adminCommission.amount) / 100);
    //res.send(baseWeightRate)
    let weight_Amount = 0, volume_Amount = 0, insurance_Amount = 0, load_Amount = 0, distance_Amount = 0, total_weight = 0;
    let packageTotal = 0;
    let packTotalArr = [];
    let estDriverEarning = 0;
    // Getting Addresses
    const plng = pickupAddress.lng;
    const plat = pickupAddress.lat;
    const dlng = deliveryAddress.lng;
    const dlat = deliveryAddress.lat;
    axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${plat},${plng}&destination=${dlat},${dlng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
        .then((resp) => {
            let totalDistance = resp.data.routes[0].legs[0].distance.value * 0.000621371;
            // ------------
            // Price Calculation
            // ----------------
            package.map((pkg, index) => {
                // total weight of Booking for load and Unload
                total_weight = total_weight + parseFloat(pkg.weight);
                // Weight Calculation
                if (parseFloat(pkg.weight) <= parseFloat(baseWeightRate.value)) {
                    weight_Amount = weight_Amount + parseFloat(baseWeightRate.amount)
                    //console.log('weight less', weight_Amount);
                }
                else {
                    let extraWeight = parseFloat(pkg.weight) - parseFloat(baseWeightRate.value);
                    //console.log('extra', extraWeight)
                    weight_Amount = weight_Amount + parseFloat(baseWeightRate.amount) + (extraWeight * parseFloat(perLbRate.amount))
                }
                // Volume calculation
                // Volume calculation
                let vol = parseFloat(pkg.length) * parseFloat(pkg.width) * parseFloat(pkg.height);
                // Unit determination 
                if (pkg.unit === 'cm') {
                    BVR_value = parseFloat(baseVolumeRate.value);
                    BVR_amount = parseFloat(baseVolumeRate.amount);
                    perUnit_value = parseFloat(perCM3Rate.value);
                    perUnit_amount = parseFloat(perCM3Rate.amount);
                }
                else if (pkg.unit === 'in') {
                    let inchtocm = 0.3937;
                    BVR_value = parseFloat(baseVolumeRate.value) * inchtocm;
                    BVR_amount = parseFloat(baseVolumeRate.amount) * inchtocm;
                    perUnit_value = parseFloat(perCM3Rate.value) * inchtocm;
                    perUnit_amount = parseFloat(perCM3Rate.amount) * inchtocm;
                }

                if (vol <= BVR_value) {
                    volume_Amount = volume_Amount + BVR_amount;
                    volume_Amount = Math.ceil(volume_Amount);
                    //console.log('volume less', weight_Amount);
                }
                else {
                    let extraVolume = parseFloat(vol) - BVR_value;
                    let extraUnits = extraVolume / perUnit_value;
                    //console.log(Math.ceil(extraUnits));
                    volume_Amount = volume_Amount + BVR_amount + (Math.ceil(extraUnits) * perUnit_amount);
                    volume_Amount = Math.ceil(volume_Amount);
                }
                // // Insurance Calculation
                if (pkg.insurance) {
                    let amt = parseFloat(pkg.estWorth) * (parseFloat(Insurance.amount) / 100);
                    insurance_Amount = insurance_Amount + amt;
                }
                // // Distance calculation
                if (totalDistance <= parseFloat(baseDistance.value)) {
                    distance_Amount = distance_Amount + parseFloat(vehicle.baseRateVec)
                    //console.log('volume less', weight_Amount);
                }
                else {
                    let extraDistance = parseFloat(totalDistance) - parseFloat(baseDistance.value);
                    //let extraUnits = extraVolume/parseFloat(charges[5].value);
                    //console.log(Math.ceil(extraUnits));
                    distance_Amount = distance_Amount + parseFloat(vehicle.baseRateVec) + (extraDistance * parseFloat(vehicle.baseRateMile))
                }
                packageTotal = weight_Amount + volume_Amount + insurance_Amount + distance_Amount;
                estDriverEarning = estDriverEarning + ((weight_Amount + volume_Amount + distance_Amount) * adminCommissionPercent);
                packageTotal = Math.round((packageTotal * 100)) / 100;
                packTotalArr[index] = packageTotal;
                weight_Amount = 0; volume_Amount = 0; insurance_Amount = 0; distance_Amount = 0;
            })

            if (load_unload) {
                if (total_weight <= parseFloat(load_unload_rate.value)) {
                    load_Amount = load_Amount + parseFloat(load_unload_rate.amount)
                    //console.log('volume less', weight_Amount);
                }
                else {
                    let extraload = total_weight - parseFloat(load_unload_rate.value);
                    let extraUnits = extraload / parseFloat(load_unload_rate.value);
                    //console.log(Math.ceil(extraUnits));
                    load_Amount = load_Amount + parseFloat(load_unload_rate.amount) + (extraUnits * parseFloat(load_unload_rate.amount))
                }
            }
            var sum = packTotalArr.reduce(function (a, b) { return a + b; }, 0);
            let total = sum + load_Amount;
            total = Math.round((total * 100)) / 100;
            estDriverEarning = estDriverEarning + (load_Amount * adminCommissionPercent);
            estDriverEarning = Math.round((estDriverEarning * 100)) / 100;
            // update booking
            Booking.update({
                rName: rName,
                rEmail: rEmail,
                rPhoneNum: pickupAddress.phoneNum,
                rAlPhoneNum: rAlPhoneNum,
                pickupDate,
                note,
                pickupId,
                deliveryId,
                UserId,
                totalDistance: Math.round((totalDistance * 100)) / 100,
                VehiclesTypeId,
                load_unload,
                total: total,
                grandTotal: total,
                CouponId: null,
                estDriverEarning,
            }, { where: { id: bookId } })
                .then((dat) => {
                    package.map((p, idx) => {
                        p.BookingId = bookId,
                            p.total = packTotalArr[idx]
                    });
                    axios.put(`${process.env.BASE_URL}/package/update/${bookId}`, { package: package })
                        .then(out => {
                            //console.log(out)
                            Booking.findOne({ where: { id: bookId } })
                                .then((order) => {
                                    res.status(200).json({
                                        'ResponseCode': '1',
                                        'ResponseMessage': 'Booking Updated',
                                        'Response': [{
                                            BookingId: `${bookId}`,
                                            total: `${order.grandTotal}`,
                                            packageIds: out.data.Response,
                                            distance: `${order.totalDistance}`,
                                            loadAmount: `${Math.round((load_Amount * 100)) / 100}`,
                                        }],
                                        'errors': '',
                                    })
                                })

                        })
                })
        });
}));

/*
            3. Get All active bookings whose payment is confirmed
    ____________________________________________________________________
    1. Get all bookings from table with the defined criteria
    2. Filter the desired data as required
    3. Return the response 
*/
router.get('/get/all', asyncMiddleware(async (req, res) => {
    const booking = await Booking.findAll({ where: { [Op.and]: [{ paymentConfirmed: true }] }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: BookingStatus }] });
    let outArr = [];
    booking.map((book, indx) => {
        const tmpObj = {
            orderNum: `${book.id}`,
            senderName: `${book.User.firstName} ${book.User.lastName}`,
            senderPhoneNum: `${book.pickupAddress.phoneNum}`,
            recieverName: `${book.rName}`,
            recieverPhoneNum: `${book.deliveryAddress.PhoneNum}`,
            pickupAddress: `${book.pickupAddress.building}, ${book.pickupAddress.exactAddress}, ${book.pickupAddress.city}, ${book.pickupAddress.state}, ${book.pickupAddress.zip}`,
            dropoffAddress: `${book.deliveryAddress.building}, ${book.deliveryAddress.exactAddress}, ${book.deliveryAddress.city}, ${book.deliveryAddress.state}, ${book.deliveryAddress.zip}`,
            pickupTime: book.pickupDate,
            amount: `${book.grandTotal}`,
            bookingStatus: book.BookingStatus === null ? 'No status' : book.BookingStatus.name,
        }
        outArr[indx] = tmpObj;
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `All Bookings`,
        'Response': outArr,
        'errors': '',
    });
}));

/*
            4. Get booking details by Booking ID
    _________________________________________________________________
    a. Get the desired booking using ID
    b. Filter out the packages Data and save to an array
    c. Make an object of the desired data
    d. Return the success response
*/
router.post('/getby', asyncMiddleware(async (req, res) => {
    let { bookId } = req.body;
    const booking = await Booking.findOne({ where: { id: bookId }, include: [{ model: Users, as: 'User' }, { model: Users, as: 'Driver' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: Package, include: [{ model: Category }] }, { model: BookingStatus }, { model: Coupons }, { model: Ratings }, { model: Tips }, { model: BookingHistory, include: { model: BookingStatus } }] });
    //return res.json(booking)
    const load_unload_rate = await Charge.findOne({ where: { title: 'load_unload' } });
    //return res.json(booking)
    let pckgArr = [];
    let totalWeight = 0;
    booking.Packages.map((pckg, idx) => {
        let d = `${pckg.length} * ${pckg.width} * ${pckg.height}`;
        totalWeight = totalWeight + parseFloat(pckg.weight);
        let tmpObj = {
            dimensions: `${pckg.length} * ${pckg.width} * ${pckg.height}`,
            unit: `${pckg.unit}`,
            weight: `${pckg.weight}`,
            worth: `${pckg.estWorth}`,
            category: `${pckg.Category.name}`,
            total: `${pckg.total}`,
            insurance: `${pckg.insurance}`,
        }
        pckgArr[idx] = tmpObj;
    });
    let histArr = [];
    booking.BookingHistories.map((hist, index) => {
        let tmpObj = {
            status: hist.BookingStatus.name,
            time: hist.time,
        };
        histArr.push(tmpObj);
    })

    // to limit it upto 2 digits
    let dist = Math.round((booking.totalDistance * 100)) / 100;
    let earn = Math.round((booking.estDriverEarning * 100)) / 100;
    let outObj = {
        senderName: `${booking.User.firstName} ${booking.User.lastName}`,
        senderPhoneNum: `${booking.User.phoneNum}`,
        recieverName: `${booking.rName}`,
        recieverPhoneNum: `${booking.rPhoneNum}`,
        pickupAddress: `${booking.pickupAddress.title} ${booking.pickupAddress.building} ${booking.pickupAddress.city} ${booking.pickupAddress.state} ${booking.pickupAddress.zip}`,
        pickupBuiliding: `${booking.pickupAddress.building}`,
        pickupCity: `${booking.pickupAddress.city}`,
        pickupState: `${booking.pickupAddress.state}`,
        pickupZip: `${booking.pickupAddress.zip}`,
        dropoffAddress: `${booking.deliveryAddress.title} ${booking.deliveryAddress.building} ${booking.deliveryAddress.city} ${booking.deliveryAddress.state} ${booking.deliveryAddress.zip}`,
        dropoffBuiliding: `${booking.deliveryAddress.building}`,
        dropoffCity: `${booking.deliveryAddress.city}`,
        dropoffState: `${booking.deliveryAddress.state}`,
        dropoffZip: `${booking.deliveryAddress.zip}`,
        pickupLat: `${booking.pickupAddress.lat}`,
        pickupLng: `${booking.pickupAddress.lng}`,
        deliveryLat: `${booking.deliveryAddress.lat}`,
        deliveryLng: `${booking.deliveryAddress.lng}`,
        note: `${booking.note}`,
        pickupTime: `${booking.pickupDate}`,
        amount: `${booking.grandTotal}`,
        bookingStatusId: `${booking.BookingStatusId}`,
        bookingStatus: booking.BookingStatus === null ? 'No status' : `${booking.BookingStatus.name}`,
        distance: `${dist}`,
        estEarning: booking.estDriverEarning ? `${earn}` : 'Not found',
        driverName: booking.DriverId ? `${booking.Driver.firstName} ${booking.Driver.lastName}` : "",
        driverImage: booking.DriverId ? `${booking.Driver.profilePicture}` : "",
        couponValue: booking.CouponId ? `${booking.Coupon.discount}` : '0',
        load_unloadStaus: `${booking.load_unload}`,
        totalWeight: `${totalWeight}`,
        baseRate: load_unload_rate.value,
        basePrice: load_unload_rate.amount,
        rating: booking.Rating ? true : false,
        ratingValue: booking.Rating ? booking.Rating.rate : 'No rating',
        tip: booking.Tip ? booking.Tip.amount : 'No tip',
        packages: pckgArr,
        history: histArr
    };
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Booking by book Id`,
        'Response': outObj,
        'errors': '',
    })
}));

/*
            5. Get active bookings by User ID
    __________________________________________________________________
    a. Filter out the desired data
    b. Return the resposnse
*/
router.get('/getactiveby/:userid', asyncMiddleware(async (req, res) => {
    let userId = req.params.userid;
    const booking = await Booking.findAll({ where: { [Op.and]: [{ UserId: userId }, { condition: 1 }, { paymentConfirmed: true }] }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: Package, include: [{ model: Category }] }, { model: BookingStatus }] });
    let outArr = [];

    booking.map((book, indx) => {
        let pckgArr = [];
        book.Packages.map((pckg, idx) => {
            let tmpObj = {
                dimensions: `${pckg.length} * ${pckg.width} * ${pckg.height}`,
                weight: pckg.weight,
                worth: pckg.estWorth,
                category: pckg.Category.name
            }
            pckgArr[idx] = tmpObj;
        });
        const tmpObj = {
            senderName: `${book.User.firstName} ${book.User.lastName}`,
            senderPhoneNum: book.User.phoneNum,
            recieverName: book.rName,
            recieverPhoneNum: book.rPhoneNum,
            pickupAddress: `${book.pickupAddress.building}, ${book.pickupAddress.exactAddress}, ${book.pickupAddress.city}, ${book.pickupAddress.state}, ${book.pickupAddress.zip}`,
            dropoffAddress: `${book.deliveryAddress.building}, ${book.deliveryAddress.exactAddress}, ${book.deliveryAddress.city}, ${book.deliveryAddress.state}, ${book.deliveryAddress.zip}`,
            note: book.note,
            pickupTime: book.pickupDate,
            amount: book.grandTotal,
            bookingStatus: book.BookingStatus === null ? 'No status' : book.BookingStatus.name,
            packages: pckgArr,
        }
        outArr[indx] = tmpObj;
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Booking by User Id ${userId}`,
        'Response': outArr,
        'errors': '',
    });
}));

/*
            6. Get all bookings by user Id without any condition
    __________________________________________________________________________
*/
router.get('/getall/:userid', asyncMiddleware(async (req, res) => {
    let userId = req.params.userid;
    const bookings = await Booking.findAll({ where: { UserId: userId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: BookingStatus }] });
    //return res.json(bookings)
    let outArr = [];
    bookings.map((book, index) => {
        let tmpObj = {
            orderNumber: book.id,
            senderName: `${book.User.firstName} ${book.User.firstName}`,
            senderPhoneNum: `${book.pickupAddress.phoneNum}`,
            orderDate: book.pickupDate,
            receiverName: `${book.rName}`,
            receiverPhoneNum: `${book.deliveryAddress.phoneNum}`,
            pickupAddress: `${book.pickupAddress.exactAddress}`,
            deliveryAddress: `${book.deliveryAddress.exactAddress}`,
            amount: book.grandTotal,
            estEarning: `${book.estDriverEarning}`,
            status: `${book.BookingStatus.name}`,
            distance: `${book.totalDistance}`
        };
        outArr[index] = tmpObj;
    })

    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Bookings by User Id ${userId}`,
        'Response': outArr,
        'errors': '',
    });
}));

/*
            7. Job Pool (Active within radius of Driver)
    __________________________________________________________________
    a. Get the radial distance from the driver along with its lat and lng
    b. Get all the active booking to which no driver is assigned 
    c. Check the distance between pickup and driver location for each booking
    d. if that distance is less than radial distance, filter out that booking
    e. Return the success response  
*/
router.post('/getactive', validateToken, asyncMiddleware(async (req, res) => {
    const { radialDistance, driverLng, driverLat } = req.body;
    const driver = await DriverDetails.findOne({ where: { UserId: req.user.id } });
    let outArr = [];
    let idx = 0;
    //Getting all the booking that are active and whose payment has been confirmed
    let actBookings = await Booking.findAll({ where: { [Op.and]: [{ condition: true }, { paymentConfirmed: true }, { DriverId: null }, { VehiclesTypeId: driver.VehiclesTypeId }] }, include: [{ model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }] });
    //return res.json(actBookings);
    // if(!actBookings){
    //     return res.json('No booking found'); 
    // }
    const cancelledBookings = await CancelHistory.findAll({ where: { UserId: req.user.id } });
    //return res.json(cancelledBookings);
    actBookings.map((sBook, index) => {
        // get the distance of all bookings with to the distance of that driver 
        let distance = getDistance(driverLat, driverLng, sBook.pickupAddress.lat, sBook.pickupAddress.lng);
        // check if distance is less than the provided distance the show that booking
        if (distance < radialDistance) {
            let dist = Math.round((sBook.totalDistance * 100)) / 100;
            let earn = Math.round((sBook.estDriverEarning * 100)) / 100;
            let tmpObj = {
                //distance,
                bookingId: sBook.id,
                pickUp: `${sBook.pickupAddress.building}, ${sBook.pickupAddress.exactAddress}, ${sBook.pickupAddress.city}, ${sBook.pickupAddress.state}, ${sBook.pickupAddress.zip}`,
                delivery: `${sBook.deliveryAddress.building}, ${sBook.deliveryAddress.exactAddress}, ${sBook.deliveryAddress.city}, ${sBook.deliveryAddress.state}, ${sBook.deliveryAddress.zip}`,
                totalDistance: `${dist}`,
                estdEarning: sBook.estDriverEarning ? `${earn}` : 'undefined',
            };
            outArr[idx] = tmpObj;
            idx++;
        }
    });
    //return res.json(outArr);
    // Filtering the bookings cancelled by this driver so that they will not appear in his job pool
    cancelledBookings.map((b, idx) => {
        outArr = outArr.filter(ele => ele.bookingId !== b.BookingId);
    });
    //return res.json(outArr);
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Active bookings within provided radius`,
        'Response': outArr.reverse(),
        'errors': '',
    });
}));

/*
            8. Get all booking by user ID by Date filter
    ___________________________________________________________________________ 
*/
router.post('/getfiltered/:userid', asyncMiddleware(async (req, res) => {
    const { from, to } = req.body;
    let userId = req.params.userid;
    const bookings = await Booking.findAll({
        where: {
            UserId: userId, paymentConfirmed: true, pickupDate: {
                [Op.between]: [`${from} 00:00`, `${to} 23:59`]
            }
        }, include: [{ model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }]
    });
    let outArr = [];
    bookings.map((book, index) => {
        let tmpObj = {
            orderNumber: book.id,
            orderDate: book.pickupDate,
            pickupAddress: `${book.pickupAddress.title} ${book.pickupAddress.building} ${book.pickupAddress.city} ${book.pickupAddress.state} ${book.pickupAddress.zip}`,
            pickupBuiliding: `${book.pickupAddress.building}`,
            pickupCity: `${book.pickupAddress.city}`,
            pickupState: `${book.pickupAddress.state}`,
            pickupZip: `${book.pickupAddress.zip}`,
            dropoffAddress: `${book.deliveryAddress.title} ${book.deliveryAddress.building} ${book.deliveryAddress.city} ${book.deliveryAddress.state} ${book.deliveryAddress.zip}`,
            dropoffBuiliding: `${book.deliveryAddress.building}`,
            dropoffCity: `${book.deliveryAddress.city}`,
            dropoffState: `${book.deliveryAddress.state}`,
            dropoffZip: `${book.deliveryAddress.zip}`,
            amount: `${book.grandTotal}`,
        };
        outArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Booking by User Id ${userId}`,
        'Response': outArr.reverse(),
        'errors': '',
    });
}));

/*
            9. Change status of booking and Create a time stamp
    __________________________________________________________________
    a. Get the status and booking ID from request body
    b. Get the status ID
    c. Update status of booking & if(it is cancelled or delivered => change the condition as well)
    d. Create history in the history table
*/
router.post('/change/bookingstatus', asyncMiddleware(async (req, res) => {
    const { status, bookingId } = req.body;
    const statusinfo = await BookingStatus.findOne({ where: { name: status } })
    //return res.json(statusinfo)
    Booking.update({ BookingStatusId: statusinfo.id }, { where: { id: bookingId } })
        .then(() => {
            if (status === 'Order Delivered' || status === 'Order Cancelled') {
                Booking.update({ condition: false }, { where: { id: bookingId } })
            }
            let cDate = new Date();
            let date = cDate.getDate();
            let month = cDate.getMonth() + 1;
            let year = cDate.getFullYear();
            let hours = cDate.getHours();
            let minutes = cDate.getMinutes();
            //console.log(`${month}/${date}/${year} ${hours}:${minutes}`);
            BookingHistory.create({
                time: `${month}/${date}/${year} ${hours}:${minutes}`,
                BookingId: bookingId,
                BookingStatusId: statusinfo.id,
            })
                .then(dat => { res.send('Status Changed') })
                .catch(err => { res.send('Error') })
        })

}))

/*
            10. Get history of booking using Booking ID
    ___________________________________________________________________________
*/
router.post('/history', asyncMiddleware(async (req, res) => {
    const { bookingId } = req.body;
    const bookinghistory = await BookingHistory.findAll({ where: { BookingId: bookingId }, include: [{ model: BookingStatus }] })
    const booking = await Booking.findOne({ where: { id: bookingId }, include: [{ model: Users, as: 'Driver' }] });
    //return res.json(booking);
    let tmpArr = [];
    bookinghistory.map((e, i) => {
        let tmpObj = {
            id: `${e.BookingStatusId}`,
            status: `${e.BookingStatus.name}`,
            time: `${e.time}`,
        }
        tmpArr[i] = tmpObj
    })
    tmpArr.shift();
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Booking status',
        'Response': tmpArr,
        'data': {
            orderId: bookingId,
            receiverName: booking.rName,
            driverName: `${booking.Driver.firstName} ${booking.Driver.lastName}`,
            driverDVToken: booking.Driver.deviceToken,
            driverPhoneNum: booking.Driver.phoneNum,
        },
        'errors': '',
    })
}))



/* 
            11. Get All Cancelled Bookings
    __________________________________________________________________
*/
router.get('/cancelled', asyncMiddleware(async (req, res) => {
    const cancelled = await CancelHistory.findAll({ include: [{ model: Users }, { model: Booking, include: [{ model: Users, as: 'User' }] }] });
    //return res.json(cancelled)
    if (!cancelled) {
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': `No record found`,
            'Response': [],
            'errors': '',
        });
    }
    let outArr = [];
    cancelled.map((val, idx) => {
        let cancelledBy = val.User.UserCategoryId === 2 ? 'Customer' : 'Driver';
        let tmpObj = {
            cancelledBy,
            cancelledByUserId: val.UserId,
            cancelledByName: `${val.User.firstName} ${val.User.lastName}`,
            bookedByName: `${val.Booking.User.firstName} ${val.Booking.User.lastName} `,
            orderNum: val.BookindId,
            total: val.Booking.grandTotal,
            note: val.note,
            reason: val.reason,
            cancelledAt: val.cancelledAt,
        };
        outArr[idx] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Cancelled Orders Data`,
        'Response': outArr,
        'errors': '',
    });
}));

/*
            12. ON GOING BOOKINGS for a driver - Driver App
    ___________________________________________________________________________-
    a. Find all the bookings of a rider which are still active
    b. Return the array after filtering some of the data
*/
router.get('/current/rider', validateToken, asyncMiddleware(async (req, res) => {
    const bookings = await Booking.findAll({ where: { DriverId: req.user.id, condition: true }, include: [{ model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: BookingStatus }] });
    //return res.json(bookings)
    let tmpArr = [];
    bookings.map((book, idx) => {
        let dist = Math.round((book.totalDistance * 100)) / 100;
        let earn = Math.round((book.estDriverEarning * 100)) / 100;
        //console.log(book.deliveryAddress.building)
        let tmpObj = {
            orderNum: `${book.id}`,
            pickup: `${book.pickupAddress.building}, ${book.pickupAddress.exactAddress}, ${book.pickupAddress.city}, ${book.pickupAddress.state}, ${book.pickupAddress.zip}`,
            delivery: `${book.deliveryAddress.building}, ${book.deliveryAddress.exactAddress}, ${book.deliveryAddress.city}, ${book.deliveryAddress.state}, ${book.deliveryAddress.zip}`,
            totalDistance: `${dist}`,
            estDriverEarning: book.estDriverEarning ? `${earn}` : 'NA',
            statusId: `${book.BookingStatusId}`,
            status: book.BookingStatus.name,
        };
        tmpArr[idx] = tmpObj;
    })
    res.json({
        'ResponseCode': '1',
        'ResponseMessage': `On going Orders`,
        'Response': tmpArr.reverse(),
        'errors': '',
    });
}));

/*
            13. Booking Details by using booking ID for driver
    __________________________________________________________________________
    a. Get the booking and filter out its packages data 
    b. Get driver Lat and Lng from firebase
    c. Get pickup ETA and dropOff ETA
    d. Return the success response
*/
router.post('/getby/driver' , validateToken , asyncMiddleware(async (req, res) => {
    let {bookId} = req.body;
    let userId = req.user.id;
    const booking = await Booking.findOne({ where: { id: bookId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: Package, include: [{ model: Category }] }, { model: BookingStatus }] });
    //return res.json(booking)
    let pckgArr = [];
    let pickupETA='', dropoffETA= '';
    let adultStatus = false;
    // pickup and drop off lat & lng
    let plat = booking.pickupAddress.lat;
    let plng = booking.pickupAddress.lng;
    //return res.json({plat, plng});
    let dlat = booking.deliveryAddress.lat;
    let dlng = booking.deliveryAddress.lng;
    //return res.json({dlat, dlng});
    booking.Packages.map((pckg, idx) => {
        let d = `${pckg.length} * ${pckg.width} * ${pckg.height}`;
        if(pckg.Category.adult_18plus){
          adultStatus = true;  
        }
        let tmpObj = {
            dimensions: `${pckg.length} * ${pckg.width} * ${pckg.height}`,
            unit : `${pckg.unit}` ,
            weight: `${pckg.weight}`,
            worth: `${pckg.estWorth}`,
            category: `${pckg.Category.name}`
        }
        pckgArr[idx] = tmpObj;
    });
    // Getting driver lat and lng 
    axios.get(process.env.FIREBASE_URL)
    .then(dat=>{
        let dId = dat.data.drivers;
        let driverLocation =  dId[`${userId}`];
        //return res.json(driverLocation);
        if(!driverLocation){
                return res.json({
                  'ResponseCode': '0',
                    'ResponseMessage': `Driver offline`,
                    'Response':[],
                    'errors': '', 
                })
        }
        const drlng = driverLocation['lng'];
        const drlat = driverLocation['lat'];
        // Getting ETA from driver to pickUp location
        axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${drlat},${drlng}&destination=${plat},${plng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
            .then((resp) => {
                pickupETA = resp.data.routes[0].legs[0].duration.text;
                //return res.json(pickupETA)
                // Getting ETA from pickUp to dropOff location
                axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${plat},${plng}&destination=${dlat},${dlng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
                    .then((resp) => {
                        dropoffETA = resp.data.routes[0].legs[0].duration.text;
                        //return res.json(dropoffETA)
                        // to limit it upto 2 digits
                        let dist = Math.round((booking.totalDistance * 100)) / 100;
                        let earn = Math.round(( booking.estDriverEarning * 100)) / 100;
                        let outObj = {
                            id: `${bookId}`,
                            driverId: `${userId}`,
                            senderName: `${booking.User.firstName} ${booking.User.lastName}`,
                            senderId: `${booking.User.id}`,
                            senderDVToken: `${booking.User.deviceToken}`,
                            senderPhoneNum: `${booking.User.phoneNum}`,
                            recieverName: `${booking.rName}`,
                            recieverPhoneNum: `${booking.rPhoneNum}`,
                            pickupAddressName: `${booking.pickupAddress.title}`,
                            pickupAddress: `${booking.pickupAddress.title} ${booking.pickupAddress.building}, ${booking.pickupAddress.city}, ${booking.pickupAddress.state}, ${booking.pickupAddress.zip}`,
                            dropoffAddress: `${booking.deliveryAddress.title} ${booking.deliveryAddress.building},  ${booking.deliveryAddress.city}, ${booking.deliveryAddress.state}, ${booking.deliveryAddress.zip} `,
                            dropoffAddressName: `${booking.deliveryAddress.title}`,
                            pickupLat: `${booking.pickupAddress.lat}`,
                            pickupLng: `${booking.pickupAddress.lng}`,
                            deliveryLat: `${booking.deliveryAddress.lat}`,
                            deliveryLng: `${booking.deliveryAddress.lng}`,
                            note: `${booking.note}`,
                            pickupTime: `${booking.pickupDate}`,
                            amount: `${booking.grandTotal}`,
                            bookingStatusId : `${booking.BookingStatusId}`,
                            bookingStatus: booking.BookingStatus === null ? 'No status' : `${booking.BookingStatus.name}`,
                            distance: `${dist}`,
                            estEarning: booking.estDriverEarning? `${earn}`: 'Not found',
                            pickupETA: `${pickupETA}`,
                            dropoffETA: `${dropoffETA}`,
                            adultStatus,
                            packages: pckgArr
                        };
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': `Booking by book Id`,
                            'Response': outObj,
                            'errors': '', 
                        })
                        
                    })
            })
            .catch(err=>{
               return res.json({
                   'ResponseCode': '0',
                    'ResponseMessage': `Error fetching data`,
                    'Response':[],
                    'errors': 'Try again later', 
                })
            })
    });
    
}));

/*
            14. Get driver details associated with a booking -  Customer App
    ___________________________________________________________________________
    a. Find the booking and if no booking exist --> Return No booking Error
    b. Get the lat & lng of Driver from Firebase
    c. Get the lat and lng of delivery address and calculate the ETA of driver to delivery Address
    d. Return the response of desired data
*/
router.post('/driverdetails', asyncMiddleware(async (req, res) => {
    const { bookingId } = req.body;
    const booking = await Booking.findOne({ where: { id: bookingId }, include: [{ model: Address, as: 'deliveryAddress' }, { model: Address, as: 'pickupAddress' }, { model: Users, as: "Driver", include: [{ model: VehicleDetails, where: { status: 1 } }] }, { model: BookingStatus }] });
    if (!booking) {
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': `No booking exist against this ID`,
            'Response': [],
            'errors': '',
        });
    };
    // const dlng = booking.deliveryAddress.lng;
    // const dlat = booking.deliveryAddress.lat;
    const dlng = booking.pickupAddress.lng;
    const dlat = booking.pickupAddress.lat;
    axios.get(process.env.FIREBASE_URL)
        .then(dat => {
            let dId = dat.data.drivers;

            let driverLocation = dId[`${booking.DriverId}`];

            //return res.json(driverLocation);
            if (!driverLocation) {
                return res.json({
                    'ResponseCode': '0',
                    'ResponseMessage': `Driver offline`,
                    'Response': [],
                    'errors': '',
                })
            }

            const drlng = driverLocation['lng'];
            const drlat = driverLocation['lat'];
            //return res.json(booking);
            axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${drlat},${drlng}&destination=${dlat},${dlng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
                // axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=31.5204,74.3587&destination=31.4625,74.2465&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
                .then((resp) => {
                    let ETA = resp.data.routes[0].legs[0].duration.text;
                    let tmpObj = {
                        id: `${booking.DriverId}`,
                        driverName: `${booking.Driver.firstName} ${booking.Driver.lastName}`,
                        driverPhoneNum: `${booking.Driver.phoneNum}`,
                        carNum: `${booking.Driver.VehicleDetails[0].licPlate}`,
                        carName: `${booking.Driver.VehicleDetails[0].make}`,
                        driverImage: `${booking.Driver.profilePicture}`,
                        deviceToken: booking.Driver.deviceToken ? `${booking.Driver.deviceToken}` : 'No token found',
                        estDeliveryTime: `${ETA}`,
                        bookingStatusId: `${booking.BookingStatusId}`,
                        bookingStatus: `${booking.BookingStatus.name}`,
                    };
                    res.status(200).json({
                        'ResponseCode': '1',
                        'ResponseMessage': `Driver Details`,
                        'Response': [tmpObj],
                        'errors': '',
                    });
                })
        });
}));

/*
            6. Get all bookings by Driver Id without any condition
    __________________________________________________________________________
*/
router.get('/getallbydriver/:userid', asyncMiddleware(async (req, res) => {
    let driverId = req.params.userid;
    const bookings = await Booking.findAll({ where: { DriverId: driverId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'pickupAddress' }, { model: Address, as: 'deliveryAddress' }, { model: BookingStatus }] });
    //return res.json(bookings)
    let outArr = [];
    bookings.map((book, index) => {
        let tmpObj = {
            orderNumber: book.id,
            senderName: `${book.User.firstName} ${book.User.lastName}`,
            senderPhoneNum: `${book.pickupAddress.phoneNum}`,
            orderDate: book.pickupDate,
            receiverName: `${book.rName}`,
            receiverPhoneNum: `${book.deliveryAddress.phoneNum}`,
            pickupAddress: `${book.pickupAddress.exactAddress}`,
            deliveryAddress: `${book.deliveryAddress.exactAddress}`,
            amount: book.grandTotal,
            estEarning: `${book.estDriverEarning}`,
            status: `${book.BookingStatus.name}`,
            distance: `${book.totalDistance}`
        };
        outArr[index] = tmpObj;
    })

    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': `Bookings by Driver Id ${driverId}`,
        'Response': outArr,
        'errors': '',
    });
}));



/*
-------------------------------------
|           DASHBOARD DATA          |
-------------------------------------
*/

/*
            D1. Get all earnings related data of completed bookings
    _________________________________________________________________________
*/
router.get('/earnings', asyncMiddleware(async (req, res) => {
    const bookings = await Booking.findAll({ where: { BookingStatusId: 6 }, include: { model: Tips } });
    const cancelledBookingsByUsers = await CancelHistory.findAll({ include: [{ model: Users, where: { UserCategoryId: 2 } }] });
    const cancelledBookingsByDrivers = await CancelHistory.findAll({ include: [{ model: Users, where: { UserCategoryId: 3 } }] });
    //return res.json(cancelledBookingsByDrivers);

    //return res.json(bookings)

    // getting last 7 and 30 days from now
    var cDate = new Date();
    var lastSevenDays = new Date(cDate.getTime() - (7 * 24 * 60 * 60 * 1000));
    var lastthirtyDays = new Date(cDate.getTime() - (30 * 24 * 60 * 60 * 1000));
    var today = new Date();
    today.setHours(0, 0, 1)
    //return res.json(cDate.getHours());
    // Converting to UNIX time
    let unixLastSeven = Date.parse(lastSevenDays);
    let unixLastThirty = Date.parse(lastthirtyDays);
    let unixCurrent = Date.parse(cDate);
    let unixToday = Date.parse(today);

    let totalEarnings = 0, driverEarnings = 0, weeklyTotal = 0, monthlyTotal = 0, weeklyDriver = 0, monthlyDriver = 0, todaysTotal = 0, todaysDriver = 0;
    let weeklyBookings = 0, monthlyBookings = 0, todaysBookings = 0;

    bookings.map((book, index) => {
        totalEarnings = totalEarnings + parseFloat(book.grandTotal);
        driverEarnings = driverEarnings + parseFloat(book.estDriverEarning);
        if (book.Tip) {
            totalEarnings = totalEarnings + parseFloat(book.Tip.amount);
            driverEarnings = driverEarnings + parseFloat(book.Tip.amount);
        }
        // Checking if the pick date is between current and last 7 days
        if (unixCurrent > Date.parse(book.pickupDate) && unixLastSeven < Date.parse(book.pickupDate)) {
            weeklyTotal = weeklyTotal + parseFloat(book.grandTotal);
            weeklyDriver = weeklyDriver + parseFloat(book.estDriverEarning);
            if (book.Tip) {
                weeklyTotal = weeklyTotal + parseFloat(book.Tip.amount);
                weeklyDriver = weeklyDriver + parseFloat(book.Tip.amount);
            }
            weeklyBookings++;
        }
        // Checking if the pick date is between current and last 30 days
        if (unixCurrent > Date.parse(book.pickupDate) && unixLastThirty < Date.parse(book.pickupDate)) {
            monthlyTotal = monthlyTotal + parseFloat(book.grandTotal);
            monthlyDriver = monthlyDriver + parseFloat(book.estDriverEarning);
            if (book.Tip) {
                monthlyTotal = monthlyTotal + parseFloat(book.Tip.amount);
                monthlyDriver = monthlyDriver + parseFloat(book.Tip.amount);
            }
            monthlyBookings++;
        }
        // Checking if the pick date is todays
        if (unixCurrent > Date.parse(book.pickupDate) && unixToday < Date.parse(book.pickupDate)) {
            todaysTotal = todaysTotal + parseFloat(book.grandTotal);
            todaysDriver = todaysDriver + parseFloat(book.estDriverEarning);
            if (book.Tip) {
                todaysTotal = todaysTotal + parseFloat(book.Tip.amount);
                todaysDriver = todaysDriver + parseFloat(book.Tip.amount);
            }
            todaysBookings++;
        }

    });

    let adminEarnbyCancelTotal = 0, adminEarnbyCancelWeekly = 0, weeklyCancel = 0, adminEarnbyCancelMonthly = 0, monthlyCancel = 0, adminEarnbyCancelToday = 0, todayCancel = 0;
    cancelledBookingsByUsers.map((c, i) => {
        //console.log(c);
        c.cancelledAt = Date.parse(c.cancelledAt);
        //console.log(unixCurrent, c.cancelledAt , unixLastSeven)
        adminEarnbyCancelTotal = adminEarnbyCancelTotal + parseFloat(c.amount);
        // Checking if the cancelDate is between current and last 7 days
        if (unixCurrent > c.cancelledAt && unixLastSeven < c.cancelledAt) {
            adminEarnbyCancelWeekly = adminEarnbyCancelWeekly + parseFloat(c.amount);
            weeklyCancel++;
        }
        // Check if the cancelDate is between current and last 30 days
        if (unixCurrent > c.cancelledAt && unixLastThirty < c.cancelledAt) {
            adminEarnbyCancelMonthly = adminEarnbyCancelMonthly + parseFloat(c.amount);
            monthlyCancel++;
        }
        // Check if the cancelDate is between current and till now
        if (unixCurrent > c.cancelledAt && unixToday < c.cancelledAt) {

            adminEarnbyCancelToday = adminEarnbyCancelToday + parseFloat(c.amount);
            todayCancel++;
        }

    });
    //return res.json(monthlyCancel)
    let driverPenaltyTotal = 0, driverPenaltyWeekly = 0, driverPenaltyMonthly = 0, driverPenaltyToday = 0;
    cancelledBookingsByDrivers.map((c, i) => {
        //console.log(unixCurrent, c.cancelledAt, unixLastSeven)
        c.cancelledAt = Date.parse(c.cancelledAt);
        driverPenaltyTotal = driverPenaltyTotal + parseFloat(c.amount);
        // Checking if the cancelDate is between current and last 7 days
        if (unixCurrent > c.cancelledAt && unixLastSeven < c.cancelledAt) {
            driverPenaltyWeekly = driverPenaltyWeekly + parseFloat(c.amount);
        }
        // Check if the cancelDate is between current and last 30 days
        if (unixCurrent > c.cancelledAt && unixLastThirty < c.cancelledAt) {
            driverPenaltyMonthly = driverPenaltyMonthly + parseFloat(c.amount);
        }
        // Check if the cancelDate is between current and till now
        if (unixCurrent > c.cancelledAt && unixToday < c.cancelledAt) {
            driverPenaltyToday = driverPenaltyToday + parseFloat(c.amount);
        }

    });

    totalEarnings = totalEarnings + driverPenaltyTotal + adminEarnbyCancelTotal;
    weeklyTotal = weeklyTotal + adminEarnbyCancelWeekly + driverPenaltyWeekly;
    monthlyTotal = monthlyTotal + adminEarnbyCancelMonthly + driverPenaltyMonthly;
    todaysTotal = todaysTotal + adminEarnbyCancelToday + driverPenaltyToday;
    //return res.json(adminEarnbyCancel)
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': `DashBoard Data`,
        'Response': {
            totalEarnings: Math.round((totalEarnings * 100)) / 100,
            totaldriverEarnings: Math.round((driverEarnings * 100)) / 100,
            totalAdminCommission: Math.round(((totalEarnings - driverEarnings) * 100)) / 100,
            adminEarnbyCancelTotal: Math.round((adminEarnbyCancelTotal * 100)) / 100,
            driverPenaltyTotal: Math.round((driverPenaltyTotal * 100)) / 100,
            totalBookings: bookings.length,
            weeklyTotal: Math.round((weeklyTotal * 100)) / 100,
            weeklyDriver: Math.round((weeklyDriver * 100)) / 100,
            weeklyAdminCommission: Math.round(((weeklyTotal - weeklyDriver) * 100)) / 100,
            cancelEarningsWeekly: Math.round((adminEarnbyCancelWeekly * 100)) / 100,
            driverPenaltyWeekly: Math.round((driverPenaltyWeekly * 100)) / 100,
            weeklyBookings,
            weeklyCancel,
            monthlyTotal: Math.round((monthlyTotal * 100)) / 100,
            monthlyDriver: Math.round((monthlyDriver * 100)) / 100,
            monthlyAdminCommission: Math.round(((monthlyTotal - monthlyDriver) * 100)) / 100,
            monthlyBookings,
            cancelEarningsMonthly: Math.round((adminEarnbyCancelMonthly * 100)) / 100,
            driverPenaltyMonthly: Math.round((driverPenaltyMonthly * 100)) / 100,
            monthlyCancel,
            todaysTotal: Math.round((todaysTotal * 100)) / 100,
            todaysDriver: Math.round((todaysDriver * 100)) / 100,
            todaysAdminCommission: Math.round(((todaysTotal - todaysDriver) * 100)) / 100,
            todaysBookings,
            cancelEarningsToday: Math.round((adminEarnbyCancelToday * 100)) / 100,
            driverPenaltyToday: Math.round((driverPenaltyToday * 100)) / 100,
            todayCancel,
        },
        'errors': '',
    });
}));

/*
            D2. Get filtered earnings related data of completed Bookings
    ___________________________________________________________________________
*/
router.post('/filtered/earnings', asyncMiddleware(async (req, res) => {
    const { from, to } = req.body;
    const bookings = await Booking.findAll({
        where: {
            BookingStatusId: 6, pickupDate: {
                [Op.between]: [`${from} 00:00`, `${to} 23:59`]
            }
        }, include: [{ model: Users, as: 'User' }, { model: Users, as: 'Driver' }, { model: Tips }]
    });
    //return res.json(bookings)
    // const cancelledBookingsByUsers = await CancelHistory.findAll({include: [{model: Users, where: {UserCategoryId: 2} }]});
    // const cancelledBookingsByDrivers = await CancelHistory.findAll({include: [{model: Users, where: {UserCategoryId: 3} }]});

    //return res.json(bookings)
    let totalEarnings = 0, driverEarnings = 0;
    let outArr = [];
    bookings.map((book, index) => {
        totalEarnings = totalEarnings + parseFloat(book.grandTotal);
        driverEarnings = driverEarnings + parseFloat(book.estDriverEarning);
        if (book.Tip) {
            totalEarnings = totalEarnings + parseFloat(book.Tip.amount);
            driverEarnings = driverEarnings + parseFloat(book.Tip.amount);
        }
        let tmpObj = {
            bookingId: book.id,
            bookingTotal: book.grandTotal,
            driverEarning: book.estDriverEarning,
            adminCommission: Math.round(((book.grandTotal - book.estDriverEarning) * 100)) / 100,
            tipAmount: book.Tip ? `${book.Tip.amount}` : 'No tip paid',
            customerName: `${book.User.firstName} ${book.User.lastName}`,
            driverName: `${book.Driver.firstName} ${book.Driver.lastName}`,
            status: 'delivered'
        }
        outArr[index] = tmpObj;
    })
    //return res.json(driverEarnings)
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': `DashBoard Data`,
        'Response': {
            totalEarnings: Math.round((totalEarnings * 100)) / 100,
            totaldriverEarnings: Math.round((driverEarnings * 100)) / 100,
            totalAdminCommission: Math.round(((totalEarnings - driverEarnings) * 100)) / 100,
            totalBookings: bookings.length,
            details: outArr,
        },
        'errors': '',
    });
}));

/*
            D3. Earnings with booking details
    _____________________________________________________________
    a. It shows total earnings of all the completed orders 
    alongwith the list of all the completed Orders
*/
router.get('/details/earnings', asyncMiddleware(async (req, res) => {
    const bookings = await Booking.findAll({ where: { BookingStatusId: 6 }, include: [{ model: Users, as: 'User' }, { model: Users, as: 'Driver' }, { model: Tips }] });
    //const cancelledBookings = await CancelHistory.findAll();
    //return res.json(bookings);


    let totalEarnings = 0, driverEarnings = 0;
    let outArr = [];
    bookings.map((book, index) => {
        totalEarnings = totalEarnings + parseFloat(book.grandTotal);
        driverEarnings = driverEarnings + parseFloat(book.estDriverEarning);
        if (book.Tip) {
            totalEarnings = totalEarnings + parseFloat(book.Tip.amount);
            driverEarnings = driverEarnings + parseFloat(book.Tip.amount);
        }
        let tmpObj = {
            bookingId: book.id,
            bookingTotal: book.grandTotal,
            driverEarning: book.estDriverEarning,
            adminCommission: Math.round(((book.grandTotal - book.estDriverEarning) * 100)) / 100,
            tipAmount: book.Tip ? `${book.Tip.amount}` : 'No tip paid',
            customerName: `${book.User.firstName} ${book.User.lastName}`,
            driverName: `${book.Driver.firstName} ${book.Driver.lastName}`,
            status: 'delivered'
        }
        outArr[index] = tmpObj;
    })
    //return res.json(driverEarnings)
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': `DashBoard Data`,
        'Response': {
            totalEarnings: Math.round((totalEarnings * 100)) / 100,
            totaldriverEarnings: Math.round((driverEarnings * 100)) / 100,
            totalAdminCommission: Math.round(((totalEarnings - driverEarnings) * 100)) / 100,
            totalBookings: bookings.length,
            details: outArr,
        },
        'errors': '',
    });
}));

/*
            D4. Display data on Dash Board
    _________________________________________________________
*/
router.get('/dashboard', asyncMiddleware(async (req, res) => {
    const totalDrivers = await Users.findAll({ where: { UserCategoryId: 3, status: 1 } });
    const pendingDrivers = await Users.findAll({ where: { UserCategoryId: 3, status: null } });
    const totalUsers = await Users.findAll({ where: { UserCategoryId: 2 } });
    //const totalVehicleTypes = await VehiclesType.findAll({where: {status: 1}});
    const allBookings = await Booking.findAll({ where: { BookingStatusId: 6 } });
    const cancelledBookings = await Booking.findAll({ where: { BookingStatusId: 7 } });
    const scheduled = await Booking.findAll({ where: { condition: 1, paymentConfirmed: 1 } });
    const allTips = await Tips.findAll();
    const cancelledBookingsByAll = await CancelHistory.findAll();
    // return res.json(allTips);
    let totalEarnings = 0, driverEarnings = 0;
    allBookings.map((booking, index) => {
        totalEarnings = totalEarnings + parseFloat(booking.grandTotal);
        driverEarnings = driverEarnings + parseFloat(booking.estDriverEarning);
    });
    // Adding tips to both total and driver earnings as well
    allTips.map((tip, index) => {
        totalEarnings = totalEarnings + parseFloat(tip.amount);
        driverEarnings = driverEarnings + parseFloat(tip.amount);
    })
    let adminEarnbyCancelTotal = 0;
    cancelledBookingsByAll.map((c, i) => {
        adminEarnbyCancelTotal = adminEarnbyCancelTotal + parseFloat(c.amount);
    });
    totalEarnings = totalEarnings + adminEarnbyCancelTotal;
    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': `DashBoard Data`,
        'Response': {
            approvedDrivers: totalDrivers ? `${totalDrivers.length}` : 'No record',
            pendingDrivers: pendingDrivers ? `${pendingDrivers.length}` : 'No record',
            totalUsers: totalUsers ? `${totalUsers.length}` : 'No record',
            //totalVehicleTypes: totalVehicleTypes? `${totalVehicleTypes.length}`: 'No record',
            totalcancelledBookings: cancelledBookings ? `${cancelledBookings.length}` : 'No record',
            totalEarnings: totalEarnings ? `${Math.round((totalEarnings * 100)) / 100}` : 'No record',
            totaldriverEarnings: driverEarnings ? `${Math.round((driverEarnings * 100)) / 100}` : 'No record',
            totalAdminCommission: `${Math.round(((totalEarnings - driverEarnings) * 100)) / 100}`,
            totalBookings: `${allBookings.length}`,
            scheduled: scheduled ? `${scheduled.length}` : 'No record',
        },
        'errors': '',
    })
}));

/*
-------------------------------------
|   Graphs and Reporting DATA        |
-------------------------------------
*/

/*
            G1. All Graphs
    ______________________________________________________________
*/
router.get('/graphsdata', asyncMiddleware(async (req, res) => {
    const bookings = await Booking.findAll({ where: { BookingStatusId: 6 }, include: { model: Tips } });
    //const cancelledBookings = await CancelHistory.findAll();
    //return res.json(cancelledBookings);

    //return res.json(bookings)

    // getting last 7 and 30 days from now
    var cDate = new Date();
    //return res.json(cDate.toString())
    let totalEarnings = 0, driverEarnings = 0, tipsEarnings = 0, noOfBookings = 0, orderEarnings = 0;
    let days = [], nos = [], order_earnings = [], tip_earnings = [], driver_earnings = [], total_earnings = [];
    let nosOne = [['Day', 'Orders']], order_earningsOne = [['Day', 'Order Earnings']], tip_earningsOne = [['Day', 'Tips']], driver_earningsOne = [['Day', 'Driver Earnings']], total_earningsOne = [['Day', 'Total Earnings']];
    // Array for 7 days back
    for (let i = 0; i < 7; i++) {
        var oneDayStart = new Date(cDate.getTime() - (i * 24 * 60 * 60 * 1000));
        var oneDayEnd = new Date(cDate.getTime() - (i * 24 * 60 * 60 * 1000));
        oneDayStart.setHours(0, 0, 1)
        oneDayEnd.setHours(23, 59, 0)
        let d = oneDayStart.toUTCString();
        let first3 = d.substring(0, 3);
        days.push(first3);
        //return res.json(days)
        //console.log(oneDayStart.toUTCString(), oneDayEnd.toUTCString())
        // Get all booking whose pickup date is for current day
        oneDayData = bookings.filter(b => Date.parse(oneDayStart) < Date.parse(b.pickupDate) && Date.parse(b.pickupDate) < Date.parse(oneDayEnd))
        oneDayData.map((b, i) => {
            orderEarnings = orderEarnings + parseFloat(b.grandTotal);
            driverEarnings = driverEarnings + parseFloat(b.estDriverEarning)
            if (b.Tip) {
                tipsEarnings = tipsEarnings + parseFloat(b.Tip.amount);
            }
        });
        // creating data points
        // let order_object = {
        //     label: first3,
        //     y: Math.round((orderEarnings * 100)) / 100,
        // };
        // let tip_object = {
        //     label: first3,
        //     y: Math.round((tipsEarnings * 100)) / 100,
        // };
        // let driver_object = {
        //     label: first3,
        //     y: Math.round((driverEarnings * 100)) / 100 ,
        // };
        // let total_object = {
        //     label: first3,
        //     y: Math.round(((orderEarnings+tipsEarnings) * 100)) / 100 ,
        // };
        // let number_object = {
        //     label: first3,
        //     y:oneDayData.length,
        // };

        // Creating array type data points
        let order_array = [first3, Math.round((orderEarnings * 100)) / 100];
        let tip_array = [first3, Math.round((tipsEarnings * 100)) / 100];
        let driver_array = [first3, Math.round((driverEarnings * 100)) / 100];
        let total_array = [first3, Math.round(((orderEarnings + tipsEarnings) * 100)) / 100];
        let number_array = [first3, oneDayData.length];
        // pushing data into arrays
        order_earnings.push(order_array);
        tip_earnings.push(tip_array);
        driver_earnings.push(driver_array);
        total_earnings.push(total_array);
        nos.push(number_array);
        totalEarnings = 0, driverEarnings = 0, tipsEarnings = 0, orderEarnings = 0;
    }
    // Reversing and concatenating arrays
    order_earnings = order_earnings.reverse();
    order_earnings = order_earningsOne.concat(order_earnings);

    tip_earnings = tip_earnings.reverse();
    tip_earnings = tip_earningsOne.concat(tip_earnings);

    driver_earnings = driver_earnings.reverse();
    driver_earnings = driver_earningsOne.concat(driver_earnings);

    total_earnings = total_earnings.reverse();
    total_earnings = total_earningsOne.concat(total_earnings);

    nos = nos.reverse();
    nos = nosOne.concat(nos);

    return res.json({
        'ResponseCode': '1',
        'ResponseMessage': `Graph Data`,
        'Response': {
            titles: ['Earning by orders per week', 'Earning by tips per week', 'Driver earnings per week', 'Total earnings per week', 'Number of orders per week'],
            vAxis: [{ title: "earnings $" }, { title: "earnings $" }, { title: "earnings $" }, { title: "earnings $" }, { title: "# of orders" }],
            data: [order_earnings, tip_earnings, driver_earnings, total_earnings, nos],
        },
        'errors': '',
    })

}));


// User Defined Functions
function getDistance(userLat, userLng, orderLat, orderLng) {
    let earth_radius = 6371;
    let dLat = (Math.PI / 180) * (orderLat - userLat);
    let dLon = (Math.PI / 180) * (orderLng - userLng)
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos((Math.PI / 180) * (orderLat)) * Math.cos((Math.PI / 180) * (orderLat)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    let c = 2 * Math.asin(Math.sqrt(a));
    let d = earth_radius * c;
    return d;
}

module.exports = router;