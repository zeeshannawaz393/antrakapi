require('dotenv').config()
const express = require('express');
const router = express();
const { Users,Booking, Address, BookIDPictures, Wallet } = require('../models');
const axios = require('axios')
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
let FCM = require('fcm-node');
const multer = require('multer')
const path = require('path');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, `Images/BookingIDPictures`)
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
const upload = multer({
    storage: storage,
});
let fcm = new FCM(process.env.FIREBASE_SERVER_KEY);

/*
            1. Order Picked Up by Rider (When Category of Packages is not 18+)
    _____________________________________________________________________________
    a. Check = if the security key added by Driver is correct or not 
        if(not correct) => Response --> Invalid Security key
        if (correct)
        b. Get lat,lng of Driver from Firebase and calculate the ETA of Drivers 
        location to delivery point using Google Maps API
        c. Call another API to change the Booking Status to Picked Up
        d. Send notification to Customer 
        e. Return the success response 
*/
router.post('/pickup', asyncMiddleware(async (req, res) =>{
    const {bookingId, securitykey, userId} = req.body;
    const booking = await Booking.findOne({ where: {id: bookingId }});
    const user = Users.findOne({where: {id: booking.UserId}});
    if (booking.securityKey === securitykey ){
        axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Picked", "bookingId": bookingId})
        .then(succ=>{
            let message = {
                    to: `${user.deviceToken}` , 
                    notification: {
                        title: 'Booking Picked Up', 
                        body: 'Waiting for rider to start ride ' 
                    },
                };
            fcm.send(message, function(err, response){
                if (err) {
                    console.log("Something has gone wrong!");
                                //res.json(err);
                } else {
                    console.log("Successfully sent with response: ", response);
                                //res.json(response);
                }
            });
            return res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Order picked up',
                'Response':[],
                'errors': '', 
            });
        })
    }
    else{
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Invalid Security Key',
            'Response':[],
            'errors': '', 
        })
    }
}));

/*
            2. Order Picked Up by Rider (When Category of Packages is 18+)
    _____________________________________________________________________________
    a. Check = if the security key added by Driver is correct or not 
        if(not correct) => Response --> Invalid Security key
        if (correct)
        b. Get lat,lng of Driver from Firebase and calculate the ETA of Drivers 
        location to delivery point using Google Maps API
        c. Call another API to change the Booking Status to Picked Up
        d. Add ID pictures of the Customer to DB
        e. Send notification to Customer 
        f. Return the success response 
*/
router.post('/pickup/withimages', upload.fields([{ name: 'senderFront' }, { name: 'senderBack' }]), asyncMiddleware(async (req, res) =>{
    const {bookingId, securitykey, userId} = req.body;
    const booking = await Booking.findOne({ where: {id: bookingId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'deliveryAddress' }]});
   // const user = Users.findOne({where: {id: booking.UserId}});
    //return res.json(booking.User.deviceToken)
    //return res.json(req.files)
    const dlng = booking.deliveryAddress.lng;
    const dlat = booking.deliveryAddress.lat;
    if (booking.securityKey === securitykey ){
        axios.get(process.env.FIREBASE_URL)
        .then(dat=>{
            let dId = dat.data.drivers;
            let driverLocation =  dId[`${userId}`];
            if(!driverLocation){
                return res.json({
                   'ResponseCode': '0',
                    'ResponseMessage': `Driver offline`,
                    'Response':[],
                    'errors': '', 
                })
            }
            const drlng = driverLocation['lng'];
            const drlat = driverLocation['lat'];
            //return res.json(booking);
            axios.get(`https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=${dlat},${dlng}&destination=${drlat},${drlng}&key=AIzaSyDoVmHrVkO68EObrVfhWrzgbAHHPQ9McMM`)
            .then((resp) => {
                let ETA = resp.data.routes[0].legs[0].duration.text;
                //console.log(resp.data.routes[0].legs[0].duration.text)
                axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Picked", "bookingId": bookingId})
                .then(succ=>{
                     BookIDPictures.create({
                         senderFront: req.files.senderFront[0].path,
                         senderBack: req.files.senderBack[0].path,
                         BookingId: bookingId,
                     });
                    let message = {
                            to: `${booking.User.deviceToken}`, 
                            notification: {
                                title: 'Booking Picked Up', 
                                body: 'Our rider has picked up your order' 
                            },
                        };
                        fcm.send(message, function(err, response){
                            if (err) {
                                console.log("Something has gone wrong!");
                                //res.json(err);
                            } else {
                                console.log("Successfully sent with response: ", response);
                                //res.json(response);
                            }
                        });
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Picked Up',
                            'Response':[{
                                ETA,
                                building: booking.building? booking.building: '',
                                aptNum: booking.aptNum? booking.aptNum: '',
                                city: booking.city? booking.city: '',
                                state: booking.state? booking.state: '',
                                zip: booking.zip? booking.zip: '',
                                deliveryAddress: `${booking.deliveryAddress.building}, ${booking.deliveryAddress.exactAddress}, ${booking.deliveryAddress.city}, ${booking.deliveryAddress.state}, ${booking.deliveryAddress.zip}`,
                                recieverName: booking.rName,
                                recieverPhoneNum: booking.deliveryAddress.phoneNum,
                                senderDvToken: booking.User.deviceToken,
                                lat: dlat,
                                lng: dlng,
                            }],
                            'errors': '', 
                        })
                });
                //res.json(resp.data.routes[0].legs[0].duration.text);
            })
            .catch(err=>{
                //res.send(err)
                res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Error getting ETA',
                    'Response':[],
                    'errors': '', 
                })
            })
            //console.log(drlat, drlng, plat,plng)
            //res.json(driverLocation);
        })
        .catch(err=>{
            //console.log(err);
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error getting Driver Data',
                'Response':[],
                'errors': '', 
            })
            //res.send('Error in loading Driver Data')
        })
    }
    else{
        res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'Invalid Security Key',
            'Response':[],
            'errors': '', 
        })
    }
}));

/*
            3. Delivered - when Category is not 18+
    _______________________________________________________________________
    a. Change the Booking Status to Deliverd by calling an API
    b. Send the notification to customer that ride has completed
    c. Create a entry in table indicating the earning of Rider
    d. Return success response
*/
router.post('/end', asyncMiddleware(async (req, res) =>{
   const {bookingId, userId} = req.body; 
   const booking = await Booking.findOne({ where: {id: bookingId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'deliveryAddress' }]});
   //const user = Users.findOne({where: {id: 1}});
   //return res.json(booking)
   axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Delivered", "bookingId": bookingId})
    .then(succ=>{
        let message = {
            to: `${booking.User.deviceToken}`, 
            notification: {
                title: 'Ride completed', 
                body: 'Our rider has delivered your order' 
            },
            data: {
                ride_completed: true
            },
        };
        fcm.send(message, function(err, response){
            if (err) {
                console.log("Something has gone wrong!");
                //res.json(err);
            } else {
                console.log("Successfully sent with response: ", response);
                //res.json(response);
            }
        });
        Wallet.create({
            amount: booking.estDriverEarning,
            BookingId: bookingId,
            UserId: userId,
        })
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Delivered',
            'Response':[{
                estEarning: booking.estDriverEarning? `${booking.estDriverEarning}`: '',
            }],
            'errors': '', 
        })
    })
}));

/*
            4. Delivered - when Category is 18+
    _______________________________________________________________________
    a. Change the Booking Status to Deliverd by calling an API
    b. Create a entry in table to add Receivers ID card Images
    c. Send the notification to customer that ride has completed
    d. Create a entry in table indicating the earning of Rider
    e. Return success response
*/
router.post('/end/withimages', upload.fields([{ name: 'receiverFront' }, { name: 'receiverBack' }]) ,asyncMiddleware(async (req, res) =>{
   const {bookingId, userId} = req.body; 
   const booking = await Booking.findOne({ where: {id: bookingId }, include: [{ model: Users, as: 'User' }, { model: Address, as: 'deliveryAddress' }]});
   //const user = Users.findOne({where: {id: booking.UserId}});
   axios.post(`${process.env.BASE_URL}/booking/change/bookingstatus`, { "status": "Order Delivered", "bookingId": bookingId})
    .then(succ=>{
        BookIDPictures.create({
            receiverFront: req.files.receiverFront[0].path,
            receiverback: req.files.receiverBack[0].path,
            BookingId: bookingId,
        });
        let message = {
            to: `${booking.User.deviceToken}`, 
            notification: {
                title: 'Ride completed', 
                body: 'Our rider has delivered your order' 
            },
            data: {
                ride_completed: true
            },
        };
        fcm.send(message, function(err, response){
            if (err) {
                console.log("Something has gone wrong!");
                //res.json(err);
            } else {
                console.log("Successfully sent with response: ", response);
                //res.json(response);
            }
        });
        Wallet.create({
            amount: booking.estDriverEarning,
            BookingId: bookingId,
            UserId: userId,
        })
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Delivered',
            'Response':[{
                estEarning: booking.estDriverEarning? `${booking.estDriverEarning}`: '',
            }],
            'errors': '', 
        })
    })
}));

module.exports = router;