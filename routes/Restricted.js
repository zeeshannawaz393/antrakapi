const express = require('express');
const router = express();
const {Restricted, CardBrands} = require('../models');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');
const multer = require('multer')
const path = require('path')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null,`Images/Restricted`)
    },
    filename: (req, file, cb) => {
        cb(null,path.basename(file.originalname, path.extname(file.originalname)) + '-'  + Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({
    storage: storage,
    limits: { fileSize: '1000000' },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/
        const mimeType = fileTypes.test(file.mimetype)  
        const extname = fileTypes.test(path.extname(file.originalname))

        if(mimeType && extname) {
            return cb(null, true)
        }
        cb('Give proper files formate to upload')
    }
}).single('image')

/*
            1. Add a restricted Item
    ___________________________________________________
    a. Check, if already exists, Return --> Error
    b. Create a new entry and Return --> Success
*/
router.post('/add', upload, asyncMiddleware(async (req, res) => {
    const {name} = req.body;
    const exist = await Restricted.findOne({where: {name: name, status: 1}});
    if(exist){
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'This item exists already',
            'Response': [],
            'errors': 'This item exists already',
        })
    }
    Restricted.create({
       name,
       image:`${req.file.path}`,
       status: true,
    })
    .then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Restricted Item Added',
            'Response': ['New Restricted item has been added'],
            'errors': '',
        })
    })
}));

/*
            2. Get all Restricted Items
    ___________________________________________________
*/
router.get('/getall', asyncMiddleware(async (req, res) => {
   const items= await Restricted.findAll({where: {status: 1}});
   let titles = [];
   let pictures =[];
   if(!items) {
       return res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'No items exist',
            'Response': [],
            'errors': '',
        })
   }
   items.map((item, index)=>{
       titles[index] = item.name;
       pictures[index] = item.image;
   })
   res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Restricted Items',
            'Response': [{
                titles: titles,
                images: pictures,
            }],
            'errors': '',
        })
   
}));

/*
            3. Get all Restricted Items / Admin
    ___________________________________________________
*/
router.get('/getall/admin', asyncMiddleware(async (req, res) => {
   const items= await Restricted.findAll({where: {status: 1}});
   outArr = [];
   if(!items) {
       return res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'No items exist',
            'Response': [],
            'errors': '',
        })
   }
   items.map((item, index)=>{
       let tmpObj = {
           id: item.id,
           name: item.name,
           image: item.image,
           status: item.status,
       };
       outArr.push(tmpObj);
   })
   res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Restricted Items',
            'Response': outArr,
            'errors': '',
        })
   
}));

/*
            4. Block- Delete Restricted Item / Admin
    ___________________________________________________
*/
router.put('/block/:id', asyncMiddleware(async (req, res) => {
   const items= await Restricted.findOne({where: {id: req.params.id}});
   if(!items) {
       return res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'No items exist',
            'Response': [],
            'errors': '',
        })
   }
   Restricted.update({status: 0}, {where: { id: req.params.id}})
   .then(()=>{
       res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Blocked',
            'Response':'',
            'errors': '',
        });
   })
}));


module.exports = router;