require("dotenv").config();
const express = require("express");
const router = express();
const {
    PaymentMethod,
 
} = require("../models");

const asyncMiddleware = require("../middlewares/async");

//add payment method
router.post(
    "/add_payment_method",
    asyncMiddleware(async (req, res) => {
        
        let { title, clientKey, secretKey } = req.body;
        const method = await PaymentMethod.findOne({
            where: [{ status: true }, { title: title }],
        });
        if (method) {
            return res.json({
                ResponseCode: "0",
                ResponseMessage: "Payment already exist",
                Response: [],
                errors: "ayment already exist",
            });
        } else {
            const old_method = await PaymentMethod.findOne({
                where: [{ status: false }, { title: title }],
            });
            if (old_method) {
                old_method.status = true;
                old_method
                    .save()
                    .then((dat) => {
                        return res.json({
                            ResponseCode: "1",
                            ResponseMessage: "Payment added successfully!",
                            Response: [],
                            errors: "",
                        });
                    })
                    .catch((error) => {
                        return res.json({
                            ResponseCode: "0",
                            ResponseMessage: error.message,
                            Response: [],
                            errors: error.message,
                        });
                    });
            } else {
                const new_method = new PaymentMethod();
                new_method.title = title;
                new_method.secretKey = secretKey;
                new_method.clientKey = clientKey;
                new_method.status = true;
                new_method
                    .save()
                    .then((meth) => {
                        return res.json({
                            ResponseCode: "1",
                            ResponseMessage: "Payment method added successfully!",
                            Response: [],
                            errors: "",
                        });
                    })
                    .catch((error) => {
                        return res.json({
                            ResponseCode: "0",
                            ResponseMessage: error.message,
                            Response: [],
                            errors: error.message,
                        });
                    });
            }
        }
    })
);

// get all payment methods
router.get(
    "/get_payment_methods",
    asyncMiddleware(async (req, res) => {
        
     const methods = await PaymentMethod.findAll({});
     return res.json({
        ResponseCode: "1",
        ResponseMessage: "All payment methods",
        Response: methods,
        errors: "",
    });
    })
);

// update payment methods
router.post(
    "/update_payment_method",
    asyncMiddleware(async (req, res) => {
    const {id,title,secretKey,clientKey} = req.body;
    const method = await PaymentMethod.findOne({where:[{id:id}]});
    if(method)
    {
        method.title = title;
        method.secretKey = secretKey;
        method.clientKey = clientKey;
        method.save().then(dat=>{
            return res.json({
                ResponseCode: "1",
                ResponseMessage: "Payment method updated successfully!",
                Response: {},
                errors: "",
            });
        })
        .catch((error) => {
            return res.json({
                ResponseCode: "0",
                ResponseMessage: error.message,
                Response: [],
                errors: error.message,
            });
        });
    }
    
    })
);

// block payment method
router.post(
    "/block_payment_method",
    asyncMiddleware(async (req, res) => {
    const {id} = req.body;
    const method = await PaymentMethod.findOne({where:[{id:id}]});
    if(method)
    {
        method.status = false;
        method.save().then(dat=>{
            return res.json({
                ResponseCode: "1",
                ResponseMessage: "Payment method blocked successfully!",
                Response: {},
                errors: "",
            });
        })
        .catch((error) => {
            return res.json({
                ResponseCode: "0",
                ResponseMessage: error.message,
                Response: [],
                errors: error.message,
            });
        });
    }
    
    })
);

// activte payment method
router.post(
    "/active_payment_method",
    asyncMiddleware(async (req, res) => {
    const {id} = req.body;
    const method = await PaymentMethod.findOne({where:[{id:id}]});
    if(method)
    {
        method.status = true;
        method.save().then(dat=>{
            return res.json({
                ResponseCode: "1",
                ResponseMessage: "Payment method activated successfully!",
                Response: {},
                errors: "",
            });
        })
        .catch((error) => {
            return res.json({
                ResponseCode: "0",
                ResponseMessage: error.message,
                Response: [],
                errors: error.message,
            });
        });
    }
    
    })
);

module.exports = router;