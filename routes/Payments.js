require('dotenv').config();
const express = require('express');
const router = express();
const { DriverDetails, Wallet, PaymentHistory,PaymentMethod, RequestHistory, Users, Banks, Tips, Charge, CancelHistory } = require('../models');
const { validateToken } = require('../middlewares/AuthorizationMW');
const { Op } = require("sequelize");
const axios = require('axios')
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');

// const stripe = require('stripe')(process.env.STRIPE_KEY);

/*
            1.Make payment to Driver 
    TODO : Transaction ID is static
    _________________________________________________________
    a. Get all the payment requests for a specific user whose status is pending
    b. Apply conditions to change the payment status based upon paid value
    c. Create record in payment history
    d. Return success msg  
*/
router.post('/make', asyncMiddleware(async (req, res) => {
    let {amount, userId} = req.body;
    
     const method = await PaymentMethod.findOne({where:[{status:true},{title:"Stripe"}]});
    const stripe = require('stripe')(method.secretKey);
    
    
    //const requests = await RequestHistory.findAll({where: { [Op.and]: [{UserId: userId},{status: 'pending'}] }});
    const driverData = await DriverDetails.findOne({where: {UserId: userId }});
    if(!driverData.bank){
        return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Bank data missing',
                'Response': [],
                'errors': "",
            });
    }
    axios.post(`${process.env.BASE_URL}/payments/due/rider/admin`, { userId: userId })
    .then(out=>{
        //console.log(out)
        //return res.json();
        if(amount>parseFloat(out.data.Response[0].balance)){
            return res.json({
                'ResponseCode': '0',
                'ResponseMessage': 'Provided amount is greater than balance amount',
                'Response': [],
                'errors': "",
            });
        }
        //let tmpAmount = amount;
        //return res.json(requests)
        // requests.map((val, index)=>{
        //     if(val.pending<tmpAmount){
        //         // Change status from pending to done
        //         // subtrat amount  - val.amount
        //         RequestHistory.update({status: 'paid',pending: 0}, {where: {id: val.id}});
        //         tmpAmount = tmpAmount - val.pending;
        //     }
        //     else if(val.pending === tmpAmount){
        //         RequestHistory.update({status: 'paid',pending: 0}, {where: {id: val.id}})
        //         tmpAmount = 0;
        //     }
        //     else{
        //         let pendingReqAmount= val.pending - tmpAmount
        //         RequestHistory.update({pending: pendingReqAmount}, {where: {id: val.id}})
        //         tmpAmount = 0;
        //     }
        //     if(tmpAmount === 0) { return }
        // });
       // Transfer money from Admin to stripe connected account and create an entry in Payment history
            // converting amount to cents
            amount = amount*100;
            //return res.json(amount);
            stripe.transfers.create({
                amount: amount,
                currency: "usd",
                destination: `${driverData.bank}`,
            })
            .then(trans=>{
                //return res.json(trans)
                stripe.payouts.create({
                    amount: amount,
                    currency: 'usd',
                    },{stripeAccount: `${driverData.bank}`
                })
                .then(paid=>{
                    let cDate = new Date();
                    let date = cDate.getDate();
                    let month = cDate.getMonth() + 1;
                    let year = cDate.getFullYear();
                    let hours = cDate.getHours();
                    let minutes = cDate.getMinutes();
                    PaymentHistory.create({
                        amount: amount/100,
                        transaction_id: paid.id,
                        time: `${month}/${date}/${year} ${hours}:${minutes}`,
                        UserId: userId,
                    })
                    .then(dat=>{
                        //return res.json(paid);
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Payment successful of Rider',
                            'Response': [],
                            'errors': "",
                         })
                    })     
                    .catch(err=>{
                        res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Request Unsuccessful',
                            'Response': [],
                            'errors': "Error in DB writing",
                        });
                    });
                })
                .catch(err=>{
                    res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Request Unsuccessful',
                        'Response': [],
                        'errors': err,
                    });
                })
            })
            .catch(err=>{
                res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Request Unsuccessful',
                    'Response': [],
                    'errors': err.raw.message,
                });
            });
            
    })
}));

/*
            2. Balance - Due Amount of rider
    ________________________________________________________
    a. Find the bank and replace the iBan digist with stars except for last 4
    b. Find all the amount that rider has earned through rides (stored in wallet)
    c. Find all the payments that has been made
    d. Find all the earned Tips
    e. Add tips and earned money to get the overall earning
    f. Find the overall paid money
    g. Find balance by subtracting paid from overall earnings
    h. Return --> Success Msg
*/
router.get('/due/rider', validateToken, asyncMiddleware(async (req, res) => {
    const userId = req.user.id;
    let accountExist = false;
    let paymentMode = '';
    
     const method = await PaymentMethod.findOne({where:[{status:true},{title:"Stripe"}]});
    const stripe = require('stripe')(method.secretKey);
    
    //const exist = await Banks.findOne({where: {status: 1, UserId: userId}});
    const driverData = await DriverDetails.findOne({where: {UserId: userId }});
    //return res.json(driverData);
    let exist={};
    if(driverData.bank){
        const account = await stripe.accounts.retrieve(`${driverData.bank}`);
        //return res.json(account)
        //check if external account exist
        if(account.charges_enabled){
            accountExist = true;
            // checking payment mode
            if(account.external_accounts.data[0].object === 'bank_account')
            {
                paymentMode= 'bank';
                exist = {
                    bankName: account.external_accounts.data[0].bank_name,
                    iBAN:account.external_accounts.data[0].last4 ,
                };
            }
            else{
                paymentMode= 'card';
                exist = {
                    iBAN:account.external_accounts.data[0].last4 ,
                };
            }
            
        }
        else{
            accountExist = true;
            paymentMode= 'bank';
                exist = {
                    bankName: 'Information Missing',
                    iBAN: 'Information Missing' ,
                };
        }
    }
    
    //return res.json(account);
    const riderWallet = await Wallet.findAll({where: {UserId: userId}});
    const paymentWallet = await PaymentHistory.findAll({where: {UserId: userId}});
    const cancelledRides = await CancelHistory.findAll({where: {UserId: userId}});
    //return res.json(cancelledRides);
    const tips = await Tips.findAll({where : {UserId: userId}});
    let due = 0, paid=0, balance =0 , tipsTotal = 0, cancelPenalty =0;
    riderWallet.map((sing, index)=>{
        due = due + parseFloat(sing.amount); 
    });
    tips.map((sing, index)=>{
        tipsTotal = tipsTotal + parseFloat(sing.amount); 
    });
    
    paymentWallet.map((sing, index)=>{
        paid = paid + parseFloat(sing.amount); 
    })
    cancelledRides.map((sing, index)=>{
        cancelPenalty = cancelPenalty + parseFloat(sing.amount); 
    })
    //return res.json(cancelPenalty)
    balance = (due+tipsTotal) - paid - cancelPenalty ;
    
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Balance Amount of Rider',
        'Response': [{
            overAllEarnings: `${due+tipsTotal}`,
            balance: `${balance}`,
            ridesAmount: `${due}` ,
            tipsAmount: `${tipsTotal}`,
            paidAmount: `${paid}`,
            cancelPenalty: `${cancelPenalty}`,
            bankexist: accountExist,
            paymentMode: paymentMode,
            acctDetails: accountExist? paymentMode === 'bank'? {bankName: exist.bankName, iBAN: exist.iBAN}: {bankName:'', iBAN: exist.iBAN}  : {},
        }],
        'errors': "",
    });
}));

/*
            3. Make Payment Request
    _______________________________________________________________________
    a. Get all the payment requests that have been made whose status is pending & Calculate a sum of pending
    b. Get the balance (due amount) by calling an API
    c. Calculate the remaining amount (the amount that can be requested)
    d. if amount is less than a specific amount, Return --> Error
    e. If amount is greater than remaining amount, Return --> Error
    f. Return --> Success msg
*/
router.post('/request', asyncMiddleware(async (req, res) => {
    let {amount, userId} = req.body;
    
    const method = await PaymentMethod.findOne({where:[{status:true},{title:"Stripe"}]});
    const stripe = require('stripe')(method.secretKey);
    
    //To get stripe connected account ID
    const driverData = await DriverDetails.findOne({where: {UserId: userId }});
    //return res.json(driverData)
    const accessToken = req.header('accessToken');
    const minAmount = await Charge.findOne({where: {title: 'Minimum amount (Driver Request)'}});
    //return res.json(accessToken)
    // const payments = await PaymentHistory.findAll({where: {UserId: userId}});
    // let paidAmount=0;
    // payments.map((sing, index)=>{
    //     paidAmount = paidAmount + sing.amount; 
    // });
    //console.log('Paid',paidAmount )
    axios.get(`${process.env.BASE_URL}/payments/due/rider`, {headers: {  'accessToken': accessToken} })
    .then(dat=>{
        console.log(dat.data.Response[0].balance);
        let balance = dat.data.Response[0].balance;
        //let remainingAmount = balance - paidAmount;
        if(amount<= parseFloat(minAmount.amount)){
            res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Request Unsuccessful',
                    'Response': [],
                    'errors': "Unsuccessful as the requested amount is less than the minimum allowed amount",
                });
            //res.send('You cant make this a request that is less than the defined amount')
        }
        else if(amount> balance){
            res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Request Unsuccessful',
                    'Response': [],
                    'errors': "You cant make this a request as your requested amount is greater than your pending",
            });
        }
        else{
            
            
            
            
            // Transfer money from Admin to stripe connected account and create an entry in Payment history
            // converting amount to cents
            amount = amount*100;
            //return res.json(amount);
            stripe.transfers.create({
                amount: amount,
                currency: "usd",
                destination: `${driverData.bank}`,
            })
            .then(trans=>{
                //return res.json(trans)
                stripe.payouts.create({
                    amount: amount,
                    currency: 'usd',
                    },{stripeAccount: `${driverData.bank}`
                })
                .then(paid=>{
                    let cDate = new Date();
                    let date = cDate.getDate();
                    let month = cDate.getMonth() + 1;
                    let year = cDate.getFullYear();
                    let hours = cDate.getHours();
                    let minutes = cDate.getMinutes();
                    PaymentHistory.create({
                        amount: amount/100,
                        transaction_id: paid.id,
                        time: `${month}/${date}/${year} ${hours}:${minutes}`,
                        UserId: userId,
                    })
                    .then(dat=>{
                        //return res.json(paid);
                        res.status(200).json({
                            'ResponseCode': '1',
                            'ResponseMessage': 'Payment successful of Rider',
                            'Response': [],
                            'errors': "",
                         })
                    })     
                    .catch(err=>{
                        res.status(200).json({
                            'ResponseCode': '0',
                            'ResponseMessage': 'Request Unsuccessful',
                            'Response': [],
                            'errors': "Error in DB writing",
                        });
                    });
                })
                .catch(err=>{
                    res.status(200).json({
                        'ResponseCode': '0',
                        'ResponseMessage': 'Request Unsuccessful',
                        'Response': [],
                        'errors': err.raw.message,
                    });
                })
            })
            .catch(err=>{
                res.status(200).json({
                    'ResponseCode': '0',
                    'ResponseMessage': 'Request Unsuccessful',
                    'Response': [],
                    'errors': err.raw.message,
                });
            });
            
        }
    })
    .catch(err=>{
        //res.send('error getting balance')
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Error getting balance',
            'Response': [],
            'errors': "",
        });
    })
}));

/*
            4. Get all payment history
    ____________________________________________________________ 
*/
router.get('/history', asyncMiddleware(async (req, res) => {
    const Histories = await PaymentHistory.findAll({include: {model : Users}});
    //return res.json(Histories)
    let outArr =[];
    Histories.map((hist,index)=>{
        //console.log(hist.User.firstName);
        let tmpObj ={
            transactionId: hist.transaction_id,
            driverId: hist.UserId,
            requestedBy: hist.User? `${hist.User.firstName} ${hist.User.lastName}`: 'No record' ,
            status: `completed`,
            amount: hist.amount,
            at: hist.time,
        };
        outArr[index] = tmpObj;
    })
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Payment History',
        'Response': outArr,
        'errors': "",
    });
}));

/*
            5. Get all payment requests history
    ____________________________________________________________ 
*/
// router.get('/requesthistory', asyncMiddleware(async (req, res) => {
//     const Histories = await RequestHistory.findAll({include: {model : Users}});
//     //return res.json(Histories)
//     let outArr =[];
//     Histories.map((hist,index)=>{
//         let tmpObj ={
//             driverId: hist.UserId,
//             requestedBy: hist.User? `${hist.User.firstName} ${hist.User.lastName}`: 'No record',
//             amount: hist.amount,
//             status: hist.status,
//             requestedAt: hist.time,
//         };
//         outArr[index] = tmpObj;
//     })
//     res.status(200).json({
//         'ResponseCode': '1',
//         'ResponseMessage': 'Payment Request History',
//         'Response': outArr,
//         'errors': "",
//     });
// }))

/*
            6. Get all payment requests whose status is pending
    ____________________________________________________________ 
*/
// router.get('/requestpending', asyncMiddleware(async (req, res) => {
//     const Histories = await RequestHistory.findAll({ where: {status: "pending"},  include: {model : Users}});
//     //return res.json(Histories)
//     let outArr =[];
//     Histories.map((hist,index)=>{
//         let tmpObj ={
//             id: hist.id,
//             requestedBy: `${hist.User.firstName} ${hist.User.lastName}`,
//             amount: hist.amount,
//             status: hist.status,
//             requestedAt: hist.time,
//             userId: hist.UserId,
//         };
//         outArr[index] = tmpObj;
//     })
//     res.status(200).json({
//         'ResponseCode': '1',
//         'ResponseMessage': 'Payment Request History',
//         'Response': outArr,
//         'errors': "",
//     });
// }))

/*
            7. Make payment for pending request
    ____________________________________________________________ 
*/
// router.post('/make/pending', asyncMiddleware(async (req, res) =>{
//     const {requestId, amount, userId} = req.body;
//     RequestHistory.update({status: 'paid',pending: 0}, {where: {id: requestId}})
//     .then(succ=>{
//         let cDate = new Date();
//         let date = cDate.getDate();
//         let month = cDate.getMonth() + 1;
//         let year = cDate.getFullYear();
//         let hours = cDate.getHours();
//         let minutes = cDate.getMinutes();
//         PaymentHistory.create({
//             amount,
//             transaction_id: '12345',
//             time: `${month}/${date}/${year} ${hours}:${minutes}`,
//             UserId: userId,
//         })
//         .then(dat=>{
//             res.status(200).json({
//                 'ResponseCode': '1',
//                 'ResponseMessage': 'Payment successful of Rider',
//                 'Response': [],
//                 'errors': "",
//             });
//         })
//     })
// }));

/*
            8. Get all the payment request made by a specific user(i.e. Driver)
    _____________________________________________________________________________
        a. Get all the history of user from DB
        b. filter it and get the desired information and save it in a new array
*/
// router.get('/request/driver', asyncMiddleware(async (req, res) =>{
//     const hist = await RequestHistory.findAll({where: {UserId: 27}});
//     let outArr =[];
//     hist.map((h,i)=>{
//         let paid = (h.amount - h.pending);
//         let tmpObj = {
//             requestId: `${h.id}`,
//             requestedAmount: `${h.amount}` ,
//             status: h.status,
//             amountPaid: `${paid}` ,
//             amountPending: `${ h.pending}`,
//             requestedAt: h.time,
//         };
//         outArr[i]= tmpObj;
//     });
//     res.status(200).json({
//         'ResponseCode': '1',
//         'ResponseMessage': 'Payment Request History',
//         'Response': outArr,
//         'errors': "",
//     });
// }));

/*
            9. Get all the payments made by Admin to a specific user(i.e. Driver)
    ____________________________________________________________________________________
        a. Get all the payments for a user from DB
        b. filter it and get the desired information and save it in a new array
*/
router.get('/payments/driver/:id', asyncMiddleware(async (req, res) =>{
    const paym = await PaymentHistory.findAll({where: {UserId: req.params.id }});
    let outArr =[];
    paym.map((h,i)=>{
        let tmpObj = {
            paymentId: `${h.id}`,
            paidAmount: `${h.amount}`,
            transactionId: `${h.transaction_id}`,
            requestedAt: h.time,
        };
        outArr[i]= tmpObj;
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Payment transaction History',
        'Response': outArr,
        'errors': "",
    });
}));
/*
            9..1 Get all the payments made by Admin to a specific user(i.e. Driver)
    ____________________________________________________________________________________
        a. Get all the payments for a user from DB
        b. filter it and get the desired information and save it in a new array
*/
router.get('/payments/alldriver', validateToken ,asyncMiddleware(async (req, res) =>{
    const paym = await PaymentHistory.findAll({where: {UserId: req.user.id }});
    let outArr =[];
    paym.map((h,i)=>{
        let tmpObj = {
            paymentId: `${h.id}`,
            paidAmount: `${h.amount}`,
            transactionId: `${h.transaction_id}`,
            requestedAt: h.time,
        };
        outArr[i]= tmpObj;
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Payment transaction History',
        'Response': outArr,
        'errors': "",
    });
}));

/*
            10. Balance - Due amount of all the drivers
    ______________________________________________________________________
*/
router.get('/alldue', asyncMiddleware(async (req, res) =>{
    // getting active drivers
    const drivers = await Users.findAll({where: {status: 1, UserCategoryId: 3}, include: {model : DriverDetails}});
    // return res.json(drivers)
    // getting all the details of earnings, tips and payments
    const riderWallet = await Wallet.findAll();
    const paymentWallet = await PaymentHistory.findAll();
    const tips = await Tips.findAll();
    const cancelledBookings = await CancelHistory.findAll();
    
    //return res.json(cancelledBookings)
    let due = 0, paid=0, balance =0 , tipsTotal = 0, cancelPenalty =0;
    let outArr = [];
    // getting balance of each driver 
    drivers.map((d,i)=>{
        // Filter earnings of specific driver
        let earningsFilter = riderWallet.filter(e=> e.UserId === d.id);
        
        // Filter Tips of specific driver
        let tipsFiltered = tips.filter(e=> e.UserId === d.id);
        // Filter payments of specific driver
        let paymentsFilter = paymentWallet.filter(e=> e.UserId === d.id);
       
        // filter cancel bookings of a driver
        let cancelFilter = cancelledBookings.filter(e=> e.UserId === d.id);
        //console.log()
        // Caluculating all due amount
        earningsFilter.map((sing, index)=>{
            due = due + parseFloat(sing.amount); 
        });
        tipsFiltered.map((sing, index)=>{
            tipsTotal = tipsTotal + parseFloat(sing.amount); 
        });
        
        paymentsFilter.map((sing, index)=>{
            paid = paid + parseFloat(sing.amount); 
        })
        cancelFilter.map((sing, index)=>{
           cancelPenalty = cancelPenalty +  parseFloat(sing.amount);
        })
        
        balance = (due+tipsTotal) - paid - cancelPenalty;
        let tmpObj = {
            id: d.id,
            name: `${d.firstName} ${d.lastName}`,
            stripeId: d.DriverDetails[0]?.bank,
            overallEarnings: due+tipsTotal,
            ridesEarnings: due,
            overallTips: tipsTotal,
            overallPaid: paid,
            cancelPenalty,
            balance,
        };
        outArr.push(tmpObj);
        due = 0, paid=0, balance =0 , tipsTotal = 0, cancelPenalty=0
    });

    return res.json(outArr);
}));

/*
            2. Balance - Due Amount of rider - Admin Panel
    ________________________________________________________
    a. Find the bank and replace the iBan digist with stars except for last 4
    b. Find all the amount that rider has earned through rides (stored in wallet)
    c. Find all the payments that has been made
    d. Find all the earned Tips
    e. Add tips and earned money to get the overall earning
    f. Find the overall paid money
    g. Find balance by subtracting paid from overall earnings
    h. Return --> Success Msg
*/
router.post('/due/rider/admin', asyncMiddleware(async (req, res) => {
    const {userId} = req.body;
    let accountExist = false;
    let paymentMode = 'bank';
    //const exist = await Banks.findOne({where: {status: 1, UserId: userId}});
    const driverData = await DriverDetails.findOne({where: {UserId: userId }});
    //return res.json(driverData);
    let exist={
        bankName: 'No record',
        iBAN: 'No record',
        routingNum : "No record"
    };
    if(driverData.bank){
        const account = await stripe.accounts.retrieve(`${driverData.bank}`);
        //return res.json(account)
        //check if external account exist
        if(account.charges_enabled){
            accountExist = true;
            // checking payment mode
            if(account.external_accounts.data[0].object === 'bank_account')
            {
                paymentMode= 'bank';
                exist = {
                    bankName: account.external_accounts.data[0].bank_name,
                    iBAN:account.external_accounts.data[0].last4 ,
                    routingNum: account.external_accounts.data[0].routing_number,
                };
            }
            else{
                paymentMode= 'card';
                exist = {
                    iBAN:account.external_accounts.data[0].last4 ,
                };
            }
            
        }
    }
    const riderWallet = await Wallet.findAll({where: {UserId: userId}});
    const paymentWallet = await PaymentHistory.findAll({where: {UserId: userId}});
    const tips = await Tips.findAll({where : {UserId: userId}});
    const cancelledRides = await CancelHistory.findAll({where: {UserId: userId}});
    let due = 0, paid=0, balance =0 , tipsTotal = 0,cancelPenalty =0;
    riderWallet.map((sing, index)=>{
        due = due + parseFloat(sing.amount); 
    });
    tips.map((sing, index)=>{
        tipsTotal = tipsTotal + parseFloat(sing.amount); 
    });
    
    paymentWallet.map((sing, index)=>{
        paid = paid + parseFloat(sing.amount); 
    })
    cancelledRides.map((sing, index)=>{
        cancelPenalty = cancelPenalty + parseFloat(sing.amount); 
    })
    //return res.json(cancelPenalty)
    balance = (due+tipsTotal) - paid - cancelPenalty ;
    
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Balance Amount of Rider',
        'Response': [{
            balance: `${balance}`,
            totalAmount: due+tipsTotal,
            ridesAmount: `${due}` ,
            tipsAmount: `${tipsTotal}`,
            paidAmount: `${paid}`,
            cancelPenalty: `${cancelPenalty}`,
            bankexist: accountExist,
            paymentMode: paymentMode,
            acctDetails: accountExist? paymentMode === 'bank'? {bankName: exist.bankName, iBAN: exist.iBAN, routingNum: exist.routingNum, }: {iBAN: exist.iBAN}  : {},
        }],
        'errors': "",
    });
}));




module.exports = router;