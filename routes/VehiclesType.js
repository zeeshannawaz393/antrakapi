const express = require('express');
const router = express();
const { VehiclesType, CardBrand } = require('../models');
const bcrypt = require('bcrypt');
const { sign } = require('jsonwebtoken');
const { validateToken } = require('../middlewares/AuthorizationMW');
const { Op, DataTypes } = require("sequelize");
const { verify } = require('jsonwebtoken');
// Importing function to pass as reference in the requests
const asyncMiddleware = require('../middlewares/async');
// Custom Error Object
const CustomException = require('../middlewares/errorobject');
const multer = require('multer')
const path = require('path');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null,`Images/VehiclesType`)
    },
    filename: (req, file, cb) => {
        cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-'  + Date.now() + path.extname(file.originalname) )
    }
})
const upload = multer({
    storage: storage,
    limits: { fileSize: '1000000' },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/
        const mimeType = fileTypes.test(file.mimetype)  
        const extname = fileTypes.test(path.extname(file.originalname))

        if(mimeType && extname) {
            return cb(null, true)
        }
        cb('Give proper files formate to upload')
    }
});

/*
1. Add vehicle 
-------------------------------------
a. check if the same name already exist 
b. if exist change its status to false
c. Add a new vehicle with status true
*/
router.post('/add', upload.single('image'), asyncMiddleware(async (req, res) => {
    const {name, baseRateVehicle, ratePerMile, volumeCapacity, weightCapacity} = req.body;
    const exist = await VehiclesType.findOne({where : {name: name}});
    if(exist){
        VehiclesType.update({status: false}, {where: {id: exist.id}})
    }
    VehiclesType.create({
        name,
        image: req.file.path,
        status: true,
        baseRateVec:baseRateVehicle,
        baseRateMile: ratePerMile,
        VolumeCap: volumeCapacity,
        WeightCap: weightCapacity,
    }).then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Vehicle Type Added',
            'Response': [{
                id: `${dat.id}`,
                name:`${dat.name}` ,
                image: `${dat.image}`,
                baserates: `${dat.baseRateVec}`,
                ratemiles: `${dat.baseRateMile}`,
                volcaps: `${dat.VolumeCap}`,
                wcaps: `${dat.WeightCap}`
            }],
            'errors': '',
        });
    })
    .catch(err=>{
       res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Error Adding Vehicle',
            'Response': [],
            'errors': '',
        }); 
    })
}));

//2. Get vehicle Details using ID  -----  Tufail api
router.get('/vec_detail/:id', asyncMiddleware(async (req, res) => {
    id=req.params.id;
    const vehicleTypes = await VehiclesType.findOne({where:{id:id}});
   
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'Vehicle Detail',
        'Response': vehicleTypes,
        'errors': '',
    });

}));

//3. Get All vehicles 
router.get('/getall', asyncMiddleware(async (req, res) => {
    const vehicleTypes = await VehiclesType.findAll({where: {status: true}});
    let outArr = [];
    if (!vehicleTypes) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No Vehicle Found',
            'Response': '',
            'errors': '',
        });
    }
    vehicleTypes.map((dat, index) => {
        var path = dat.image;
        var path2 = path.replace(/\\/g, "/");
        let tmpObj = {
            id: dat.id,
            name: dat.name,
            image: path2,
            status: dat.status,
            baseRateVehicle: dat.baseRateVec,
            ratePerMile: dat.baseRateMile,
            volumeCapacity: dat.VolumeCap,
            weightCapacity: dat.WeightCap,
            createdAt:dat.createdAt
        };
        outArr[index] = tmpObj;
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'All Vehicle Details',
        'Response': vehicleTypes,
        'errors': '',
    });
}));


// 4. Delete Vehicle by ID // Also for changing the status of Vehicle
router.delete('/delete/:id', asyncMiddleware(async (req, res) => {
    const id = req.params.id;
    VehiclesType.update( {status: false}, { where: {id: id}})
        .then(() => {
            res.status(200).json({
                'ResponseCode': '1',
                'ResponseMessage': 'Vehicle Deleted',
                'Response': [],
                'errors': '',
            })
        })
        .catch(err => {
            res.status(200).json({
                'ResponseCode': '0',
                'ResponseMessage': 'Error',
                'Response': [],
                'errors': 'Vehicle deletion unsuccessful',
            })
        })
}));

// 5. Update Vehicle by ID
router.post('/update/:id',upload.single('image'), asyncMiddleware(async (req, res) => {
    const vehId = req.params.id;
    const {name, status ,baseRateVehicle, ratePerMile, volumeCapacity, weightCapacity} = req.body;
    VehiclesType.update({
        name,
        image: req.file.path,
        status,
        baseRateVec: baseRateVehicle,
        baseRateMile: ratePerMile,
        VolumeCap: volumeCapacity,
        WeightCap: weightCapacity,
    }, {where: {id: vehId}})
    .then(()=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Vehicle Updated',
            'Response': [],
            'errors': '',
        });
    })
    .catch(err=>{
       res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Error Updating Vehicle',
            'Response': [],
            'errors': '',
        }); 
    })
}));

/*
6. Get all vehicles that fall within the capacity criteria
------------------------------------------------------------------------
a. Get all vehicles from table where status is true
b. get the max weight and max volume out of all packages
c. Apply filter to get the desired vehicles
*/
router.post('/getall/filtered', asyncMiddleware(async (req, res) => {
    //return res.json('API HIT')
    const {packages } = req.body;
    // Getting all vehicles
    const vehicleTypes = await VehiclesType.findAll({where: {status: true}});
   // return res.json(parseFloat(vehicleTypes[2].VolumeCap));
    let outArr = [];
    if (!vehicleTypes) {
        return res.status(200).json({
            'ResponseCode': '0',
            'ResponseMessage': 'No Vehicle Found',
            'Response': '',
            'errors': '',
        });
    }
    // Going through pakages to get the max weight and volume among all the packages
    let maxWeight =0.0, maxVolume=0.0;
    packages.map((pkg,indx)=>{
        if(parseFloat(pkg.weight)> maxWeight){
            maxWeight = parseFloat(pkg.weight);
        }
        let vol = parseFloat(pkg.length) * parseFloat(pkg.width) * parseFloat(pkg.height);
        if(pkg.unit === 'in'){
            vol = vol * 0.0610237;
        }
        if(vol> maxVolume){
            maxVolume = vol;
        }
    })
    //res.json(maxWeight);
    let c=0;
    // Filtering Vehicles
    vehicleTypes.map((dat, index) => {
        if(parseFloat(dat.VolumeCap)>= maxVolume && parseFloat(dat.WeightCap)>= maxWeight){
            var path = dat.image;
            var path2 = path.replace(/\\/g, "/");
            let tmpObj = {
                id: dat.id,
                name: dat.name,
                image: path2,
            };
        outArr[c] = tmpObj;
        c++;
        }
        
    });
    res.status(200).json({
        'ResponseCode': '1',
        'ResponseMessage': 'All Vehicle Details',
        'Response': outArr,
        'errors': '',
    });
}));

// --------------------------
// TO ADD IMAGES FOR CARDS  |
// --------------------------

//4. Add Card Brand // Change the folder path as well
router.post('/addbrand', upload.single('image'), asyncMiddleware(async (req, res) => {
    const {title} = req.body;
    //const exist = await VehiclesType.findOne({where : {name: name}});
    //if(exist) throw new CustomException('The vehicle with this name already exists' , 200);
    //console.log(req.file.path)
    CardBrand.create({
        title,
        image: req.file.path,
    }).then((dat)=>{
        res.status(200).json({
            'ResponseCode': '1',
            'ResponseMessage': 'Brand Added',
            'Response': [],
            'errors': '',
        });
    })
}));

module.exports = router;