require('dotenv').config();
const express = require('express');
const app = express();
const db = require('./models');
const {PaymentHistory,PaymentMethod } = require('./models');
const cors = require('cors');
const error = require('./middlewares/error');
var cron = require('node-cron');

const axios = require('axios')
const stripe = require('stripe')(process.env.STRIPE_KEY);

//const path = require('path')
// Importing routes
const userRouter = require('./routes/Users');
const fEmailRouter = require('./routes/ForgetPassword');
const categoryRouter = require('./routes/Category');
const paymentmethodRouter = require('./routes/PaymentMethod');
const packageRouter = require('./routes/Package');
const addressRouter = require('./routes/Address');
const couponsRouter = require('./routes/Coupons');
const bookingsRouter = require('./routes/Booking');
const bookingStatusRouter = require('./routes/BookingStatus');
const roleRouter = require('./routes/Role');
const driverRouter = require('./routes/Driver');
const vehicleRouter = require('./routes/VehiclesType');
const chargesRouter = require('./routes/Charges');
const appSettingRouter = require('./routes/AppSettings');
const Restrictedroute = require('./routes/Restricted');
const ratingTipsRouter = require('./routes/RatingTips');
const BookIDPicturesRouter = require('./routes/BookIDPictures');
const paymentsRouter = require('./routes/Payments');
//Handling Uncaught Exceptions (Sync)
process.on('uncaughtException', (ex)=>{
    console.log('Internal Server Error', ex.message);
});
// // // Handling Unhandled promise (Async)
process.on('unhandledRejection', (ex)=>{
    console.log('Something went wrong', ex.message);
});
//To send Json data using post request
app.use(express.json({limit: '50mb'}));

// To
app.use(cors());

//employing routes in the middleware functions  
app.use('/appsettings', appSettingRouter);
app.use('/users', userRouter);
app.use('/users/femail', fEmailRouter);
app.use('/category', categoryRouter);
app.use('/package', packageRouter);
app.use('/address', addressRouter);
app.use('/coupons', couponsRouter);
app.use('/booking', bookingsRouter);
app.use('/bookingstatus', bookingStatusRouter);
app.use('/role', roleRouter);
app.use('/driver', driverRouter);
app.use('/driver/p',  BookIDPicturesRouter );
app.use('/vehicle', vehicleRouter);
app.use('/charges', chargesRouter);
app.use('/restricted', Restrictedroute);
app.use('/ratingtips', ratingTipsRouter);
app.use('/payments', paymentsRouter);
app.use('/method', paymentmethodRouter);

// Error middleware : To show any error if promise fails
app.use(error); 

// To make the folder Public
app. use('/Images' ,express.static('./Images'));

// Running every friday night at 11:50 pm
// Transferring all the balance to driver accounts
cron.schedule('55 23 * * 5',async () => {
    
    
    const method = await PaymentMethod.findOne({where:[{status:true},{title:"Stripe"}]});
    const stripe = require('stripe')(method.secretKey);
    
  //console.log('running a task every sec');
  // get all the drivers
  axios.get(`${process.env.BASE_URL}/payments/alldue`)
  .then(result=>{
      //console.log(result.data);
      result.data.map((dat,i)=>{
        // converting amount to cents
        let amount = parseFloat(dat.balance);
        amount = amount*100;
        if(amount>0){
            if(!dat.stripeId){
                console.log(`Missing bank Account for Driver ${dat.name}`)
                return false;
            }
            stripe.transfers.create({
            amount: amount,
            currency: "usd",
            destination: `${dat.stripeId}`,
        })
        .then(trans=>{
            //return res.json(trans)
            stripe.payouts.create({
                amount: amount,
                currency: 'usd',
                },{stripeAccount: `${dat.stripeId}`
            })
            .then(paid=>{
                let cDate = new Date();
                let date = cDate.getDate();
                let month = cDate.getMonth() + 1;
                let year = cDate.getFullYear();
                let hours = cDate.getHours();
                let minutes = cDate.getMinutes();
                PaymentHistory.create({
                    amount: amount/100,
                    transaction_id: paid.id,
                    time: `${month}/${date}/${year} ${hours}:${minutes}`,
                    UserId: dat.id,
                })
                .then(d=>{
                    console.log(`Balance paid for driver ${dat.name}`)
                })     
            });
        })
        }
        else{
            console.log(`No outstandning balance for driver ${dat.name}`)
        }
        
      })
  })     
    
},
{
   scheduled: true,
   timezone: "America/Los_Angeles"
 });

// Initializing Server along with creating all the tables that exist in the models folder
db.sequelize.sync().then(()=>{
    app.listen(3203, ()=> {console.log('Starting the server at port 3203 ...')});
});